"""
  This file is part of TALER
  Copyright (C) 2014, 2015 Christian Grothoff (and other contributing authors)

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>

  @author Florian Dold
  @author Benedikt Muller
  @author Sree Harsha Totakura
"""

import taler

def test():
    # Tests for private key
    priv = taler.c_RsaPrivateKey(1024)
    enc = priv.encode()
    print (enc)
    priv2 = taler.c_RsaPrivateKey.decode(enc)
    enc2 = priv2.encode()
    assert enc == enc2

    # Tests for public key
    pub = priv.get_public()
    pub_enc = pub.encode()
    print (pub_enc)
    pub2 = taler.c_RsaPublicKey(priv)
    pub2_enc = pub2.encode()
    assert pub2_enc == pub_enc
    pub3 = taler.c_RsaPublicKey.decode(pub_enc)
    pub3_enc = pub3.encode()
    assert pub3_enc == pub_enc

    # Tests for blinding key
    bkey = taler.c_RsaBlindingKey(1024, pub)
    bkey_enc = bkey.encode()
    print (bkey_enc)
    bkey2 = taler.c_RsaBlindingKey.decode(bkey_enc, pub)
    bkey2_enc = bkey2.encode()
    assert bkey2_enc == bkey_enc

    # RSA signing and verification
    msg =  b'I am happy to join with you today in what will go down in history \
    as the greatest demonstration for freedom in the history of our nation.'
    hash = taler.sha512(msg)
    sig = priv2.sign(hash)
    assert pub.verify(hash, sig) is True
    hash_false = taler.sha512(b'faked')
    assert pub.verify(hash_false, sig) is False
    sig_enc = sig.encode()
    print(sig_enc)
    sig2 = taler.c_RsaSignature.decode(sig_enc)
    sig2_enc = sig2.encode()
    assert sig_enc == sig2_enc

    # RSA blind signing and verification
    bmsg = bkey.blind(hash)
    bsig = priv.sign(bmsg)
    sig = bkey.unblind(bsig)
    assert pub2.verify(hash, sig) is True

if __name__ == "__main__":
    test()
