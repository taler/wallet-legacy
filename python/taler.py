"""
  This file is part of TALER
  Copyright (C) 2014, 2015 Christian Grothoff (and other contributing authors)

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>

  @author Florian Dold
  @author Benedikt Muller
  @author Sree Harsha Totakura
"""

from ctypes import *
from ctypes.util import find_library
from functools import total_ordering
import hashlib

gnunet_util = CDLL("libgnunetutil.so")
taler_util = CDLL("libtalerutil.so")
libc = CDLL(find_library("c"))

GNUNET_CRYPTO_AES_KEY_LENGTH = 256 // 8

SIGNATURE_WITHDRAW = 4
SIGNATURE_REFRESH_MELT = 5
SIGNATURE_REFRESH_COMMIT = 6
SIGNATURE_REFRESH_MELT_CONFIRM = 9

AMOUNT_FRAC_BASE = 1000000
AMOUNT_FRAC_LEN = 6

crypto_quality =  {'weak' : 0, 'strong' : 1, 'nonce' : 2}

class c_HashCode(Structure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [
            ("bits", c_byte * 64),
            ]

class c_SymmetricSessionKey(Structure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [
            ("aes_key", c_byte * GNUNET_CRYPTO_AES_KEY_LENGTH),
            ("twofish_key", c_byte * GNUNET_CRYPTO_AES_KEY_LENGTH)
            ]

class c_SymmetricInitializationVector(Structure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [
            ("aes_iv", c_byte * (GNUNET_CRYPTO_AES_KEY_LENGTH // 2)),
            ("twofish_iv", c_byte * (GNUNET_CRYPTO_AES_KEY_LENGTH // 2))
            ]


_rsa_private_key_create = gnunet_util.GNUNET_CRYPTO_rsa_private_key_create
_rsa_private_key_create.restype = c_void_p
_rsa_private_key_create.argtypes = [c_uint]

_rsa_private_key_free = gnunet_util.GNUNET_CRYPTO_rsa_private_key_free
_rsa_private_key_free.restype = None
_rsa_private_key_free.argtypes = [c_void_p]

_rsa_private_key_encode = gnunet_util.GNUNET_CRYPTO_rsa_private_key_encode
_rsa_private_key_encode.restype = c_size_t
_rsa_private_key_encode.argtypes = [c_void_p, POINTER(c_void_p)]

_rsa_private_key_decode = gnunet_util.GNUNET_CRYPTO_rsa_private_key_decode
_rsa_private_key_decode.restype = c_void_p
_rsa_private_key_decode.argtypes = [POINTER(c_void_p), c_size_t]

_rsa_private_key_get_public = gnunet_util.GNUNET_CRYPTO_rsa_private_key_get_public
_rsa_private_key_get_public.restype = c_void_p
_rsa_private_key_get_public.argtypes = [c_void_p]

_rsa_sign = gnunet_util.GNUNET_CRYPTO_rsa_sign
_rsa_sign.restype = c_void_p # RsaSignature
_rsa_sign.argtypes = [c_void_p, # RsaPrivateKey
                      c_void_p, # message
                      c_size_t] # message size

class c_RsaPrivateKey():
    """RSA private key containing the pointer to the GNUNET_CRYPTO_rsa_PrivateKey
    structure.

    """

    def __init__(self, size=0):
        """Create an RSA private key with the given length.  The length should be a
        multiple of 8.

        @param arg the RSA key length or the encoded byte string representing
        the private key

        """
        if size is not 0:
            print ("Creating private key")
            self.ptr = _rsa_private_key_create(size)
        else:
            self.ptr = None

    def __del__(self):
        """Free the resources occupied by the given RSA private key

        @param priv the RSA private key
        """
        if self.ptr is not None and self.ptr is not 0:
            _rsa_private_key_free (self.ptr)

    def encode(self):
        """Encode the given private key

        @param priv the private key to encode
        @return the encoded private key as byte string
        """
        enc_ptr = c_void_p()
        size = _rsa_private_key_encode (self.ptr, byref(enc_ptr))
        enc = string_at(enc_ptr.value, size)
        free (enc_ptr)
        return enc

    def _get_public(self):
        """Get the correponding public key returned as a pointer"""
        pub_ptr = _rsa_private_key_get_public (self.ptr)
        if pub_ptr is 0:
            raise Exception("Cannot derive public key")
        return pub_ptr

    def get_public(self):
        """Get the correponding public key returned as an object"""
        pub = c_RsaPublicKey()
        pub.ptr = self._get_public()
        return pub

    def sign(self, msg):
        """Sign the given message"""
        size = len(msg)
        ptr = (c_byte * size).from_buffer_copy(msg)
        sig_ptr = _rsa_sign (self.ptr, cast(byref(ptr), c_void_p), size)
        if sig_ptr is 0:
            return None
        sig = c_RsaSignature ()
        sig.ptr = sig_ptr
        return sig

    @classmethod
    def decode(cls, enc):
        """
        Create a private key from the given encoding.

        @param enc encoding of a private key
        @return the private key. An exception is raised upon failure to decode
                  the given encoding.
        """
        size = len(enc)
        enc_ptr = (c_byte * size).from_buffer_copy (enc)
        ptr = _rsa_private_key_decode (cast(byref(enc_ptr), POINTER(c_void_p)), size)
        if ptr is 0:
            raise Exception("cannot decode private key")
        priv = cls()
        priv.ptr = ptr
        return priv

_rsa_public_key_free = gnunet_util.GNUNET_CRYPTO_rsa_public_key_free
_rsa_public_key_free.restype = None
_rsa_public_key_free.argtypes = [c_void_p]

_rsa_public_key_encode = gnunet_util.GNUNET_CRYPTO_rsa_public_key_encode
_rsa_public_key_encode.restype = c_size_t
_rsa_public_key_encode.argtypes = [c_void_p, POINTER(c_void_p)]

_rsa_public_key_decode = gnunet_util.GNUNET_CRYPTO_rsa_public_key_decode
_rsa_public_key_decode.restype = c_void_p
_rsa_public_key_decode.argtypes = [POINTER(c_void_p), c_size_t]

_rsa_verify = gnunet_util.GNUNET_CRYPTO_rsa_verify
_rsa_verify.restype = c_int
_rsa_verify.argtypes = [POINTER(c_HashCode),
                        c_void_p, # RsaSignature
                        c_void_p] # RsaPublicKey

class c_RsaPublicKey():
    """RSA public key containing the pointer to the GNUNET_CRYPTO_rsa_PublicKey
    structure

    """

    def __init__(self, priv=None):
        """Create an RSA private key with the given length.  The length should be a
        multiple of 8.

        @param arg the RSA key length or the encoded byte string representing
        the private key

        """
        self.ptr = None
        if priv is not None:
            self.ptr = priv._get_public()

    def __del__(self):
        """Relinquish the resources occupied by the given public key"""
        if self.ptr is not None and self.ptr is not 0:
            _rsa_public_key_free (self.ptr)

    def encode(self):
        """Encodes the public key

        @return the encoded public key as byte string
        """
        enc_ptr = c_void_p()
        size = _rsa_public_key_encode (self.ptr, byref(enc_ptr))
        enc = string_at(enc_ptr.value, size)
        free (enc_ptr)
        return enc;

    def verify(self, hash, sig):
        """
        Verifys whether the given hash corresponds to the given signature and the
        signature is made with the corresponding private key of this public key object

        @param hash the hash value
        @param sig the RSA signature
        @return True if the signature is valid; False otherwise
        """
        if type(hash) in [bytes, bytearray]:
            hash = c_HashCode.from_buffer_copy (hash)
            valid = _rsa_verify (byref(hash), sig.ptr, self.ptr)
        if valid is 1:
            return True
        return False

    @classmethod
    def decode(cls, enc):
        """Decodes the given encoded public key.

        @param enc byte string containing the encoded public key
        @return the public key or an exception upon decoding failure
        """
        size = len(enc)
        enc_ptr = (c_byte *size).from_buffer_copy (enc)
        ptr = _rsa_public_key_decode (cast(byref(enc_ptr), POINTER(c_void_p)), size)
        if ptr is 0:
            raise Exception("Cannot decode public key")
        pub = cls()
        pub.ptr = ptr
        return pub

_rsa_signature_free = gnunet_util.GNUNET_CRYPTO_rsa_signature_free
_rsa_signature_free.restype = None
_rsa_signature_free.argtypes = [c_void_p] # c_RsaSignature

_rsa_signature_encode = gnunet_util.GNUNET_CRYPTO_rsa_signature_encode
_rsa_signature_encode.restype = c_size_t
_rsa_signature_encode.argtypes = [c_void_p, # c_RsaSignature
                                  POINTER(c_void_p)] # Pointer to encoding

_rsa_signature_decode = gnunet_util.GNUNET_CRYPTO_rsa_signature_decode
_rsa_signature_decode.restype = c_void_p # c_RsaSignature
_rsa_signature_decode.argtypes = [c_void_p, # Encoding
                                  c_size_t] # size
class c_RsaSignature():
    """RSA signature containing the pointer to the GNUNET_CRYPTO_rsa_Signature
    structure

    """
    def __init__ (self):
        self.ptr = None

    def __del__(self):
        if self.ptr is not None and self.ptr is not 0:
            _rsa_signature_free (self.ptr)

    def encode(self):
         """
         Encodes the given signature

         @return the encoded signature as a byte array
         """
         ptr = c_void_p ()
         size = _rsa_signature_encode (self.ptr, byref(ptr))
         enc = string_at (ptr.value, size)
         free (cast(ptr.value, c_void_p))
         return enc

    @classmethod
    def decode(cls, enc):
        """
        Decodes the given encoded signature

        @param sig_enc the encoded RSA signature
        @return The decoded signature object
        """
        size = len(enc)
        ptr = (c_byte * size).from_buffer_copy (enc)
        sig_ptr = _rsa_signature_decode (cast(byref(ptr), c_void_p), size)
        if sig_ptr is 0:
            raise Exception("Decoding the given signature failed")
        sig = cls()
        sig.ptr = sig_ptr
        return sig

_rsa_blinding_key_create = gnunet_util.GNUNET_CRYPTO_rsa_blinding_key_create
_rsa_blinding_key_create.restype = c_void_p
_rsa_blinding_key_create.argtypes = [c_uint]

_rsa_blinding_key_free = gnunet_util.GNUNET_CRYPTO_rsa_blinding_key_free
_rsa_blinding_key_free.restype = None
_rsa_blinding_key_free.argtypes = [c_void_p]

_rsa_blinding_key_encode = gnunet_util.GNUNET_CRYPTO_rsa_blinding_key_encode
_rsa_blinding_key_encode.restype = c_size_t
_rsa_blinding_key_encode.argtypes = [c_void_p, POINTER(c_void_p)]

_rsa_blinding_key_decode = gnunet_util.GNUNET_CRYPTO_rsa_blinding_key_decode
_rsa_blinding_key_decode.restype = c_void_p
_rsa_blinding_key_decode.argtypes = [POINTER(c_void_p), c_size_t]

_rsa_blind = gnunet_util.GNUNET_CRYPTO_rsa_blind
_rsa_blind.restype = c_size_t
_rsa_blind.argtypes = [POINTER(c_HashCode),
                       c_void_p, # RsaBlindingKey
                       c_void_p, # RsaPublicKey
                       POINTER(c_void_p)] # return

_rsa_unblind = gnunet_util.GNUNET_CRYPTO_rsa_unblind
_rsa_unblind.restype = c_void_p # RsaSignature
_rsa_unblind.argtypes = [c_void_p, # RsaSignature
                         c_void_p, # RsaBlindingKey
                         c_void_p] # RsaPublicKey

class c_RsaBlindingKey():
    """RSA Blinding key containing the pointer to the GNUNET_CRYPTO_rsa_BlindingKey
    structure

    """

    def __init__(self, size, pub):
        """
        Creates a blinding key of given length

        @param len the size of the key
        """
        self.ptr = None
        self.pub = pub
        if size is not 0:
            self.ptr = _rsa_blinding_key_create (size)
            if self.ptr is 0:
                raise Exception("Failed to create blinding key")

    def __del__(self):
        """Relinquishes the resources occupied by the given blinding key"""
        _rsa_blinding_key_free (self.ptr)

    def encode(self):
        """Encode the given blinding key"""
        enc_ptr = c_void_p()
        size = _rsa_blinding_key_encode (self.ptr, byref(enc_ptr))
        enc = string_at(enc_ptr.value, size)
        free (cast(enc_ptr.value, c_void_p))
        return enc

    def blind(self, hash):
        """
        Applies RSA blinding to the given hash.

        @param hash the hash to blind
        @return the blinded hash as a byte array
        """
        if type(hash) in [bytes, bytearray]:
            hash = c_HashCode.from_buffer_copy (hash)
        ptr = c_void_p ()
        size = _rsa_blind (byref(hash), self.ptr, self.pub.ptr, byref(ptr))
        ret = string_at(ptr.value, size)
        free (cast(ptr.value, c_void_p))
        return ret

    def unblind(self, bsig):
        """
        Unblinds the given blind-signed signature.

        @param bsig the blind-signed signature
        @return the unblinded signature; None upon error
        """
        sig_ptr = _rsa_unblind (bsig.ptr, self.ptr, self.pub.ptr)
        if sig_ptr is 0:
            return None
        sig = c_RsaSignature()
        sig.ptr = sig_ptr
        return sig

    @classmethod
    def decode(cls, enc, pub):
        """Decodes the given encoded blinding key

        @param enc byte string containing the encoded blinding key
        @param pub the public key for whom this blinding applies
        @return the blinding key
        """
        size = len(enc)
        enc_ptr = (c_byte * size).from_buffer_copy (enc)
        ptr = _rsa_blinding_key_decode (cast(byref(enc_ptr), POINTER(c_void_p)), size)
        if ptr is 0:
            raise Exception("cannot decode blinding key")
        bkey = cls(0, None)
        bkey.ptr = ptr
        bkey.pub = pub
        return bkey

class c_EddsaPrivateKey(Structure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [("data", c_byte * 32)]

class c_EcdsaPrivateKey(Structure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [("data", c_byte * 32)]


class c_EcdhePrivateKey(Structure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [("data", c_byte * 32)]

class c_EcdhePublicKey(Structure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [("data", c_byte * 32)]


class c_EddsaPublicKey(Structure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [("data", c_byte * 32)]


class c_EddsaSignature(Structure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [("rs", c_byte * 64)]


class c_EcdsaPublicKey(Structure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [("data", c_byte * 32)]


class c_EcdsaSignature(Structure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [("rs", c_byte * 64)]

class c_EccSignaturePurpose(BigEndianStructure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [
            ("size", c_uint32),
            ("purpose", c_uint32)
            ]

class c_AbsoluteTime(BigEndianStructure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [("abs_value_us", c_uint64)]


class c_SignKeyIssuePub(BigEndianStructure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [
            ("purpose", c_EccSignaturePurpose),
            ("master_pub", c_EddsaPublicKey),
            ("start", c_AbsoluteTime),
            ("expire", c_AbsoluteTime)
            ]

class c_BlindSigningPublicKey(BigEndianStructure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [("data", c_byte * 32)]

class c_BlindSessionPublicKey(BigEndianStructure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [("r", c_byte * 32)]

class c_BlindEnvelope(BigEndianStructure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [("mh", c_byte * 32)]

class c_BlindEnvelopeSig(BigEndianStructure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [("mh", c_byte * 32)]



class c_BlindingPublicKey(BigEndianStructure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [
            ("r_y", c_byte * 32),
            ]

class c_BlindingPrivateKey(BigEndianStructure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [
            ("a", c_byte * 32),
            ("b", c_byte * 32),
            ("pub", c_BlindingPublicKey)
            ]

class c_BlindSignature(BigEndianStructure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [
            ("pub", c_BlindingPublicKey),
            ("s", c_byte * 32)
            ]


class c_WithdrawSignRequestBody(BigEndianStructure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [
            ("purpose", c_EccSignaturePurpose),
            ("reserve_pub", c_EddsaPublicKey),
            ("blind_session_pub", c_BlindSessionPublicKey),
            ("denom_pub", c_BlindSigningPublicKey),
            ("coin_envelope", c_BlindEnvelope)
            ]

class c_RefreshMeltSignRequestBody(BigEndianStructure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [
            ("purpose", c_EccSignaturePurpose),
            ("melt_hash", c_HashCode),
            ]

class c_RefreshMeltConfirmSignRequestBody(BigEndianStructure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [
            ("purpose", c_EccSignaturePurpose),
            ("session_pub", c_EddsaPublicKey),
            ]

class c_RefreshCommitSignRequestBody(BigEndianStructure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [
            ("purpose", c_EccSignaturePurpose),
            ("commit_hash", c_HashCode),
            ]

class c_Link(BigEndianStructure):
    __slots__ = []
    _pack_ = 1
    _fields_ = [
            ("coin_priv", c_EcdsaPrivateKey),
            ("blinding_priv", c_BlindingPrivateKey)
            ]



eddsa_key_create = gnunet_util.GNUNET_CRYPTO_eddsa_key_create
eddsa_key_create.restype = POINTER(c_EddsaPrivateKey)

eddsa_key_get_public = gnunet_util.GNUNET_CRYPTO_eddsa_key_get_public
eddsa_key_get_public.argtypes = [
        POINTER(c_EddsaPrivateKey), POINTER(c_EddsaPublicKey)]
eddsa_key_get_public.restype = None

ecdsa_key_create = gnunet_util.GNUNET_CRYPTO_ecdsa_key_create
ecdsa_key_create.restype = POINTER(c_EcdsaPrivateKey)

ecdsa_key_get_public = gnunet_util.GNUNET_CRYPTO_ecdsa_key_get_public
ecdsa_key_get_public.argtypes = [
        POINTER(c_EcdsaPrivateKey), POINTER(c_EcdsaPublicKey)]
ecdsa_key_get_public.restype = None

data_to_string = gnunet_util.GNUNET_STRINGS_data_to_string
data_to_string.restype = c_int
data_to_string.argtypes = [c_void_p, c_size_t, c_char_p, c_size_t]

string_to_data = gnunet_util.GNUNET_STRINGS_string_to_data
string_to_data.restype = c_int
string_to_data.argtypes = [c_char_p, c_size_t, c_void_p, c_size_t]

eddsa_sign = gnunet_util.GNUNET_CRYPTO_eddsa_sign
eddsa_sign.restype = c_int
eddsa_sign.argtypes = [POINTER(c_EddsaPrivateKey), c_void_p, POINTER(c_EddsaSignature)]

ecdsa_sign = gnunet_util.GNUNET_CRYPTO_ecdsa_sign
ecdsa_sign.restype = c_int
ecdsa_sign.argtypes = [POINTER(c_EcdsaPrivateKey), c_void_p, POINTER(c_EcdsaSignature)]

_kdf = gnunet_util.GNUNET_CRYPTO_kdf
_kdf.restype = c_int
_kdf.argtypes = [c_void_p, c_size_t, c_void_p, c_size_t, c_void_p, c_size_t]

_symmetric_create_session_key = gnunet_util.GNUNET_CRYPTO_symmetric_create_session_key
_symmetric_create_session_key.restype = None
_symmetric_create_session_key.argtypes = [POINTER(c_SymmetricSessionKey)]


_symmetric_derive_iv = gnunet_util.GNUNET_CRYPTO_symmetric_derive_iv
_symmetric_derive_iv.restype = None
_symmetric_derive_iv.argtypes = [
        POINTER(c_SymmetricInitializationVector),
        POINTER(c_SymmetricSessionKey),
        c_void_p,
        c_size_t
        ]

_symmetric_encrypt = gnunet_util.GNUNET_CRYPTO_symmetric_encrypt
_symmetric_encrypt.restype = c_ssize_t
_symmetric_encrypt.argtypes = [
        c_void_p,
        c_size_t,
        POINTER(c_SymmetricSessionKey),
        POINTER(c_SymmetricInitializationVector),
        c_void_p
        ]

_symmetric_decrypt = gnunet_util.GNUNET_CRYPTO_symmetric_decrypt
_symmetric_decrypt.restype = c_ssize_t
_symmetric_decrypt.argtypes = [
        c_void_p,
        c_size_t,
        POINTER(c_SymmetricSessionKey),
        POINTER(c_SymmetricInitializationVector),
        c_void_p
        ]

_random_block = gnunet_util.GNUNET_CRYPTO_random_block
_random_block.restype = None
_random_block.argtypes = [c_int, c_void_p, c_size_t]

_ecc_ecdh = gnunet_util.GNUNET_CRYPTO_ecc_ecdh
_ecc_ecdh.restype = c_int
_ecc_ecdh.argtypes = [POINTER(c_EcdhePrivateKey), POINTER(c_EcdhePublicKey), POINTER(c_HashCode)]

_refresh_encrypt = taler_util.TALER_refresh_encrypt
_refresh_encrypt.restype = c_int
_refresh_encrypt.argtypes = [c_void_p, c_size_t, POINTER(c_HashCode), c_void_p]

_refresh_decrypt = taler_util.TALER_refresh_decrypt
_refresh_decrypt.restype = c_int
_refresh_decrypt.argtypes = [c_void_p, c_size_t, POINTER(c_HashCode), c_void_p]

free = libc.free
free.restype = None
free.argtypes = [c_void_p]

def sha512(msg):
    """Returns the SHA 512 hash of the given message

    @param msg the message as a byte string
    """
    return hashlib.sha512(msg).digest()

def random_block(size, quality='strong'):
    buf = create_string_buffer(size)
    _random_block(crypto_quality[quality], buf, size)
    return bytes(buf)


def encrypt_link(block, key):
    buf = create_string_buffer(block, len(block))
    h = c_HashCode.from_buffer_copy(key)
    res = _refresh_encrypt(buf, len(block), h, buf)
    if res != len(block):
        raise ValueError("encryption returned wrong size")
    return bytes(buf)


def decrypt_link(block, key):
    buf = create_string_buffer(block, len(block))
    h = c_HashCode.from_buffer_copy(key)
    res = _refresh_decrypt(buf, len(block), h, buf)
    if res != len(block):
        raise ValueError("decryption returned wrong size")
    return bytes(buf)


class SymmetricSessionKey:
    def __init__(self, c_ssk):
        self.c_ssk = c_ssk

    def derive_iv(self, salt, *ctx_chunks):
        if not isinstance(salt, bytes):
            raise TypeError("salt must by 'bytes', not '%s'" % type(salt))
        if not all((isinstance(x, bytes) for x in ctx_chunks)):
            raise TypeError("context chunks must be bytes")
        c_siv = c_SymmetricInitializationVector()
        rest = [(x, c_size_t(len(x))) for x in ctx_chunks]
        # flatten it
        rest = [x for sub in rest for x in sub]
        _symmetric_derive_iv(c_siv, self.c_ssk, salt, c_size_t(len(salt)), *(rest + [None]))
        return c_siv

    @staticmethod
    def create_random():
        c_ssk = c_SymmetricSessionKey()
        _symmetric_create_session_key(c_ssk)
        return SymmetricSessionKey(c_ssk)

    def to_bytes(self):
        return bytes(self.c_ssk)

    def encrypt(self, block, iv):
        buf = create_string_buffer(block, len(block))
        res = _symmetric_encrypt(buf, len(block), self.c_ssk, iv, buf)
        if res != len(block):
            raise ValueError("encryption returned wrong size")
        return bytes(buf)

    def decrypt(self, block, iv):
        buf = create_string_buffer(block, len(block))
        res = _symmetric_decrypt(buf, len(block), self.c_ssk, iv, buf)
        if res != len(block):
            raise ValueError("decryption returned wrong size")
        return bytes(buf)

    @staticmethod
    def derive(salt, skm, *ctx_chunks):
        buf = kdf(sizeof(c_SymmetricSessionKey), salt, skm, *ctx_chunks)
        c_ssk = c_SymmetricSessionKey.from_buffer_copy(buf)
        return SymmetricSessionKey(c_ssk)


def kdf(out_len, salt, skm, *ctx_chunks):
    buf = create_string_buffer(out_len)
    rest = [(x, c_size_t(len(x))) for x in ctx_chunks]
    # flatten it
    rest = [x for sub in rest for x in sub]
    r = _kdf(buf, out_len, salt, len(salt), skm, len(skm), *(rest + [None]))
    if r != 1:
        raise ValueError("kdf failed")
    return bytes(buf)


def data_encode(data):
    l = len(data) * 8
    if l % 5 > 0:
        l += 5 - l % 5
    l //= 5
    buf = create_string_buffer (l)
    res = data_to_string(data, len(data), buf, l)
    if res is None:
        raise ValueError("encoding failed")
    return bytes(buf).decode("ascii")


def data_decode(enc):
    if isinstance(enc, str):
        enc = enc.encode('ascii', 'ignore')
    l = len(enc)
    buf = create_string_buffer(l * 5 // 8)
    res = string_to_data(enc, l, buf, len(buf))
    if res != 1:
        raise ValueError("Decoding failed")
    return bytes(buf)


class EddsaPrivateKey:
    def __init__(self, data):
        self.data = data

    def get_public_key(self):
        c_priv = c_EddsaPrivateKey.from_buffer_copy(self.data)
        c_pub = c_EddsaPublicKey()
        eddsa_key_get_public (c_priv, c_pub)
        return EddsaPublicKey(bytes(c_pub))

    @staticmethod
    def create():
        priv = eddsa_key_create ()
        return EddsaPrivateKey(bytes(priv.contents))



class EddsaPublicKey:
    def __init__(self, data):
        self.data = data

    @staticmethod
    def from_string_encoding(enc):
        if isinstance(enc, str):
            enc = enc.encode('ascii', 'ignore')
        pub = c_EddsaPublicKey()
        data = data_decode(enc)
        if len(data) != 32:
            raise Exception("invalid key format")
        return EddsaPublicKey(data)


class EcdsaPrivateKey:
    def __init__(self, data):
        self.data = data

    def get_public_key(self):
        c_priv = c_EcdsaPrivateKey.from_buffer_copy(self.data)
        c_pub = c_EcdsaPublicKey()
        ecdsa_key_get_public (c_priv, c_pub)
        return EcdsaPublicKey(bytes(c_pub))

    @staticmethod
    def create():
        priv = ecdsa_key_create ()
        return EcdsaPrivateKey(bytes(priv.contents))


class EcdsaPublicKey:
    def __init__(self, data):
        self.data = data

    @staticmethod
    def from_string_encoding(enc):
        if isinstance(enc, str):
            enc = enc.encode('ascii', 'ignore')
        pub = c_EcdsaPublicKey()
        data = data_decode(enc)
        if len(data) != 32:
            raise Exception("invalid key format")
        return EcdsaPublicKey(data)

@total_ordering
class Amount:
    def __init__(self, string_or_val, fraction=None, currency=None):
        if isinstance(string_or_val, str):
            if ":" in string_or_val:
                curr, rest = string_or_val.split(":", 1)
            elif currency is not None:
                curr = currency
                rest = string_or_val
            else:
                raise ValueError("No currency in amount")
            self.currency = curr
            if "." in rest:
                pre, post = rest.split(".")
                try:
                    self.value = int(pre)
                except ValueError:
                    raise ValueError("Invalid value for amount (before period)")
                self.fraction = 0
                v = 100000
                for x in post:
                    if v == 0:
                        raise ValueError("Insufficient Precision")
                    try:
                        n = int(x)
                    except ValueError:
                        raise ValueError("Invalid value for amount (after period)")
                    self.fraction += n * v
                    v = v // 10
            else:
                self.fraction = 0
                self.value = int(rest)
        elif isinstance(string_or_val, int):
            self.value = string_or_val
            self.fraction = int(fraction)
            self.currency = currency
        else:
            raise ArgumentError()

        if len(self.currency) != 3:
            raise ValueError("currency must by 3 characters long")

    def __repr__(self):
        return "<Amount currency='%s', value=%s, fraction=%s>" % (self.currency, self.value, self.fraction)

    def __lt__(self, other):
        return (self.value, self.fraction) < (other.value, other.fraction)

    def __eq__(self, other):
        return (self.value, self.fraction) == (other.value, other.fraction)

    def __add__(self, other):
        a = Amount(self.value + other.value, self.fraction + other.fraction,
                self.currency)
        a.normalize()
        return a

    def normalize(self):
        while self.fraction >= 1000000:
            self.fraction -= 1000000
            self.value += 1

    def __str__(self):
        val = self.value
        frac = self.fraction
        while frac >= 1000000:
            frac -= 1000000
            val += 1
        if frac:
            frac_str = ""
            while frac != 0:
                frac_str += str(frac // 100000)
                frac = (frac * 10) % 1000000
            return "%s:%s.%s" % (self.currency, val, frac_str)
        else:
            return "%s:%s" % (self.currency, val)
