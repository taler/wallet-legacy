"""
  This file is part of TALER
  Copyright (C) 2014, 2015 Christian Grothoff (and other contributing authors)

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>

  @author Florian Dold
  @author Benedikt Muller
  @author Sree Harsha Totakura
"""

from ctypes import *

gcrypt = CDLL("libgcrypt.so")


GCRY_KDF_SCRYPT = 48

_kdf_derive = gcrypt.gcry_kdf_derive
_kdf_derive.restype = c_uint
_kdf_derive.argtypes = [
        # pw, pwlen
        c_void_p, c_size_t,
        # algo, subalgo
        c_int, c_int,
        # salt, saltlen
        c_void_p, c_size_t,
        # iterations
        c_ulong,
        # keysize, keybuffer
        c_size_t, c_void_p
        ]

def scrypt(out_len, pw, salt, N=10, p=1, r=8):
    if not isinstance(pw, bytes):
        raise ArgumentError("pw must be of type 'bytes'")
    if not isinstance(salt, bytes):
        raise ArgumentError("salt must be of type 'bytes'")
    if r != 8:
        raise Exception("only r=8 is supported by libgcrypt")
    buf = create_string_buffer(out_len)
    r = _kdf_derive(pw, len(pw), GCRY_KDF_SCRYPT, N, salt, len(salt), p,
            c_size_t(out_len), buf)
    if r != 0:
        raise Exception("scrypt kdf failed")
    return bytes(buf)
