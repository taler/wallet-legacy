/*
  This file is part of TALER
  Copyright (C) 2015, 2016 INRIA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>

  @author Marcello Stanisci
*/

function startup(data, reason) {

  /**
   * do not load the extension automatically,
   * in order to not poison the cache and allow to manually
   * install the latest version (which happens when reason == APP_INSTALL)
   * from developer's working directory
   */
  if (APP_STARTUP == reason)
    return;
  // start-up code here
  Components.utils.import("chrome://taler-button/content/lib/debug.jsm");
  tConsoleLog("bootstrap");
};

function shutdown(data, reason) {
  // uninstall the wallet
};

function install(data, reason) {};
function uninstall(data, reason) {};
