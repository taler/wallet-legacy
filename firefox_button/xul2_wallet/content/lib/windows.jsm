/*
  This file is part of TALER
  Copyright (C) 2015, 2016 INRIA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
*/

/**
 * @file functions mainly addressed to get 'handles' to the active
 * windows and tabs
 * @author Marcello Stanisci
 */

/**
 * Iterate over all the chrome open 'window'(s) and applies a callback
 * to them
 *
 * @param todo - callback to call. This callback expects at least 'windows'
 * among its parameters
 * @param arg - (extra) argument to pass to `todo`
 */
function forEachOpenWindow(todo, arg) {
  var windows = Services.wm.getEnumerator("navigator:browser");
  while (windows.hasMoreElements()) {
    todo(windows.getNext().QueryInterface(Components.interfaces.nsIDOMWindow), arg);
  }
}
