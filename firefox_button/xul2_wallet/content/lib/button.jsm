/*

  This file is part of TALER
  Copyright (C) 2015, 2016 INRIA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>

*/

/**
 * @file main definitions concerning the 'button'
 * @author Marcello Stanisci
 */
var EXPORTED_SYMBOLS = ['buttonHelpers'];
/**
 * Container of "secondary" definitions around the button
 *
 * @typedef ButtonHelpers
 *
 * @type {object}
 *
 * @property {function} applyStyle - apply CSS meant to style the button
 * @property {function} applyStyle - apply CSS meant to style the button
 */
let buttonHelpers = {
  /**
   * Apply CSS to the browser
   *
   * @param {string[]} styles - URIs of each CSS to apply, typically
   * pointing within the extension's tree
   * @param {number} action - indicates whether the style needs to be
   * loaded or unloaded
   */
  applyStyle: function(styles, action){
    let styleSheetService =
      Components.classes["@mozilla.org/content/style-sheet-service;1"]
      .getService(Components.interfaces.nsIStyleSheetService);
    for (let i = 0, len = styles.length; i < len; i++) {
      let styleSheetURI = Services.io.newURI(styleSheets[i], null, null);
    if (EXTENSION_LOAD == action)
      styleSheetService
      .loadAndRegisterSheet(styleSheetURI, styleSheetService.AUTHOR_SHEET);
    else if (EXTENSION_UNLOAD == action)
      if (styleSheetService
          .sheetRegistered(styleSheetURI, styleSheetService.AUTHOR_SHEET))
        styleSheetService
	.unregisterSheet(styleSheetURI, styleSheetService.AUTHOR_SHEET);
    }
  },
  /**
   * Make the button appear on the toolbar
   *
   * @param {object} button - Taler button, already instantiated (also
   * meaning that it's already part of document's children)
   * @param {objec} window - chrome 'window' to which attach the button
   */
  injectButton: function(button, window){
    let toolbox = window.document.querySelector("#navigator-toolbox");
    toolbox.palette.appendChild(button);
    let navBar = window.document.querySelector('#nav-bar');
    navBar.insertItem("taler-button");
};

/**
 * Container of Taler button's main component. Mainly associating a button
 * with the chrome window it is attached to. FIXME This paragraph should
 * actually document the class it gives birth to
 *
 * @typedef TalerButton
 *
 * @type {object}
 *
 * @property {object} button - DOM object representin a Taler button
 * @property {object} window - the chrome window where `button` is
 * attached to
 */
function TalerButton(cw){ //XXX this is the Class constructor
  // get handle to syntactical button
  // instantiate it
  // this.button = button DOM
  // this.window = cw
}
