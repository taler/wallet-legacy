/*
  This file is part of TALER
  Copyright (C) 2015, 2016 INRIA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>

*/

/**
 * @file Chrome related listeners and handles getters
 * @author Marcello Stanisci
 */

Components.utils.import("resource://gre/modules/Services.jsm");
//Components.utils.import("chrome://taler-button/content/taler.jsm");

/**
 * Returns the (browser's) window on top of the user's screen
 *
 * @return {object} the 'window' chrome DOM element associated with the
 * last seen window. This window should have the last seen HTML document
 * in its "content" property
 */
function getLastWindow() {
  let window = Services.wm.getMostRecentWindow("navigator:browser");
  return window;
}

/**
 * Container of chrome "hooks", namely facilities to get handles
 * to both existing chrome windows and potential ones
 *
 * @typedef ChromeHooks
 *
 * @type {object}
 *
 * @property {function} forEachWindow - loops over each existing chrome
 * window
 * @property {function} forEachPotentialWindow - register a callback to
 * be called with the new chrome window as argument TODO catch CTRL-N event
 * and react accordingly
 */
let chromeHooks = {
  /**
   * @param todo - callback to call. This callback expects at least 'windows'
   * among its parameters
   * @param arg - (extra) argument to pass to `todo`
   */
  forEachOpenWindow: function(cb, arg){
    let windows = Services.wm.getEnumerator("navigator:browser");
    while (windows.hasMoreElements()) {
      cb(windows.getNext()
          .QueryInterface(Components.interfaces.nsIDOMWindow),
	  arg);
    }
  },
  forEachPotentialWindow: function(cb){
    let listener = {
      onOpenWindow: function(cw) {
        let window =
        cw.QueryInterface(Components.interfaces.nsIInterfaceRequestor)
        .getInterface(Components.interfaces.nsIDOMWindow);
        let onFullyLoad = function(){
          window.removeEventListener("load", onWindowLoad);
          if (window.document.documentElement.getAttribute("windowtype")
              == "navigator:browser") cb(window);
        };
      window.addEventListener("load", onFullyLoad);},
      onCloseWindow: function(cw) {/*TODO?*/},
      onWindowTitleChange: function(cw, newTitle) {/*TODO?*/}
    };
    // register the listener object
    Services.wm.addListener(listener);
  }
};
