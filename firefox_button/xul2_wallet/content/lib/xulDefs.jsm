/*
  This file is part of TALER
  Copyright (C) 2015, 2016 INRIA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
*/

/**
 * @file Definitions concerning XUL, mostly related to `xul.js` library
 * @author Marcello Stanisci
 */
var EXPORTED_SYMBOLS = ['buttonConstants'];

/**
 * Library implementing an alternative syntax to create XUL nodes - it saves
 * us from repetitively typing 'createNode + appendChild'. (note: it's NOT possible
 * to put this import in this file's global scope - chrome.manifest is still not
 * properly loaded)
 */
Components.utils.import("chrome://taler-button/content/lib/xul.jsm");

defineTags(
  'panel', 'vbox', 'hbox', 'description',
  'label', 'textbox', 'button', 'menu',
  'toobarbutton', 'menupopup', 'menuitem',
  'script', 'menuseparator'
);

const {
  PANEL, VBOX, HBOX, DESCRIPTION,
  LABEL, TEXTBOX, BUTTON, MENU,
  TOOLBARBUTTON, MENUPOPUP, MENUITEM,
  SCRIPT, MENUSEPARATOR, PROGRESSMETER
} = Xul;

/**
 * Container of constants useful to the Taler button
 *
 * @typedef ButtonConstants
 *
 * @type {object}
 *
 * @property {object} button - syntactical definition of the button, see
 * library `xul.js`
 * @property {string[]} styles - URIs of each button's CSS
 */
var buttonConstants = {
  button: TOOLBARBUTTON(tButtonAttrs,
    MENUPOPUP(

      MENU({
          'label': "Test"
        },
        MENUPOPUP(

          MENUITEM({
            'label': "Run /test battery",
            'id': 'taler-battery-test'
          }),


          MENUITEM({
            'label': "64-bit test",
            'id': '64bit-testing'
          }),

          MENUITEM({
            'label': "base32",
            'id': 'taler-base32-id'
          }),
          MENUITEM({
            'label': "Make signature purpose",
            'id': 'taler-sigpurp-test'
          }),

          MENUITEM({
            'label': "encrypt",
            'id': 'taler-encrypt-id'
          }),
          MENUITEM({
            'label': "HKDF",
            'id': 'taler-hkdf-id'
          }),
          MENUITEM({
            'label': "ECDHE",
            'id': 'taler-ecdhe-id'
          }),
          MENUITEM({
            'label': "EDDSA",
            'id': 'taler-eddsa-id'
          }),
          MENUITEM({
            'label': "RSA",
            'id': 'taler-rsa-id'
          }),
          MENUITEM({
            'label': "transfer",
            'id': 'taler-transfer-id'
          }),
          MENUITEM({
            'label': "Show time and date",
            'id': 'taler-tnd-id'
          })
        )
      ),
      MENU({
          'label': "Dev tools"
        },
        MENUPOPUP(
          MENUITEM({
            'label': "attach progress meter",
            'id': 'prog-meter-test'
          }),
          MENUITEM({
            'label': "get mint keys",
            'id': 'taler-keys-test'
          }),
          MENUITEM({
            'label': "pay test",
            'id': 'pay-test'
          }),
          MENUITEM({
            'label': "new pay test",
            'id': 'new-pay-test'
          }),
          MENUITEM({
            'label': "wipe coins",
            'id': 'wipe-coins'
          }),
          MENUITEM({
            'label': "wipe reserves",
            'id': 'wipe-reserves'
          }),
          MENUITEM({
            'label': "wipe keys",
            'id': 'wipe-keys'
          })
        )
      )
    ),
  styles: ["chrome://taler-button/skin/taler.css"]
};


