#  This file is part of TALER
#  Copyright (C) 2015, 2016 INRIA
#
#  TALER is free software; you can redistribute it and/or modify it under the
#  terms of the GNU General Public License as published by the Free Software
#  Foundation; either version 3, or (at your option) any later version.
#
#  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
#  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with
#  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
#
#  @author Marcello Stanisci
#  @author Christian Grothoff

# The name of the extension.
ext_name := taler-wallet

# The UUID of the extension.
ext_id := $(shell grep em:id install.rdf | grep -o '[^>]*@[^<]*')

ext_ver := $(shell grep em:version install.rdf | grep -o '[0-9.]\+')

# The name of the profile dir where the extension can be installed.
profile_dir := taler

# The zip application to be used.
ZIP := zip

# The target location of the build and build files.
bin_dir := xpi

# The target XPI file.
xpi_file := $(bin_dir)/$(ext_name)-$(ext_ver).xpi

# The type of operating system this make command is running on.
os_type := $(patsubst darwin%,darwin,$(shell echo $(OSTYPE)))

# The location of the extension profile.
#ifeq ($(os_type), darwin)
#  profile_path := \
#    ~/Library/Application\ Support/Firefox/Profiles/$(profile_dir)/extensions/\{$(ext_id)\}
#else
#  ifeq ($(os_type), linux-gnu)
#    profile_path := \
#      ~/.mozilla/firefox/$(profile_dir)/extensions/$(ext_id)
#  else
#    profile_path := \
#      "$(subst \,\\,$(APPDATA))\\Mozilla\\Firefox\\Profiles\\$(profile_dir)\\extensions\\{$(ext_id)}"
#  endif
#endif

profile_path := ~/.mozilla/firefox/$(profile_dir)/extensions/$(ext_id)

# The temporary location where the extension tree will be copied and built.
build_dir := $(bin_dir)/build

# This builds the extension XPI file.
.PHONY: all
all: $(xpi_file)
	@echo
	@echo "Build finished successfully."
	@echo

# This cleans all temporary files and directories created by 'make'.
.PHONY: clean
clean:
	@rm -rf $(build_dir)
	@rm -f $(xpi_file)
	@echo "Cleanup is done."

# This cleans all the files installed under "taler" profile.
.PHONY: ditsclean
distclean:
	@echo Removing $(profile_path)
	@rm -rf $(profile_path)
	@echo "Extension not anymore installed."

# The sources for the XPI file.
xpi_built := install.rdf \
             chrome.manifest \
	     bootstrap.js \
             $(wildcard content/*.xul) \
             $(wildcard content/*.css) \
             $(wildcard content/*.js) \
             $(wildcard content/*.jsm) \
             $(wildcard content/lib/*.jsm) \
             $(wildcard content/data/*.png) \
             $(wildcard skin/*.css) \
             $(wildcard skin/*.png) \
             $(wildcard locale/*/*.properties)

# This builds everything except for the actual XPI, and then it copies it to the
# specified profile directory, allowing a quick update that requires no install.
.PHONY: install
install: $(xpi_built)
	@echo "Installing in profile folder: $(profile_path)"
	@mkdir -p $(profile_path)
	@cp --parent -Rf -t $(profile_path) $(xpi_built)
	@echo "Installing in profile folder. Done!"
	@echo

.PHONY: run
run: $(xpi_file)
	@echo "Injecting (http post) $(xpi_file) in localhost 8888"
	wget --post-file=$(xpi_file) http://127.0.0.1:8888

$(xpi_file): $(xpi_built)
	@echo "Creating XPI file."
	-@mkdir -p $(bin_dir)
	@$(ZIP) $(xpi_file) $(xpi_built)
	@cd $(bin_dir) && ln -sf $(ext_name)-$(ext_ver).xpi $(ext_name).xpi
	@echo "Creating XPI file. Done!"
