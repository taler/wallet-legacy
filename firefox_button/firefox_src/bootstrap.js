/*
  This file is part of TALER
  Copyright (C) 2015, 2016 INRIA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>

  @author Marcello Stanisci
*/

// future use
//const internalXMLHttpRequest = Components.Constructor("@mozilla.org/xmlextras/xmlhttprequest;1",




function startup(data, reason) {

  /* do not load the extension automatically,
    in order to not poison the cache and allow to manually
    install the latest version (which happens when reason == APP_INSTALL)
    from developer's working directory */
  if (APP_STARTUP == reason)
    return;

  Components.utils.import("chrome://taler-button/content/lib/windowMgmt.jsm");
  Components.utils.import("chrome://taler-button/content/lib/prettyPrint.jsm");
  Components.utils.import("resource://gre/modules/Services.jsm");
  Components.utils.import("chrome://taler-button/content/taler.jsm");

  /* CustomizableUI used to create toolbar button */
  // not yet: Components.utils.import('resource:///modules/CustomizableUI.jsm');

  // main init() function
  Taler.Wallet.initWallet();
  Taler.Wallet.notifyPresence();
};

function shutdown(data, reason) {

  Taler.Wallet.notifyAbsence();
  Taler.Wallet.haltWallet();

  Components.utils.unload("chrome://taler-button/content/taler.jsm");
  Components.utils.unload("chrome://taler-button/content/windowMgmt.jsm");
  Components.utils.unload("chrome://taler-button/content/prettyPrint.jsm");
  Components.utils.unload("chrome://taler-button/content/actions.jsm");
  Components.utils.unload("chrome://taler-button/content/chromeTransform.jsm");
  Components.utils.unload("chrome://taler-button/content/xul.jsm");
  Components.utils.unload("chrome://taler-button/content/io.jsm");
  Components.utils.unload("chrome://taler-button/content/util.jsm");
  //Components.utils.unload("resource://gre/modules/FileUtils.jsm");

  // HACK WARNING: The Addon Manager does not properly clear all addon related caches on update;
  //               in order to fully update images and locales, their caches need clearing here
  Services.obs.notifyObservers(null, "chrome-flush-caches", null);


};


function install(data, reason) {


};

function uninstall(data, reason) {};
