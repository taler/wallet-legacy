/*
  This file is part of TALER
  Copyright (C) 2015, 2016 INRIA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>

  @author Marcello Stanisci
*/

/* To be run inside Firefox Scratchpad (Shift+F4). Open
  the Web Console to see the output (Ctrl+Shitf+k) */

/* Implementing an increment operator for arrays of
  ('Point'-like objects containing) integers. For simplicity,
  say that a 'point'-like object is a pair X.Y where 'X' is the
  current count of it, and 'Y' is the maximal allowed count.
  So, given an array of points like [2.5, 4.6, 5.5, 31.32],
  then incr([2.5, 4.6, 5.5, 31.32]) = [2.5, 4.6, 5.5, 32.32],
  and incr(incr([2.5, 4.6, 5.5, 31.32])) = [2.5, 5.6, 5.5, 32.32] */

function Point (w, v, maxNo) {
  this.weight = w;
  this.value = v;
  this.count = 0;
  this.max = maxNo;
}

let itemMaxNo = 200;
let itemTypes = 20;
let points = new Array (itemTypes);
for (let i = 0; i < points.length; i++)  
  points[i] = new Point (i, i, itemMaxNo);
// console.log (JSON.stringify (points));

/* Incrementator */
function incr (arr) {
  /* 'pop()' has also the effect of deleting the last
    element from the array */
  let last = arr.pop ();
  if (typeof last === "undefined")
    return arr;
  
  if (last.count < last.max) {
    last.count++;
  }
  else
    arr = incr (arr);
    
  arr.push (last);
  return arr;    
}

function test (noIter){
  let tick = [];
  for (let k = 0; k < noIter; k++)
    tick = incr (points);
  let str = "[";
  for (let l = 0; l < tick.length; l++){
    if (l + 1 == tick.length)
      str = str + tick[l].count;
    else
      str = str + tick[l].count + ", ";  
  }
  str = str + "]";
  console.log (str);
}

/* test-ish noIter */
let middle = Math.floor (Math.random ()
                         * (points.length * itemMaxNo));
let zero = 0;
let one = 1;
let full = points.length * itemMaxNo;

/* Uncomment one at a time, since the array passed
  to 'incr()' is stateful */

//test (zero);
//test (one);
//test (full - 1);
//test (full);
//test (full + 1);























