/*
  This file is part of TALER
  Copyright (C) 2015, 2016 INRIA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>

  @author Marcello Stanisci
*/
function onContractLoad(){

  /* contract object (parsed earlier from JSON) */
  let contract = window.arguments[0].ctrObj;
  let listRoot = document.getElementById("a-taler-contract-list");

  Object.keys(contract).forEach(

  function(property){

    let row = document.createElement("row");
    let left = document.createElement("label");
    left.setAttribute("value", property + ": ");
    let right = document.createElement("label");
    right.setAttribute("value", contract[property]);
    row.appendChild(left);
    row.appendChild(right);
    listRoot.appendChild(row);
  });

};

function onCancel(){
  window.arguments[0].choice = "canceled";
};

function onPay(){
  /* record what the user has done (to be read in the
    opener window) */
  window.arguments[0].choice = "paid";
};
