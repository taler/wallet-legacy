/*
  This file is part of TALER
  Copyright (C) 2015, 2016 INRIA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>

  @author Marcello Stanisci
*/

/* Return a 'menulist' based on the array  of mints'
  URLs given as argument. The given array must mention only
  those mints which accept the currency chosen for the
  withdrawal */

function generateMintsList(mintsList){

  let menulist = document.createElement("menulist");
  menulist.setAttribute("editable", "true");
  menulist.setAttribute("id", "mints-picklist");
  let menupopup = document.createElement("menupopup");
  menulist.appendChild(menupopup);

  function addItem(item, index, array){
    let menuitem = document.createElement("menuitem");
    menuitem.setAttribute("value", item);
    menuitem.setAttribute("label", item);
    menupopup.appendChild(menuitem);
  };

  mintsList.forEach(addItem);

  menulist.value = "mint.demo.taler.net";
  return menulist;
}

function onWithdrawDialogLoad(){

  /* contract object (parsed earlier from JSON) */
  let mintsList = window.arguments[0].mints;
  let content = document.getElementById('content');

  if(!mintsList){
    let label = document.createElement("label");
    label.setAttribute("value", "Error while preparing reserve data");
    content.appendChild(label);
  }
  else
    content.appendChild(generateMintsList(mintsList));
};

function onCancel(){
  window.arguments[0].chosenMint = "canceled";
};

function onConfirm(){
 /* return to the called the chosen mint */
  if(window.arguments[0].mints){
    let menulist = document.getElementById('mints-picklist');
    //window.opener.alert(menulist.value);
    window.arguments[0].chosenMint = menulist.getAttribute("value");
  }
};
