/*

  This file is part of TALER
  Copyright (C) 2015, 2016 INRIA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>

*/

/**
 * @file Grouping of XUL manipulators, mostly intended to change the button's
 * appearance
 * @author Marcello Stanisci
 */

var EXPORTED_SYMBOLS = [
  'injectButton',
  'defineTalerButton',
  'manageTalerStyles',
  'applyButton',
  'changeButtonColor',
  'attachProgressMeter',
  'removeProgressMeter'
];

Components.utils.import("resource://gre/modules/Services.jsm");
Components.utils.import("chrome://taler-button/content/lib/reserves.jsm");
Components.utils.import("chrome://taler-button/content/lib/keys.jsm");
Components.utils.import("chrome://taler-button/content/lib/pay.jsm");
Components.utils.import("chrome://taler-button/content/lib/test_api.jsm");
Components.utils.import("chrome://taler-button/content/lib/windowMgmt.jsm");
Components.utils.import("chrome://taler-button/content/lib/prettyPrint.jsm");
Components.utils.import("chrome://taler-button/content/lib/util.jsm");

/**
 * Library implementing an alternative syntax to create XUL nodes - it saves
 * us from repetitively typing 'createNode + appendChild'. (note: it's NOT possible
 * to put this import in this file's global scope - chrome.manifest is still not
 * properly loaded)
 */
Components.utils.import("chrome://taler-button/content/lib/xul.jsm");

defineTags(
  'panel', 'vbox', 'hbox', 'description',
  'label', 'textbox', 'button', 'menu',
  'toobarbutton', 'menupopup', 'menuitem',
  'script', 'menuseparator'
);

const {
  PANEL, VBOX, HBOX, DESCRIPTION,
  LABEL, TEXTBOX, BUTTON, MENU,
  TOOLBARBUTTON, MENUPOPUP, MENUITEM,
  SCRIPT, MENUSEPARATOR, PROGRESSMETER
} = Xul;


/**
 * Define the XUL object which will be the button once instantiated
 */

function defineTalerButton() {

  var tButtonAttrs = {
    id: "taler-button",
    label: "Taler",
    type: "menu",
    class: "toolbarbutton-1 chromeclass-toolbar-additional",
    tooltiptext: "Taler wallet"
  };

  var newButton = TOOLBARBUTTON(tButtonAttrs,
    MENUPOPUP(

      /*
      MENUITEM({
        'label': "Create reserve",
        'id': 'reserve'
      }),
      MENUITEM({
        'label': "Show transactions history",
        'id': 'trn-history'
      }),*/

      MENU({
          'label': "Test"
        },
        MENUPOPUP(

          MENUITEM({
            'label': "Run /test battery",
            'id': 'taler-battery-test'
          }),


          MENUITEM({
            'label': "64-bit test",
            'id': '64bit-testing'
          }),

          MENUITEM({
            'label': "base32",
            'id': 'taler-base32-id'
          }),
          MENUITEM({
            'label': "Make signature purpose",
            'id': 'taler-sigpurp-test'
          }),

          MENUITEM({
            'label': "encrypt",
            'id': 'taler-encrypt-id'
          }),
          MENUITEM({
            'label': "HKDF",
            'id': 'taler-hkdf-id'
          }),
          MENUITEM({
            'label': "ECDHE",
            'id': 'taler-ecdhe-id'
          }),
          MENUITEM({
            'label': "EDDSA",
            'id': 'taler-eddsa-id'
          }),
          MENUITEM({
            'label': "RSA",
            'id': 'taler-rsa-id'
          }),
          MENUITEM({
            'label': "transfer",
            'id': 'taler-transfer-id'
          }),
          MENUITEM({
            'label': "Show time and date",
            'id': 'taler-tnd-id'
          })
        )
      ),
      MENU({
          'label': "Dev tools"
        },
        MENUPOPUP(
          MENUITEM({
            'label': "attach progress meter",
            'id': 'prog-meter-test'
          }),
          MENUITEM({
            'label': "get mint keys",
            'id': 'taler-keys-test'
          }),
          MENUITEM({
            'label': "pay test",
            'id': 'pay-test'
          }),
          MENUITEM({
            'label': "new pay test",
            'id': 'new-pay-test'
          }),
          MENUITEM({
            'label': "wipe coins",
            'id': 'wipe-coins'
          }),
          MENUITEM({
            'label': "wipe reserves",
            'id': 'wipe-reserves'
          }),
          MENUITEM({
            'label': "wipe keys",
            'id': 'wipe-keys'
          })
        )
      )
    )
  );

  return newButton;
};

/**
 * Inject a Taler button into the given page
 *
 * @param {object} buttonToInject - XUL definition of a Taler button
 * @param {object} xulWin - the chrome document which will host this
 * button
 */
function injectButton(buttonToInject, xulwin) {

 /* pick up the 'palette': palette will be a field named 'palette' inside 'var toolbox'
  not yet, it might be ways more short and recommended using CustomizableUI in order
  to add a button to the toolbar which is draggable by the user */

  let toolbox = xulwin.document.querySelector("#navigator-toolbox");

  tDebug("inj palette is : " + toolbox.palette.nodeName, false);
  let tmp = buttonToInject.build(xulwin.document); 
  tDebug("button's name : " + tmp.nodeName, false);
  toolbox.palette.appendChild(tmp);

  let navBar = xulwin.document.querySelector('#nav-bar');
  navBar.insertItem("taler-button");
  //navBar.removeChild(tmp);

};

/**
 * Associate the Taler CSS style with the button (and other visual elements)
 *
 * @param {string} action - "load" of "unload" depending on which is the
 * situation
 */
function manageTalerStyles(action) {

  var styleSheets = ["chrome://taler-button/skin/taler.css"];
  let styleSheetService = Components.classes["@mozilla.org/content/style-sheet-service;1"]
    .getService(Components.interfaces.nsIStyleSheetService);

  for (let i = 0, len = styleSheets.length; i < len; i++) {
    let styleSheetURI = Services.io.newURI(styleSheets[i], null, null);

    if (action == "load")
      styleSheetService.loadAndRegisterSheet(styleSheetURI, styleSheetService.AUTHOR_SHEET);
    else

    if (action == "unload")
      if (styleSheetService.sheetRegistered(styleSheetURI, styleSheetService.AUTHOR_SHEET))
        styleSheetService.unregisterSheet(styleSheetURI, styleSheetService.AUTHOR_SHEET);

  }

};


/**
 * Make the button appear next to the addressbar, and enriches its menu
 * at the same time
 * 
 * @param {object} xulWin - chrome 'document' DOM which will host this button
 * @param {object} button - XUL defined button
 */
function applyButton(xulWin, button) {

  // actual extension's injection
  injectButton(button, xulWin);

  // binding extension's buttons with dedicated functions in our core library

  // get a reference to each button
  // var testButton = xulWin.document.getElementById('testcase');
  //var trnHistBut = xulWin.document.getElementById('trn-history');
  //var createResBut = xulWin.document.getElementById('reserve');
  var tndBut = xulWin.document.getElementById('taler-tnd-id');
  var crockBut = xulWin.document.getElementById('taler-base32-id');
  var hkdfBut = xulWin.document.getElementById('taler-hkdf-id');
  var hashBut = xulWin.document.getElementById('taler-hash-id');
  var eddsaBut = xulWin.document.getElementById('taler-eddsa-id');
  var ecdheBut = xulWin.document.getElementById('taler-ecdhe-id');
  var encryptBut = xulWin.document.getElementById('taler-encrypt-id');
  var rsaBut = xulWin.document.getElementById('taler-rsa-id');
  var keysBut = xulWin.document.getElementById('taler-keys-test');
  var sigpurpBut = xulWin.document.getElementById('taler-sigpurp-test');
  var batteryBut = xulWin.document.getElementById('taler-battery-test');
  var bitBut = xulWin.document.getElementById('64bit-testing');
  var wipeBut = xulWin.document.getElementById('wipe-reserves');
  var wipeKBut = xulWin.document.getElementById('wipe-keys');
  var clrcoBut = xulWin.document.getElementById('wipe-coins');
  var payBut = xulWin.document.getElementById('pay-test');
  var newPayBut = xulWin.document.getElementById('new-pay-test');
  var progMeterBut = xulWin.document.getElementById('prog-meter-test');

  /* Attach the dedicated handler for each button
   FIXME It may be possible that 'evt.view' points the last window, thus
   saving us from importing 'windowMgmt.jsm' for calling its 'getLastWindow()'
   from actions.jsm.
   (See https://developer.mozilla.org/en-US/docs/Web/Events/command) */

  /*trnHistBut.addEventListener('command', function(evt) {
    trnsHistory();
  });*/

  /*createResBut.addEventListener('command', function(evt) {
    createReserve();
  });*/

  progMeterBut.addEventListener('command', function(evt) {
    attachProgressMeter();
    getLastWindow().setTimeout(function(){
      removeProgressMeter();}, 10000);
  });
  newPayBut.addEventListener('command', function(evt) {
    testNewPayment("http://mint.demo.taler.net/", "KUDOS"); 
  });
  payBut.addEventListener('command', function(evt) {
    testPayment("mint.demo.taler.net", "KUDOS"); 
  });
  tndBut.addEventListener('command', function(evt) {
    dateAndTime();
  });
  crockBut.addEventListener('command', function(evt) {
    base32test();
  });
  hkdfBut.addEventListener('command', function(evt) {
    hkdfTest();
  });
  eddsaBut.addEventListener('command', function(evt) {
    eddsaTest();
  });
  ecdheBut.addEventListener('command', function(evt) {
    ecdheTest();
  });
  encryptBut.addEventListener('command', function(evt) {
    encryptTest();
  });
  rsaBut.addEventListener('command', function(evt) {
    rsaTest();
  });
  sigpurpBut.addEventListener('command', function(evt) {
    makePurposeTest();
  });
  keysBut.addEventListener('command', function(evt) {
    obtainKeys("http://mint.demo.taler.net/", (arg)=>{}, null, populateKeysDB);
    //obtainKeys("http://mint.demo.taler.net/", (arg)=>{}, null, populateKeysDBPromises);
  });
  batteryBut.addEventListener('command', function(evt) {
    (talerTest());
  });

  bitBut.addEventListener('command', function(evt) {
    (test64bit());
  });

  wipeBut.addEventListener('command', function(evt) {
    wipeReserves();
  });

  wipeKBut.addEventListener('command', function(evt) {
    wipeKeys();
  });

  clrcoBut.addEventListener('command', function(evt) {
    wipeCoins();
  });

}

var color = "background:#99FF66; \
	   margin: 4px 4px 4px 4px; \
	   padding: 4px; \
	   background-clip:content-box;";

/**
 * Change button's color
 * 
 * @param {bool} talerable - true to get the button green, false
 * to get it grey
 */
function changeButtonColor(talerable) {
  var button = getLastWindow().document.getElementById("taler-button");
  if (talerable)
    button.setAttribute("style", color);
  else
    button.removeAttribute("style");
}

/**
 * Attach a progress meter to the button
 */
function attachProgressMeter(){
  let doc = getLastWindow().document;
  let talerButton = doc.getElementById("taler-button");
  tDebug("chrome:" + talerButton, true);
  let panelAttrs = {
    id: "withdraw-meter",
    type: "arrow",
    noautohide: true
  };
  let progMeterAttrs = {
    mode: "undetermined",
  };
  let panel = PANEL(panelAttrs,
                VBOX({},
		  HBOX({},
		    LABEL({value: "withdrawing"})//,
		    //BUTTON({icon: "close", // WARNING: a way to shrink button's size is needed
		    //        oncommand: "removeProgressMeter();"})
		  ),
                  PROGRESSMETER(progMeterAttrs)
		)
              );
  panel.build(doc.documentElement);
  doc.getElementById("withdraw-meter").openPopup(talerButton, "", 0, 0, false, false);
}

/**
 * Remove progress meter from the button
 */
function removeProgressMeter(){
  let progMeter = getLastWindow().document.getElementById("withdraw-meter");
  progMeter.hidePopup();
}
