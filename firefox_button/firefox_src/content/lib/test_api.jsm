/*

  This file is part of TALER
  Copyright (C) 2015, 2016 INRIA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>

*/

/**
 * @file testcases for /test, see {@link http://api.taler.net/api-mint.html#the-test-api}
 * @author Marcello Stanisci
 */

Components.utils.import("resource://gre/modules/Services.jsm");
Components.utils.import("chrome://taler-button/content/lib/windowMgmt.jsm");
Components.utils.import("chrome://taler-button/content/lib/prettyPrint.jsm");
Components.utils.import("chrome://taler-button/content/lib/io.jsm");
Components.utils.import("chrome://taler-button/content/lib/util.jsm");
Components.utils.import("chrome://taler-button/content/lib/emscripIface.jsm");
Components.utils.import("resource://gre/modules/FileUtils.jsm");
Components.utils.import("resource://gre/modules/ctypes.jsm");


var EXPORTED_SYMBOLS = [
  'testCmp',
  'talerTest',
  'testBlindDb',
  'crockTest',
  'base32test',
  'fileTest',
  'hkdfTest',
  'hashTest',
  'eddsaTest',
  'ecdheTest',
  'encryptTest',
  'rsaTest',
  'localEncryptTest',
  'makePurposeTest',
  'test64bit',
];

/**
 * Yet another base32 test
 */
function crockTest() {

  /* Taking the ArrayBuffer associated with a loaded file and copying its
   content into the emscripten heap */
  function base32fileTest(AB) {

    /* AB has to be accessed through a 'view', so a object of type
    'TypedArray' will get its raw content and serve it as a 'normal' array.
    */
    var uintView = new Uint8Array(AB);
    // emscr's malloc
    var fileBuf = getLastWindow().Module._malloc(uintView.length);
    // actual copying
    getLastWindow().Module.writeArrayToMemory(uintView, fileBuf);
    // actual encoding
    var crockedPtr = GSALLdataToStringAlloc(fileBuf, uintView.length);
    // picking crocked data from emscr's heap
    var crockedStr = getLastWindow().Module.UTF8ToString(crockedPtr);
    // creating the JSON as of mint's specs
    var jsonCrock = JSON.stringify({
      'input': crockedStr
    });
    tDebug(jsonCrock, false);
    // showing what is being sent
    showOutput(jsonCrock);
    // connecting to the mint
    makeHttp(showOutput, tDebug, "POST", mint + "/test/base32", jsonCrock);
    // deallocating what us and libgnunet allocated on emscripten's heap
    TWRgnunetFree(crockedPtr);
    getLastWindow()._free(fileBuf);
  };
}

/**
 * Run the entire /test battery, printing the result
 * on the current HTML document
 *
 * @param {boolean} async - if true, run the test in the background
 */
function talerTest(async) {
  DWRtestString("dummy", 102);
  base32test();
  if (async) {
    let window = getLastWindow();
    window.setTimeout(encryptTest(), 4000);
    window.setTimeout(hkdfTest(), 8000);
    window.setTimeout(ecdheTest(), 16000);
    window.setTimeout(eddsaTest(), 32000);
    window.setTimeout(rsaTest(), 64000);
  }
  else {
    encryptTest();
    hkdfTest();
    ecdheTest();
    eddsaTest();
    rsaTest();
  }
}

/**
 * base32 testcase. It sends an encoded string, gets its
 * hash value back, generate that hash locally and compare
 * that with the received value
 */
function base32test() {
  var testr = genString();
  var strPtr = ALLstr(testr);
  var testEnc = getCrockedValue(strPtr, testr.length);
  // generating the expected value from the mint
  let testHash = TWRALLhash(strPtr, testr.length);
  var hashStr = getCrockedValue(testHash, HASHCODE_SIZE);
  var jsonCrock = JSON.stringify({
    'input': testEnc
  });
  makeHttp(
    // response handler
    function(json) {
      // getting the encoding (aka a string) from the mint
      var enc = JSON.parse(json).output;
      if (hashStr === enc)
        showOutput("/test/base32, ok");
      else
        showOutput("/test/base32, fail: mismatch between"
	           + " locally generated and mint's base32"
		   + " encoding of the same input");
    },
    tDebug, "POST", "http://" + getMintUrl() + "/test/base32", jsonCrock);
}

function fileTest() {

  /* 'arraydFile' is of type ArrayBuffer and will containd the bytes
   from the file being loaded. Despite official MDN documentation,
   the length of that array is held in a property called 'byteLength'
   -- as seen on the following 'example' page.
   https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/Sending_and_Receiving_Binary_Data
   */

  getFile(function(arraydFile) {

    tDebug("I'm " + arraydFile.byteLength + " cells long", false);

  }, "penny.png");
}

/**
 * HKDF test
 */
function hkdfTest() {
  var testr = genString();
  var strPtr = ALLstr(testr);
  var testEnc = getCrockedValue(strPtr, testr.length);
  var jsonCrock = JSON.stringify({
    'input': testEnc
  });
  // generating locally the expected value
  var hk = TWRALLgenKeyFromBlob("salty", strPtr, testr.length);
  let hkEnc = getCrockedValue(hk, HASHCODE_SIZE);
  if (hk == null)
    showOutput("/test/hkdf: failed to (locally) generate hash key");

  showOutput("Generating locally the expected value : " + hkEnc, false);
  showOutput("Sending : " + jsonCrock, false);
  makeHttp(
    // response handler
    function(json) {
      // getting the encoding from the mint
      var enc = JSON.parse(json).output;
      if (enc === hkEnc)
        showOutput("/test/hkdf, ok");
      else
        showOutput("/test/hkdf: mismatch between locally generated"
	           + " hash key and the one received from mint");
    },
    tDebug, "POST", "http://" + getMintUrl() + "/test/hkdf", jsonCrock);
  emscFree(strPtr);
}

/**
 * Hash test
 */
function hashTest() {
  var testr = genString();
  var testrPtr = ALLstr(testr);
  var blob = TWRALLhash(testrPtr, testr.length);
  var crockBlob = getCrockedValue(blob, HASHCODE_SIZE);
  showOutput("base32 of hashed test string: " + crockBlob);
  // deallocate
  TWRgnunetFree(blob);
  emscFree(testrPtr);

}

/**
 * Eddsa test
 */
function eddsaTest() {
  // to be freed!
  var privPtr = GCALLeddsaKeyCreate();
  // get public key
  var pubKey = TWRALLeddsaPublicKeyFromPrivate(privPtr);
  showOutput("public key eddsa " + getCrockedValue(pubKey, EDDSAKEY_SIZE),
             false);
  // each key is sized 256/8 * sizeof(unsigned char) = 32 (bytes)
  var crockPriv = getCrockedValue(privPtr, EDDSAKEY_SIZE);
  var crockPub = getCrockedValue(pubKey, EDDSAKEY_SIZE);
  // sign test message
  var signed = TWRALLsignTest(privPtr);
  var crockedSign = getCrockedValue(signed, 64);
  showOutput("base32 of signed: " + crockedSign, false);
  // make the JSON
  var toSend = JSON.stringify(
    {
      'eddsa_pub': crockPub,
      'eddsa_sig': crockedSign
    }
  );
  showOutput("eddsa test, sending : " + toSend, false);
  makeHttp(
    // response handler
    function(json) {
      // getting the base32 encoding from the mint
      var jsObj = JSON.parse(json);
      var kpub_enc = jsObj.eddsa_pub;
      var sign_enc = jsObj.eddsa_sig;
      showOutput("Receiving : " + json, false);
      // decoding what received from the mint
      var kpub = ALLgetDeCrock(kpub_enc);
      var sign = ALLgetDeCrock(sign_enc);
      /* verifying what received (RECALL: only RSA keys get
      encoded in a further binary format before travelling) */
      if (WRverifyTest(sign.dec, kpub.dec) == GNUNET_OK)
        showOutput("/test/eddsa, ok");
      else
        showOutput("/test/eddsa, failed: signature NOT valid!");

      emscFree(kpub.dec);
      emscFree(sign.dec);
    },
    tDebug, "POST", "http://" + getMintUrl() + "/test/eddsa", toSend);

  TWRgnunetFree(privPtr);
  TWRgnunetFree(pubKey);
  TWRgnunetFree(signed);
}

/**
 * Ecdhe test
 */
function ecdheTest() {

  // Create ECDHE private key. To be freed!
  var privKey = GCALLecdheKeyCreate();

  var pubKey = TWRALLecdhePublicKeyFromPrivateKey(privKey);

  // each key is sized 256/8 * sizeof(unsigned char) = 32 (bytes)
  var crockPriv = getCrockedValue(privKey, 32);
  var crockPub = getCrockedValue(pubKey, 32);

  /* derive key material for local proof against what the mint sends back
   1) alloc hash, 2) gen key material, 3) crock it, 4) print it!
   var kmat = WRALLgetHashCode();
  var res = GCeccEcdh(privKey, pubKey, kmat); */

  let kmat = TWRALLeccEcdh(privKey, pubKey);

  if (kmat == null)
    showOutput("key material NOT generated");

  let kmatEnc = getCrockedValue(kmat, HASHCODE_SIZE);

  showOutput("base32 of private key: " + crockPriv, false);
  showOutput("base32 of public key: " + crockPub, false);
  showOutput("locally generated key material : " + kmatEnc, false);

  TWRgnunetFree(privKey);
  TWRgnunetFree(pubKey);
  TWRgnunetFree(kmat);

  // creating the JSON
  var obj = {
    'ecdhe_pub': crockPub,
    'ecdhe_priv': crockPriv
  };
  var jsonEcdhe = JSON.stringify(obj);
  makeHttp(
    // response handler
    function(json) {
      // getting the encoding (aka a string) from the mint
      var enc = JSON.parse(json).ecdh_hash;
      // check if values match
      if (enc === kmatEnc)
        showOutput("/test/ecdhe, ok");
      else
        showOutput("ECDHE test, failed : got different key from the mint");
      showOutput("Receiving : " + json, false);
    },
    tDebug, "POST", "http://" + getMintUrl() + "/test/ecdhe", jsonEcdhe);
}

/**
 * Encryption test
 */
function encryptTest() {

  var testr = genString();
  var hashed = testr + "salty";

  // encode the test string
  var testrPtr = ALLstr(testr);
  var testrCrock = getCrockedValue(testrPtr, testr.length + 1);

  // var inPlaceDeCrock = getLastWindow()
         //.Module.UTF8ToString(ALLgetDeCrock(testrCrock).dec);
  // showOutput("decrock input : " + inPlaceDeCrock);

  // hash and encode the binary 'salt'
  var hashedPtr = ALLstr(hashed);
  let buf = TWRALLhash(hashedPtr, hashed.length);
  var blobCrock = getCrockedValue(buf, HASHCODE_SIZE);

  var toSend = JSON.stringify({
    'input': testrCrock,
    'key_hash': blobCrock
  });
  showOutput("Sending : " + toSend, false);


  // generating symmetric key and init. vector
  var skey = TWRALLgenSymmetricKey("skey", buf, HASHCODE_SIZE);
  var iv = TWRALLgenInitVector("iv", buf, HASHCODE_SIZE);

  makeHttp(

    // response handler
    function(json) {

      // getting the encoding (aka a string) from the mint
      var enc = JSON.parse(json).output;
      // decoding what received from the mint
      // (dec.dec will hold the blob from the mint)
      var dec = ALLgetDeCrock(enc);
      //var outbuf = emscMalloc();
      // decrypt
      var declength = GCsymmetricDecrypt(dec.dec, dec.length,
                                         skey, iv, dec.dec);
      showOutput("Receiving : " + json, false);
      showOutput("decrypted bytes : " + declength, false);
      if (declength > 0)
        showOutput("/test/encrypt, ok");
      else
        showOutput("/test/encrypt, failed :"
	           + "GNUNET_CRYPTO_symmetric_decrypt returned -1");
      TWRgnunetFree(skey);
      TWRgnunetFree(iv);
      emscFree(dec.dec);
    },
    tDebug, "POST", "http://" + getMintUrl() + "/test/encrypt", toSend);

  emscFree(testrPtr);
  emscFree(hashedPtr);
  TWRgnunetFree(buf);


}

/**
 * RSA test
 */
function rsaTest() {

  var window = getLastWindow();
  // creating a message
  var str = genString();
  var strPtr = ALLstr(str);
  // hashing the message
  var buf = TWRALLhash(strPtr, str.length);
  var bk = GCALLrsaBlindingKeyCreate(1024);
  makeHttp(
    function(json) {
      showOutput("receiving : " + json, false);
      var pbkMintEnc = JSON.parse(json).rsa_pub;
      var pbkMint = TWRALLrsaPublicKeyDecodeFromString(pbkMintEnc);
      showOutput("mint's pub : " + pbkMint, false);
      var blindPtr = emscMalloc(PTR_SIZE);
      let blindSize = GCALLrsaBlind(buf, bk, pbkMint, blindPtr);
      if (!(blindSize > 0))
        showOutput("/test/rsa/get, failed: Blinding NOT accomplished");
      var blindPtr2 = window.Module.getValue(blindPtr, '*');
      var crockBlind = getCrockedValue(blindPtr2, blindSize);
      var blindJson = JSON.stringify({
        'blind_ev': crockBlind
      });
      // send blinded blob to mint
      makeHttp(
        function(json2) {
          var blindSig = JSON.parse(json2).rsa_blind_sig;
          var blindSigBlob = ALLgetDeCrock(blindSig);
          var sign = GCALLrsaSignatureDecode(blindSigBlob.dec,
	                                     blindSigBlob.length);
          if (!sign)
            showOutput("/test/rsa/sign, failed: signature not decoded ");
          // unblind
          var unblindedSig = GCALLrsaUnblind(sign, bk, pbkMint);
          if (!unblindedSig)
            showOutput("/test/rsa/sign, failed: signature not unblinded");
          // verify
          if (GNUNET_OK == GCrsaVerify(buf, unblindedSig, pbkMint))
            showOutput("/test/rsa/{get,sign}, ok");
          else showOutput("/test/rsa/sign, failed: invalid mint's signature");

          GCrsaSignatureFree(unblindedSig);
          emscFree(blindSigBlob.dec);
          GCrsaSignatureFree(sign);
          emscFree(blindPtr);
          TWRgnunetFree(blindPtr2);
          GCrsaPublicKeyFree(pbkMint);
          GCrsaBlindingKeyFree(bk);

        }, tDebug, "POST", "http://" + getMintUrl() + "/test/rsa/sign",
	   blindJson);
    },
    tDebug, "GET", "http://" + getMintUrl() + "/test/rsa/get");
}

/**
 * Encrypt and decrypt locally
 */
function localEncryptTest() {

  var plain = genString();
  var salt = plain + "salt it";

  // hash the binary "salt"
  var saltPtr = ALLstr(salt);
  var buf = GChash(saltPtr, salt.length);

  // generating symmetric key and init. vector
  var skey = WRALLgenSymmetricKey("skey", buf, HASHCODE_SIZE);
  var iv = WRALLgenInitVector("iv", buf, HASHCODE_SIZE);

  // encrypt plaintext
  var strAll = ALLstr(plain);
  var encAll = emscMalloc(plain.length);
  var result = emscMalloc(plain.length);

  var output = GCsymmetricEncrypt(strAll, plain.length, skey, iv, result);
  showOutput("Encrypting " + output + " bytes");

  output = GCsymmetricDecrypt(result, plain.length, skey, iv, result);
  showOutput("Decrypting " + output + " bytes in : "
             + getLastWindow().Module.UTF8ToString(result)
	     + " at [ " + result + " ]");
  showOutput("Plain string was at [ " + strAll + " ]");

  TWRgnunetFree(buf);
  emscFree(saltPtr);
  emscFree(strAll);
  emscFree(encAll);
  emscFree(result);
}

/**
 * Testing the capability (implemented in C-land) of generating the
 * purpose for a signature.
 */
function makePurposeTest() {
  let motiv = "master_denomination_key_validity";
  let payload = ALLstr(motiv);
  let purpo = WRALLmakePurpose(motiv, payload, motiv.length);
  showOutput("purpose at : " + purpo);
  gnunetFree(purpo);
  emscFree(payload);
}

/**
 * Testing the DB query for getting a blind key
 * from an amount
 */
function testBlindDb() {
  getBlindKey(getMintUrl(), 10, "EUR");
}

/**
 * 64-bit management test
 */
function test64bit() {

  let ll = ctypes.UInt64(666);
  let Hi = ctypes.UInt64.hi(ll);
  let Lo = ctypes.UInt64.lo(ll);
  showOutput("dHi, dLo : " + Hi + ", " + Lo);

  DWRtestString(Lo, 0, "dummy");
}

/**
 * Testing the capability of emscripted code of string
 * comparison
 */
function testCmp() {
  let y = ALLstr("only this works", true);
  let x = DWRtestStringCmp(y);
  emscFree(y);
  showOutput("string len : " + x);
}
