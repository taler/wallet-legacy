/*

  This file is part of TALER
  Copyright (C) 2015, 2016 INRIA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>

*/

/**
 * @author Marcello Stanisci
 */

var EXPORTED_SYMBOLS = [
  'obtainKeys',
  'populateKeysDBPromises',
  'populateKeysDB',
];

Components.utils.import("resource://gre/modules/Services.jsm");
Components.utils.import("chrome://taler-button/content/lib/windowMgmt.jsm");
Components.utils.import("chrome://taler-button/content/lib/prettyPrint.jsm");
Components.utils.import("chrome://taler-button/content/lib/io.jsm");
Components.utils.import("chrome://taler-button/content/lib/util.jsm");
Components.utils.import("chrome://taler-button/content/lib/emscripIface.jsm");
Components.utils.import("resource://gre/modules/FileUtils.jsm");
Components.utils.import("resource://gre/modules/ctypes.jsm");

/* KEYS MANAGEMENT */

/* TODO: verification of auditor's signature */

let verifyWorker = new Worker("chrome://taler-button/content/lib/obtainKeys.worker.jsm");

/**
 * Get a mint keyset; verification done via Web Worker
 *
 * @param {string} mintUrl - base URL of the desired mint (no "/keys" at the end)
 * NOTE: it includes 'http://' and a final '/'
 * @param cb - callback function to call once the keys have been received and
 * verified
 * @param cls - argument for `cb`
 * @param populator - cb which fills out the DB with the retrieved keys
 */
function obtainKeys(mintUrl, cb, cls, populator) {

  makeHttp(function(resp) {
    let keys = JSON.parse(resp);
    let thisPopulator;
    verifyWorker.onmessage = function (msg){
      if (GNUNET_OK == msg.data){
        if (!populator)
	  thisPopulator = populateKeysDB;
	else
	  thisPopulator = populator;

          /*let populatePromise = Promise(function(resolve, reject){
	    thisPopulator(keys, mintUrl);
	    resolve(cls);
	  });
          populatePromise.then(cb);*/

	thisPopulator(keys, mintUrl);
        getLastWindow().setTimeout(function(){cb(cls)}, 1000);
	}
    };
    verifyWorker.postMessage({reason: "verifyKeys",
                              param : {keys: keys,
			               mint: mintUrl}
			     });
    },
    tDebug,
    "GET",
    mintUrl + "keys");
}

/**
 * Store any key information in the local DB. Typically called
 * after successful verification of keys
 * 
 * @param {object} keys - some mint keyset, compliant with the specification
 * given in {@link http://api.taler.net/api-mint.html#obtaining-mint-keys}
 */
function populateKeysDB(keys, mintUrl){
  tDebug(JSON.stringify(keys), false);
  let dbConn = getDbHandle();
  try{
  let mintTableStmt = dbConn.createStatement("CREATE TABLE IF NOT EXISTS mint_table"
                                             + " (mint_base_url TEXT PRIMARY KEY"
			                     + ", master_key TEXT, last_denom_update TEXT)");

  mintTableStmt.executeAsync(
    {
      handleResult: function(){},
      handleCompletion: function(){
        let dbConn = getDbHandle();
        let mintstmt = dbConn.createStatement("INSERT OR REPLACE INTO mint_table VALUES"
                                        + " (:mint, :masterKey, :lastDenomUpdate)");
        mintstmt.params.mint = mintUrl;
        mintstmt.params.masterKey = keys.master_public_key;
        mintstmt.params.lastDenomUpdate = keys.list_issue_date;
        mintstmt.executeAsync();},
      handleError: function(){}
    }
  );

  let denomTableStmt = dbConn.createStatement("CREATE TABLE IF NOT EXISTS denom_table"
                                              + " (mint TEXT, stamp_start TEXT"
	                                      + ", stamp_expire_withdraw TEXT"
			                      + ", stamp_expire_deposit TEXT"
			                      + ", stamp_expire_legal TEXT"
			                      + ", denom_pub TEXT PRIMARY KEY"
			                      + ", FOREIGN KEY(mint) REFERENCES"
			                      + " mint_table(mint_base_url))");

  denomTableStmt.executeAsync(
    {
      handleResult: function(){},
      handleCompletion: function(){
        let dbConn = getDbHandle();
        let denomStmt = dbConn.createStatement("INSERT OR REPLACE INTO denom_table VALUES"
                                               + " (:mint, :stampStart, :stampExpireWithdraw"
  	                                       + ", :stampExpireDeposit, :stampExpireLegal"
  					       + ", :denomPub)");
        for (let i = 0; i < keys.denoms.length; i++){
          denomStmt.params.mint = mintUrl;
          denomStmt.params.denomPub = keys.denoms[i].denom_pub;
          denomStmt.params.stampStart = keys.denoms[i].stamp_start;
          denomStmt.params.stampExpireWithdraw = keys.denoms[i].stamp_expire_withdraw;
          denomStmt.params.stampExpireDeposit = keys.denoms[i].stamp_expire_deposit;
          denomStmt.params.stampExpireLegal = keys.denoms[i].stamp_expire_legal;
          denomStmt.executeAsync();
	}
	dbConn.asyncClose();
      },
      handleError: function(){}
    }
  );
  let denomAmntTableStmt = dbConn.createStatement("CREATE TABLE IF NOT EXISTS denom_amount_table"
                                                  + " (mint TEXT, denom TEXT, value INTEGER, fraction INTEGER"
			                          + ", currency TEXT, FOREIGN KEY(denom) REFERENCES"
			                          + " denom_table(denom_pub), UNIQUE(denom) ON"
			                          + " CONFLICT ROLLBACK)");

  denomAmntTableStmt.executeAsync(
    {
      handleResult: function(){},
      handleCompletion: function(){
        let dbConn = getDbHandle();
        let denomAmntStmt = dbConn.createStatement("INSERT OR REPLACE INTO denom_amount_table VALUES"
                                                   + " (:mint, :denomCol, :valueCol, :fractionCol"
  	                                           + ", :currencyCol)");
        for (let i = 0; i < keys.denoms.length; i++){
          denomAmntStmt.params.mint = mintUrl;
          denomAmntStmt.params.denomCol = keys.denoms[i].denom_pub;
          denomAmntStmt.params.valueCol = keys.denoms[i].value.value;
          denomAmntStmt.params.fractionCol = keys.denoms[i].value.fraction;
          denomAmntStmt.params.currencyCol = keys.denoms[i].value.currency;
          denomAmntStmt.executeAsync();
	}
	dbConn.asyncClose();
      },
      handleError: function(){}    
    }
  );
  let feeWithdrawTableStmt = dbConn.createStatement("CREATE TABLE IF NOT EXISTS fee_withdraw_table"
                                                    + " (mint TEXT, denom TEXT, value INTEGER, fraction INTEGER"
			                            + ", currency TEXT, FOREIGN KEY(denom)"
			                            + " REFERENCES denom_table(denom_pub)"
			                            + ", UNIQUE(denom) ON CONFLICT ROLLBACK)");
  feeWithdrawTableStmt.executeAsync(
    {
      handleResult: function(){},
      handleCompletion: function(){
        let dbConn = getDbHandle();
        let feeWithdrawStmt = dbConn.createStatement("INSERT OR REPLACE INTO fee_withdraw_table VALUES"
                                                     + " (:mint, :denomCol, :valueCol, :fractionCol"
                                                     + ", :currencyCol)");
        for (let i = 0; i < keys.denoms.length; i++){
          feeWithdrawStmt.params.mint = mintUrl;
          feeWithdrawStmt.params.denomCol = keys.denoms[i].denom_pub;
          feeWithdrawStmt.params.valueCol = keys.denoms[i].fee_withdraw.value;
          feeWithdrawStmt.params.fractionCol = keys.denoms[i].fee_withdraw.fraction;
          feeWithdrawStmt.params.currencyCol = keys.denoms[i].fee_withdraw.currency;
          feeWithdrawStmt.executeAsync();
	}
	dbConn.asyncClose();
      },
    }
  );						    

  let feeDepositTableStmt = dbConn.createStatement("CREATE TABLE IF NOT EXISTS fee_deposit_table"
                          + " (mint TEXT, denom TEXT, value INTEGER, fraction INTEGER"
			  + ", currency TEXT, FOREIGN KEY(denom)"
			  + " REFERENCES denom_table(denom_pub)"
			  + ", UNIQUE(denom) ON CONFLICT ROLLBACK)");
  feeDepositTableStmt.executeAsync(
    {
      handleResult: function(){},
      handleCompletion: function(){

        let dbConn = getDbHandle();
        let feeDepositStmt = dbConn.createStatement("INSERT OR REPLACE INTO fee_deposit_table VALUES"
                                                    + " (:mint, :denomCol, :valueCol, :fractionCol"
                                                    + ", :currencyCol)");
        for (let i = 0; i < keys.denoms.length; i++){
	  let feeDepositStmt = dbConn.createStatement("INSERT OR REPLACE INTO fee_deposit_table VALUES"
                                                      + " (:mint, :denomCol, :valueCol, :fractionCol"
                                                      + ", :currencyCol)");
	  feeDepositStmt.params.mint = mintUrl;
          feeDepositStmt.params.denomCol = keys.denoms[i].denom_pub;
          feeDepositStmt.params.valueCol = keys.denoms[i].fee_deposit.value;
          feeDepositStmt.params.fractionCol = keys.denoms[i].fee_deposit.fraction;
          feeDepositStmt.params.currencyCol = keys.denoms[i].fee_deposit.currency;
          feeDepositStmt.executeAsync();	
	}
	dbConn.asyncClose();
      },
    }
  );
  let feeRefreshTableStmt = dbConn.createStatement("CREATE TABLE IF NOT EXISTS fee_refresh_table"
                          + " (mint TEXT, denom TEXT, value INTEGER, fraction INTEGER"
			  + ", currency TEXT, FOREIGN KEY(denom)"
			  + " REFERENCES denom_table(denom_pub)"
			  + ", UNIQUE(denom) ON CONFLICT ROLLBACK)");
  feeRefreshTableStmt.executeAsync(
    {
      handleResult: function(){},
      handleCompletion: function(){
        let dbConn = getDbHandle();
        let feeRefreshStmt = dbConn.createStatement("INSERT OR REPLACE INTO fee_refresh_table VALUES"
                                                    + " (:mint, :denomCol, :valueCol, :fractionCol"
                                                    + ", :currencyCol)");
        for (let i = 0; i < keys.denoms.length; i++){
          feeRefreshStmt.params.mint = mintUrl;
          feeRefreshStmt.params.denomCol = keys.denoms[i].denom_pub;
          feeRefreshStmt.params.valueCol = keys.denoms[i].fee_refresh.value;
          feeRefreshStmt.params.fractionCol = keys.denoms[i].fee_refresh.fraction;
          feeRefreshStmt.params.currencyCol = keys.denoms[i].fee_refresh.currency;
          feeRefreshStmt.executeAsync();
        }
	dbConn.asyncClose();
      },
    } 
  );

  /* create signkeys_table */
  let signkeysTableStmt = dbConn.createStatement("CREATE TABLE IF NOT EXISTS signkeys_table"
                          + " (mint TEXT, key TEXT, stamp_start TEXT"
			  + ", stamp_expire TEXT, stamp_end TEXT"
			  + ", FOREIGN KEY(mint) REFERENCES"
			  + " mint_table(mint_base_url))");
  signkeysTableStmt.executeAsync(
    {
      handleResult: function(){},
      handleCompletion: function(){
        let dbConn = getDbHandle();
        let signKeysStmt = dbConn.createStatement("INSERT INTO signkeys_table VALUES"
                                                  + " (:mintCol, :keyCol, :stampStartCol"
                                                  + ", :stampExpireCol, :stampEndCol)");
        for (var i in keys.signkeys) {
          signKeysStmt.params.mintCol = mintUrl;
          signKeysStmt.params.stampStartCol = keys.signkeys[i].stamp_start;
          signKeysStmt.params.keyCol = keys.signkeys[i].key;
          signKeysStmt.params.stampExpireCol = keys.signkeys[i].stamp_expire;
          signKeysStmt.params.stampEndCol = keys.signkeys[i].stamp_end;
          signKeysStmt.executeAsync();
	}
	dbConn.asyncClose();
      },
    }
  );
  }
  catch(e){
    printError(e);
    return;
  }
  /* close db connection */
  dbConn.asyncClose();
  return;
}

/**
 * Store any key information in the local DB. Typically called
 * after successful verification of keys. Makes use of "promises"
 * constructs
 * 
 * @param {object} keys - some mint keyset, compliant with the specification
 * given in {@link http://api.taler.net/api-mint.html#obtaining-mint-keys}
 */
function populateKeysDBPromises(keys, mintUrl){}
