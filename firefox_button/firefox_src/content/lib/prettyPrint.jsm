/*

  This file is part of TALER
  Copyright (C) 2015, 2016 INRIA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>

*/

/**
 * @file Here are kept all the functions that manage to show output on screen, both
 * debugging ones and 'normal' ones.
 * @author Marcello Stanisci
 */

var EXPORTED_SYMBOLS = ['tDebug', 'showNativeOutput', 'showOutput'];

Components.utils.import("chrome://taler-button/content/lib/windowMgmt.jsm");

/* importing the 'mediator' to the chrome console */
const bConsoleService = Components.classes["@mozilla.org/consoleservice;1"]
                        .getService(Components.interfaces.nsIConsoleService);

/**
 * Print to the Browser Console (open with CTRL-J). Needed since
 * is not possible to just call 'console.log()' from an "extension"
 * context
 *
 * @param {string} msg - the message to print
 * @param {boolean} flag - if false, the message will not be printed
 */
function tDebug(msg, flag) {
  let tof = true;
  if (typeof flag !== "undefined")
    tof = flag;
  if (tof) bConsoleService.logStringMessage("taler dbg: " + msg);
}

/**
 * Print to the Web Console (open with CTRL-K).
 *
 * @param {string} output - the message to print
 */
function showNativeOutput(output) {

  var nativeConsole = getLastWindow().gBrowser.contentWindow.console;
  nativeConsole.log("taler output: " + output);

}

/**
 * Print on the current HTML document. In particular, it
 * shows a box containing the message on the upper part of
 * the page; with a button to erase it.
 *
 * @param {string} output - message to make appear
 * @param {boolean} flag - if false, do not print `output`
 */
function showOutput(output, flag) {
  /* any output from us will be placed inside a DIV with id='taler-output'.
    That DIV will be placed at the top of the showed window */
  let tof = true;
  if (typeof flag !== "undefined") tof = flag;
  if (!tof) return;
  /* what if 'contentDocument' is NOT HTML ? */
  var thisHTMLDoc = getLastWindow().gBrowser.contentDocument;
  /* in case we are not on some HTML document, invite the user to open one.
    if (!thisHTMLDoc.body)
    getLastWindow().alert("Please open a HTML document!\n\n\'about:blank\' will work too."); */
  /* Check if 'our' DIV is already present in the page. */
  var ourDiv = thisHTMLDoc.getElementById('taler-output');
  if (!ourDiv) {
    /* Create whole container */
    ourDiv = thisHTMLDoc.createElement('div');
    ourDiv.setAttribute('id', 'taler-output');
    /* set up a 'clear console' button */
    var clrButton = thisHTMLDoc.createElement('button');
    clrButton.setAttribute('type', 'button');
    clrButton.innerHTML = 'Clear console';
    /* instruct the button to kill output's lines and itself */
    clrButton.addEventListener('click', function () {
      var div = document.getElementById('taler-output');
      div.parentNode.removeChild(div);
    });
    /* the following string makes the button remain visible while erasing the lines of output */
    //var toDel = thisDiv.getElementsByTagName("div")[1]; thisDiv.removeChild(toDel);');
    /* button's right align */
    clrButton.setAttribute('style', 'text-align: right; margin: 15px;');
    /* it keeps the output lines from being right aligned like the button is */
    var clrButtonDiv = thisHTMLDoc.createElement("div");
    clrButtonDiv.appendChild(clrButton);
    clrButtonDiv.setAttribute('style', 'text-align: right');
    /* append button to its wrapper DIV */
    ourDiv.appendChild(clrButtonDiv);
    /* place the main DIV on top of the shown window */
    thisHTMLDoc.body.insertBefore(ourDiv, thisHTMLDoc.body.firstChild);
  }

  /* check whether there is already a 'pastebin'-like DIV inside the main one */
  var pasteBin = ourDiv.getElementsByTagName("div")[1];
  tDebug(pasteBin);
  if (!pasteBin) {
    /* Create pastebin-like sub-DIV container */
    pasteBin = thisHTMLDoc.createElement('div');
    pasteBin.setAttribute('id', 'op-div');
    /* draw a frame around it */
    pasteBin.setAttribute('style', 'border-style: dotted; border-width: 1px; margin: 15px; padding: 15px;');
    /* Append it inside the main one */
    ourDiv.appendChild(pasteBin);
  }
  /* create a 'line' of output */
  var verbatim = thisHTMLDoc.createElement("pre");
  /* inject the output in the line */
  verbatim.innerHTML = output;
  /* put the created line at the bottom of the pastebin DIV */
  ourDiv.getElementsByTagName("div")[1].appendChild(verbatim);
}
