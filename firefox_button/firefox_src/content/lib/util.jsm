/*

  This file is part of TALER
  Copyright (C) 2014, 2015 Christian Grothoff (and other contributing authors)

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>


*/


Components.utils.import("resource://gre/modules/Services.jsm");
Components.utils.import("resource://gre/modules/FileUtils.jsm");
Components.utils.import("chrome://taler-button/content/lib/windowMgmt.jsm");
Components.utils.import("chrome://taler-button/content/lib/prettyPrint.jsm");
Components.utils.import("chrome://taler-button/content/lib/emscripIface.jsm");
Components.utils.import("resource://gre/modules/ctypes.jsm");

var EXPORTED_SYMBOLS = [
  'getPrettyAmount',
  'sameAmount',
  'printError',
  'isMint',
  'ALLstr',
  'getTimestampNumber',
  'reserveCreated',
  'TWRALLgetAmount32', // remove '32' ASAP
  'TWRALLgetAmount',
  'TWRALLsumAmounts',
  'multiplyAmount',
  'findCoinInArray',
  'genString',
  'dumpAmount',
  'numberFromAmountPtr',
  'dumpAmountPtr',
  'getMintUrl',
  'getMintCurrency',
  'getDbHandle',
  'getCrockedValue',
  'ALLgetDeCrock',
  'GNUNET_OK',
  'GNUNET_NO',
  'GNUNET_SYSERR',
  'HASHCODE_SIZE',
  'PTR_SIZE',
  'EDDSAKEY_SIZE',
  'EDDSASIG_SIZE',
  'BLINDFACT1024_SIZE',
  'RSASIG_SIZE',
  'NULL'
];

var GNUNET_OK = 1;
var GNUNET_NO = 0;
var GNUNET_SYSERR = -1;
var NULL = 0;
var HASHCODE_SIZE = 64;
var PTR_SIZE = 4;
var EDDSAKEY_SIZE = 256 / 8;
var EDDSASIG_SIZE = 2 * (256 / 8);
var BLINDFACT1024_SIZE = 1024 / 8;
var RSASIG_SIZE =

  // check whether the given site is a mint. TBD
  function isMint(ref) {

    return true;

  }


/**
 * Cenerate a random string (mainly for debugging purposes)
 * @return {string}
 */

function genString() {

  return "Some XYA String";

}

/**
 * Convert amount to pretty format
 *
 * @param {number} amountPtr - pointer to allocated `struct TALER_Amount`
 * @return {string} - the amount in the format "CURRENCY x.y". Note that
 * 'y' is returned as is in the allocated struct; i.e. without being
 * converted to the decimals
 */
function getPrettyAmount(amountPtr){
  let ret = TWRgetCurrency(amountPtr)
    + " " + TWRgetValue(amountPtr)
    + "." + TWRgetFraction(amountPtr);
  return ret;
}

/**
 * Get a JavaScript "number" from an allocated amount
 *
 * @param {number} amountPtr - the allocated `struct TALER_Amount`
 * to "extract" a number from
 * @return {number} the native float representing `amountPtr`
 */
function numberFromAmountPtr(amountPtr){
  return TWRgetValue(amountPtr) + (TWRgetFraction(amountPtr) / 1000000);
}

/**
 * Multiply amount for a scalar
 *
 * @param {number} amountPtr - pointer to allocated `struct TALER_Amount`
 * representing the amount to multiply
 * @param {number} times - scalar factor, accept only > 0. XXX cover case 0?
 * @return {number} `amountPtr` holding the new value, GNUNET_SYSERR on
 * overflow
 */
function multiplyAmount(amountPtr, times){
  if (times < 1)
    return GNUNET_SYSERR;
  else if (times == 1)
    return amountPtr;
  for (let i=0; i<times; i++)
    if (GNUNET_SYSERR == TamountAdd(amountPtr, amountPtr, amountPtr))
      return GNUNET_SYSERR;
  return amountPtr;
}
/**
 * Compare amounts. Used to avoid allocating `struct TALER_Amount`
 * in "quick" operation
 *
 * @param {talerAmount} a
 * @param {talerAmount} b
 * @return {boolean} true, if a == b
 */
function sameAmount(a, b){
  return (a.value == b.value) &&
         (a.fraction == b.fraction) &&
	 (a.currency == b.currency);

}

/**
 * Check if a given coin is among a collection of
 * coins. Typically used to see if some coin has already
 * been added to the coin to spend
 *
 * @param {object[]} collection - the collection of coin
 * @param {object} coin - the "coin" to find
 * @param {function} cmp - the cb that checks if two coins are the same thing
 * @param {function} strip - the function which translates array cells in a
 * format suitable for comparison
 * @return {number} - the array index holding `coin`, or
 * GNUNET_NO if not found
 */
function findCoinInArray(collection, coin, cmp, strip){
  let ret = GNUNET_NO;
  for (let j=0; j<collection.length; j++)
    if (cmp(coin, strip(collection[j])))
      ret = j;
  /* XXX what if the same amount occurs twice inside the array? */
  return ret;
}
/**
 * Retrieve which currency a mint works with
 *
 * @param {string} mintUrl - the mint's base URL
 * @return {string} the currency associated with `mintUrl`
 */
function getMintCurrency(mintUrl){
  let dbConn = getDbHandle();
  let getCurrencyStmt =
  dbConn.createStatement("SELECT currency FROM denom_amount_table"
                          + " WHERE mint = :mint");
  getCurrencyStmt.params.mint = mintUrl;
  if (!getCurrencyStmt.executeStep())
    return null;
  let ret = getCurrencyStmt.row.currency;
  showOutput("Gotten currency: " + getCurrencyStmt.row.currency, false);
  showOutput("to return: " + ret, false);
  getCurrencyStmt.reset();
  getCurrencyStmt.finalize();
  dbConn.close();
  return ret;
}

/**
 * Obsolete
 */
function getMintUrl(form) {

  //let mint = "localmint";
  let mint = "mint.demo.taler.net";

  if (!form) return mint;

  // retrieve taler's reserve form from current document
  var content = getLastWindow().gBrowser.contentDocument;
  mint = content.getElementById("mint-url");
  return mint.value;
}

/**
 * Obsolete
 */
function reserveCreated(outcome) {
  let window = getLastWindow();
  let content = window.gBrowser.contentDocument;
  let h1 = content.createElement("h1");
  if (outcome)
    h1.innerHTML = "Operation succeeded, the shopping"
                   + "area is <a href=shopping.html>there</a>!";
  else
    h1.innerHTML = "Operation did NOT succeed";
  // put h1 in place of 'form'
  let form = content.getElementById("reserve-form");
  form.parentNode.replaceChild(h1, form);
}

/**
 * Open "wallet.git" database
 *
 * @return {object} handle to wallet's SQLite DB
 */
function getDbHandle() {

  let file = FileUtils.getFile("ProfD", ["wallet.sqlite"]);
  let dbConn = Services.storage.openDatabase(file);
  return dbConn;

}

/**
 * Encodes data to Crockford's base32 encoding
 *
 * @param {number} valPtr - pointer to the allocated (in the emscripten's heap)
 * data to encode
 * @return {string} encoding of `valPtr`
 */
function getCrockedValue(valPtr, length) {

  //actual encoding
  var crockedPtr = GSALLdataToStringAlloc(valPtr, length);

  // dereferencing the crocked value (aka a string)
  var crocked = getLastWindow().Module.UTF8ToString(crockedPtr);

  TWRgnunetFree(crockedPtr);

  return crocked;

}


/**
 * Allocate and copy the given string into emscripten's heap
 *
 * @param {string} str - the string to allocate and copy
 * @param {boolean} addNullTerm - if true nul-terminates the string in
 * memory
 */
function ALLstr(str, addNullTerm) {

  // we don't allocate space for the null terminator
  var strPtr = emscMalloc(str.length);
  // copy it into the allocated location
  getLastWindow().Module.writeStringToMemory(str, strPtr, !addNullTerm);

  return strPtr;

}

/**
 * Decode a base32 string
 *
 * @param {string} crock - the string to decode
 * @return {object} data - decoded data
 * @return {object} data.ptr - pointer to the allocated binary data
 * @return {object} data.length - how big (in bytes) is the decoded
 * data
 */
function ALLgetDeCrock(crock) {

  var str = ALLstr(crock);

  var decLength = Math.floor((crock.length * 5) / 8);
  var out = emscMalloc(decLength);

  if (GSstringToData(str, crock.length, out, decLength) != GNUNET_OK)
    tDebug("failed to decode");

  emscFree(str);

  return {
    'dec': out,
    'length': decLength
  };

}

/**
 * Free memory in (emscripten's) heap
 *
 * @param {number} ptr - pointer to the chink to free
 */
function emscFree(ptr) {

  getLastWindow().Module._free(ptr);

}

function TWRALLsumAmounts(val1, frac1,
                          val2, frac2,
			  currency) {

  let val1_64 = ctypes.UInt64(val1);
  let val2_64 = ctypes.UInt64(val2);

  let sum = TWRALLamountAdd(ctypes.UInt64.lo(val1_64),
                            ctypes.UInt64.hi(val1_64),
                            ctypes.UInt64.lo(val2_64),
                            ctypes.UInt64.hi(val2_64),
                            frac1,
			    frac2,
			    currency);
  return sum;
}


function TWRALLgetAmount(a){

  let ptr = TWRALLgetAmount32(a.value,
                              a.fraction,
                              a.currency);
  return ptr;
}

/**
 * Allocates and copy a `struct TALER_Amount` value in the heap
 *
 * @param {number} value - integer part of the amount to store
 * @param {number} fraction - fractional part of the amount to store
 * @param {string} currency - curerncy of the amount to store
 * @return {number} pointer to the allocated chunk of memory
 */
function TWRALLgetAmount32(value, fraction, currency) {

  let value64 = ctypes.UInt64(value);
  let amountPtr = TWRALLgetAmount(ctypes.UInt64.lo(value64),
                                  ctypes.UInt64.hi(value64),
                                  fraction,
				  currency);
  return amountPtr;
}

function printError(err) {
  showOutput("Error at " + err.fileName + " ("
             + err.lineno + ") : "
	     + err.message, true);
}


/**
 * Extracts seconds from a date
 *
 * @param {string} dateStr - a Taler-compliant date in the form "/Date(x)/"
 * @return {number} the 'x' part of `dateStr`
 */
function getTimestampNumber(dateStr) {
  /**
   * needed since there are cases (for example when
   * `dateStr` is an object returned from a SQLite query)
   * where `dateStr` doesn't properly respect the String
   * interface
   */
  let dummy = new String(dateStr);
  let mec = dummy.match(/Date\(([0-9]+)\)/);
  if (null == mec){
    let stack = new Error();
    tDebug(stack.stack);
  }
    return Number(mec[1]);
}

/**
 * Fancyfy amounts
 * 
 * @param {talerAmount} a - the amount to fancyfy
 * @return {string} - a string representing `a` as "currency value.fraction"
 */
function dumpAmount(a){

  return a.currency + " " + Number(a.value + (a.fraction/1000000));
}

/**
 * Fancyfy amounts allocated in the emscripten's heap
 * 
 * @param {number} a - pointer to the amount to fancyfy
 * @return {string} - a string representing `a` as "currency value.fraction"
 */
function dumpAmountPtr(a){

  return TWRgetCurrency(a) + " " + Number(TWRgetValue(a) + (TWRgetFraction(a)/1000000));
}

/**
 * Fake a contract
 *
 * @param {string} currency - the currency this contracts will show prices of
 * @return {object} a contract compliant to {@link http://api.taler.net/api-merchant.html#contract}
 */
function fakeContract(currency){

let productX = {
  description: "Test product",
  quantity: 1,
  price: 
    {value: 0,
     fraction: 10000,
     currency: currency},
  product_id: 9,
  taxes: [{testtax: {value: 0,
                     fraction: 10000,
                     currency: currency}}],
  delivery_date: "1 January 1970",
  delivery_location: "loc_label"};

let juri = {
  coutry: "Test",
  city: "Test",
  state: "Test",
  region: "Test",
  province: "Test",
  zip_code: 4,
  street: "Test",
  street_number: 3
};


let lAddr = {
  coutry: "Test",
  city: "Test",
  state: "Test",
  region: "Test",
  province: "Test",
  zip_code: 4,
  street: "Test",
  street_number: 3
};

let mAddr = {
  coutry: "Test",
  city: "Test",
  state: "Test",
  region: "Test",
  province: "Test",
  zip_code: 4,
  street: "Test",
  street_number: 3
};

let auditor = {
  name: "auditor name",
  auditor_pub: "audit key pub",
  uri: "http://uri"
};

let mint = {
  master_pub: "mint master pub",
  url: "http://uri"
};

let contract = {
  amount:
    {value: 0,
     fraction: 10000,
     currency: currency},
  max_fee:
    {value: 0,
     fraction: 10000,
     currency: currency},
  transaction_id: 3,
  products: [productX],
  timestamp: "/Date(1448983992)/",
  refund_deadline: "/Date(1448999992)/",
  expiry: "/Date(1448999992)/",
  merchant_pub: "m pub here",
  merchant: {address: "merchAddr",
             name: "Test Merchant",
	     jurisdiction: "juriAddr"},
  H_wire: "h wire",
  H_contract: "h contract",
  auditors: [auditor],
  pay_url: "pay url",
  mints: [mint],
  locations: {juriAddr: juri,
              merchantAddr: mAddr,
	      loc_label: lAddr}

};


}
