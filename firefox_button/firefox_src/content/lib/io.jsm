/*

  This file is part of TALER
  Copyright (C) 2015, 2016 INRIA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>

*/

/**
 * @file This library is mainly dedicated to functions for retrieving data, both
 * on a network and from the extension 'bundle' (that is inside the 'data'
 * subdirectory).
 * @author Marcello Stanisci
 */

Components.utils.import("chrome://taler-button/content/lib/prettyPrint.jsm");

var EXPORTED_SYMBOLS = ['getFile', 'makeHttp'];

/**
 * @param todo - callback function which will be applied
 * on the ArrayBuffer which reflects the file named by `name`
 * @param {string} name - the name of the file being to read
 */
function getFile(todo, name) {

  tDebug("getFile called");

  var fileReq = Components.classes["@mozilla.org/xmlextras/xmlhttprequest;1"]
    .createInstance(Components.interfaces.nsIXMLHttpRequest);
  fileReq.open("GET", "chrome://taler-button/content/data/" + name, true);
  fileReq.responseType = "arraybuffer";

  fileReq.onload = function() {

    var arrFile = fileReq.response;
    if (arrFile) todo(arrFile)
    else tDebug("could not load file: " + name);


  };

  fileReq.onerror = function() {
    tDebug("onerror file load event");

  };

  fileReq.send(null);
}


/** 
 * Do HTTP activity asynchronously, by using XMLHttpRequest, according to the
 * given parameters
 *
 * @param onSuccFun - callback function which will be applied on the responseText
 * of the HTTP response
 * @param onErrFun - callback function called when the response status of this HTTP
 * request is not 200. This function will get the status code as its (unique) parameter
 * @param {string} method - which HTTP method is to be performed
 * @param {string} url - url to perform this `method` against
 * @param {object} value - any value to upload, in case of a POST/PUT
 */
function makeHttp(onSuccFun, onErrFun, method, url, value) {
  var httpReq = Components.classes["@mozilla.org/xmlextras/xmlhttprequest;1"]
    .createInstance(Components.interfaces.nsIXMLHttpRequest);
  httpReq.onload = function(mintEvt) {
    if (httpReq.readyState == 4)
      switch (httpReq.status) {
      case 200:
	  onSuccFun(httpReq.responseText);
        break;
      case 404:
        onErrFun(httpReq.status);
        break;
      default:
        onErrFun(httpReq.status);
        onErrFun(httpReq.responseText);
    }
  };
  httpReq.onerror = function(mintEvt) {

    onErrFun("httpReq.onerror reached"); // TBD
  };


  httpReq.open(method, url, true);
  httpReq.setRequestHeader('Content-type', 'application/json');
  httpReq.send(value);


  return httpReq;
}
