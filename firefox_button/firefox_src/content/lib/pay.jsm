/*

  This file is part of TALER
  Copyright (C) 2015, 2016 INRIA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>

*/

/**
 * @file This file's purpose is to expose to the main object 'Taler' those
 * functions that are not attachable to a button but represent a whole
 * protocol's 'block'; for example, the callback for receiving a
 * contract.
 * @author Marcello Stanisci
 */

Components.utils.import("resource://gre/modules/Services.jsm");
Components.utils
  .import("chrome://taler-button/content/lib/windowMgmt.jsm");
Components.utils
  .import("chrome://taler-button/content/lib/prettyPrint.jsm");
Components
  .utils.import("chrome://taler-button/content/lib/reserves.jsm");
Components
  .utils.import("chrome://taler-button/content/lib/io.jsm");
Components
  .utils.import("chrome://taler-button/content/lib/util.jsm");
Components
  .utils.import("chrome://taler-button/content/lib/emscripIface.jsm");
Components.utils.import("resource://gre/modules/FileUtils.jsm");
Components.utils.import("resource://gre/modules/ctypes.jsm");

var EXPORTED_SYMBOLS = ['handleContract',
			'testAmountGetters',
                        'countCoins',
			'testPayment',
			'testNewPayment',
			'testCoinPick'];
/**
 * Handler to the reception of a contract. Actual performer of the payment.
 * Does not return any value, since it is supposed to manage errors or displaying
 * a fullfillment page
 * 
 * @param {string} contractJson - JSON gotten as a response to a /taler/contract
 * HTTP GET. See the [API]{@link http://api.taler.net/api-merchant.html#contract}
 */
function handleContract (contractJson) {
  
  let window = getLastWindow();
  let contractObj = JSON.parse(contractJson.detail);
  let merchantSite = contractJson.originalTarget.domain;

  /* TODO stringify the 'contract' object in a way compatible with
    'JSON_COMPACT | JSON_PRESERVE_ORDER', hash it, and verify the
    signature provided by the merchant against this hashed contract.
    As of now, no verification is made on the contract, and the
    contract's hash is taken (and used as is to generate the deposit
    permission) from the contract */

    tDebug(JSON.stringify(contractObj), true); 
    let price = contractObj.contract.amount;

    let mint = contractObj.contract.mints.pop();

    if (mint == null){
      showOutput("No mints indicated by this merchant", true);
      return;
    }

    showOutput("First mint: " + mint.url,
               false);
    let havings = getPaymentInput(mint.url, contractObj.contract.amount);
    showOutput("HAVINGS" + JSON.stringify(havings), false);
    havings.sort(sortPaymentCoins);
    let choice = knapsackSolver(havings,
                                price,
				true,
				contractObj.contract.max_fee);
    showOutput("First result " + JSON.stringify(choice),
               false);
    /* the mint we have sufficient funds for this purchase will be
      the choice */
    while ((GNUNET_NO == choice || GNUNET_SYSERR == choice)
           && (mint = contractObj.contract.mints.pop())) {
    
    
      showOutput("Other mint: " + mint.url,
                 true);
      havings = getPaymentInput(mint.url);
      havings.sort(sortPaymentCoins);
      choice = knapsackSolver(havings,
                              price,
	                      true,
			      contractObj.contract.max_fee);
     showOutput("Other result " + JSON.stringify(choice),
                false);
    }

    if (GNUNET_NO == choice){
      window.alert("Insufficient money in the wallet"
                   + " for all mints"); 
      return;
    }

    if (GNUNET_SYSERR == choice){
      window.alert("Not possible to satisfy merchant"
                            + " constraint about the deposit fee"); 
      return;
    }

    tDebug("Picked coins " + JSON.stringify(choice), false);
    let amountNumber = contractObj.contract.amount.value +
      (contractObj.contract.amount.fraction / 1000000);
    let contractObjPar = 
      {'ctrObj' :
        {  'amount' :
	    amountNumber.toString() + " " + contractObj.contract.amount.currency,
           'shop' :
            contractObj.contract.merchant.name
        },
       'choice' : null};

    /* visualize the contract. The field 'choice' will hold what decided
      by the customer: pay, or refuse. */
    window.openDialog("chrome://taler-button/content/contract.xul",
                  "",
                  "chrome, dialog, modal, resizable=yes",
		   contractObjPar).focus();

    if (contractObjPar.choice != "paid")
      return;

    showOutput("Paying with : " + JSON.stringify(choice), false);
    let dbHandle = getDbHandle();
    let coinStmt;
    try {
    coinStmt =  dbHandle.createStatement("SELECT * FROM coins"
                                         + " WHERE denom = :denom"
                                         + " AND pending = :no"
			                 + " AND spent = :no");
    }
    catch(err){
      window.alert("Please withdraw coins prior to buy!");
      return;
    }

    let depFeeStmt;
    let pendingStmt;
    try{
      depFeeStmt = dbHandle.createStatement("SELECT value, fraction, currency"
                                            + " FROM fee_deposit_table"
                                            + " WHERE denom = :thisDenom");

      pendingStmt = dbHandle.createStatement("UPDATE coins SET pending = :ok"
	                                     + ", transaction_id = :transactionId"
				             + " WHERE coin_priv = :thisPriv");
    }
    catch(e){
      printError(e);
      return;
    }

    /* Preparing the "global" part of the deposit permission; that
      is: everything except coins */
    let DepositPermission = {
      H_wire : contractObj.contract.H_wire,
      H_contract : contractObj.H_contract,
      transaction_id : contractObj.contract.transaction_id,
      merchant_pub : contractObj.contract.merchant_pub, 
      refund_deadline : contractObj.contract.refund_deadline,
      timestamp : contractObj.contract.timestamp,
      mint : mint.url,
      coins : new Array()};

    /* This outer cycle loops over each denomination the wallet is
      using at least one coin thereof */
    for(let i = 0; i < choice.length; i++){

      coinStmt.params.denom = choice[i].denom_pub;
      coinStmt.params.no = false;

      /* Each denomination has only one deposit fee, so make
        this query just once per denomination */

      depFeeStmt.params.thisDenom = choice[i].denom_pub;
      if (!depFeeStmt.executeStep()){
        showOutput("Deposit fee not found for denom"
	           + " " + choice[i].denom_pub,
		   true);
        depFeeStmt.finalize();
	dbHandle.close();
	return;
      
      }
        
      /* Looping over each coin of the same denomination */
      for (let j = 0; j < choice[i].no; j++){
        
	/* Extra check */
	if (!coinStmt.executeStep()){
	  /* Todo */
          showOutput("Non sufficient funds, or no coins for"
	             + " denom " + choice[i].denom_pub,
		     true);
	  depFeeStmt.finalize();
	  coinStmt.finalize();
	  dbHandle.close();
	  return;
	}
	
        
        let coinPrivEnc = coinStmt.row.coin_priv;

	pendingStmt.params.ok = true;
	pendingStmt.params.thisPriv = coinPrivEnc;
	pendingStmt.params.transactionId = contractObj.contract.transaction_id;
        pendingStmt.executeStep();
	pendingStmt.reset();


        let coinPubPtr =
	  TWRALLeddsaPublicKeyFromPrivString(coinPrivEnc); 
        let coinPubEnc =
	  getCrockedValue(coinPubPtr,
	                  EDDSAKEY_SIZE);
        let coinValue =  coinStmt.row.value;
        let coinFraction =  coinStmt.row.fraction;
        let coinCur =  coinStmt.row.currency;
        let ubSigEnc = coinStmt.row.mint_sig;      
        let denomKey = coinStmt.row.denom;
      
        showOutput("Picked up coin: value = "
                   + coinValue
                   + ", fraction = "
	           + coinFraction
	           + ", currency = "
	           + coinCur
	           + ", pending (?): "
	           + coinStmt.row.pending,
	           false);

        showOutput("Coin's ub signature: " + ubSigEnc, false);
        showOutput("Corresponding denom. key = " + denomKey, false);
        showOutput("Coin pub = " + coinPubEnc, false);

        let feeValue = depFeeStmt.row.value;
        let feeFraction = depFeeStmt.row.fraction;
        let feeCurrency = depFeeStmt.row.currency;

        showOutput("fee for depositing this coin is : "
                   + feeValue
	           + "."
	           + feeFraction
	           + " "
	           + feeCurrency,
	           false);
 
	/* Each coin pays a fraction of the total price corresponding
	  to its denomination minus its deposit fee, so summing
	  the paid amount (for a single coin) with the deposit fee yields
	  exactly the coin's value */

        let summed = TWRALLgetAmount32(coinValue,
                                       coinFraction,
				       coinCur);
        if (summed == NULL) {
          showOutput("errors in summing amount+deposit_fee\n");
          return;
        }

        let feePtr = TWRALLgetAmount32(feeValue,
                                       feeFraction,
                                       feeCurrency);
    
        showOutput("fee is pointing : " + feePtr, false);

        /***** Hashing the contract: for future use *****/
        //let strPtr = ALLstr(JSON.stringify(contractObj.contract));

        /* WARNING: possible (slight) differences with the jansson's
        JSON_COMPACT method for stringifying the JSONs which could make
        the match of this contract with the one held by the merchant fail
        on the merchant's side */
    
        //let contractHashBuf = TWRALLhash(strPtr,
                                         //contractObj.contract.length);
        //let contractHashEnc = getCrockedValue(contractHashBuf,
                                              //HASHCODE_SIZE);
        //emscFree(strPtr);
        //TWRgnunetFree(contractHashBuf);
        //showOutput("hashed contract : " + contractHashEnc, false);
    
        //showOutput("(of stringified JSON : "
                   //+ JSON.stringify(contractObj.contract)
    	           //+ ")",
    	           //false);

        /***** End of Hasing facility *****/

        /* signing the depperm */
        
        // get timestamps
        let timestampNumber =
          getTimestampNumber(contractObj.contract.timestamp);
        let refundNumber =
          getTimestampNumber(contractObj.contract.refund_deadline);
    
        tDebug("timestamp is : "
                   + contractObj.contract.timestamp,
    	           true);
        tDebug("refund deadline is : "
                   + contractObj.contract.refund_deadline,
    	           true);
        tDebug("timestamp number I is : " + timestampNumber, true);
        tDebug("refund number I is : " + refundNumber, true);
        tDebug("transaction id is : "
                   + contractObj.contract.transaction_id,
    	           true);

        let hContractPtr = ALLgetDeCrock(contractObj.H_contract);
        tDebug("H_contract got from merchant : "
                   + contractObj.H_contract,
    	           true);
        tDebug("merchant_pub : "
                   + contractObj.contract.merchant_pub,
    	           true);
        let hWirePtr = ALLgetDeCrock(contractObj.contract.H_wire);
        tDebug("H_wire got from merchant : "
                   + contractObj.contract.H_wire,
    	           true);
        let merchantPubPtr =
	  ALLgetDeCrock(contractObj.contract.merchant_pub);
	  tDebug("merchant pub: " + contractObj.contract.merchant_pub, true);
        let coinPrivPtr =
	  TWRALLeddsaPrivateKeyFromString(coinPrivEnc);
        let deppermSig =
          TWRALLsignDepositPermission(hContractPtr.dec,
                                      hWirePtr.dec,
                                      timestampNumber,
                                      refundNumber,
    				      contractObj.contract.transaction_id,
    				      summed,
    				      feePtr,
    				      merchantPubPtr.dec,
    				      coinPubPtr,
    				      coinPrivPtr);
    
        let deppermSigEnc = getCrockedValue(deppermSig,
	                                    EDDSASIG_SIZE);
        tDebug("depperm signature base32 : "
	       + deppermSigEnc,
	       true);
    
        emscFree(hContractPtr.dec);
        emscFree(hWirePtr.dec);
        emscFree(merchantPubPtr.dec);
        TWRgnunetFree(coinPrivPtr);
        TWRgnunetFree(deppermSig);
        TWRgnunetFree(coinPubPtr);
        TWRgnunetFree(feePtr);

        let coin = {f : { value : TWRgetValue(summed),
	                  fraction : TWRgetFraction(summed),
			  currency : TWRgetCurrency(summed) },
		    coin_pub : coinPubEnc,
		    denom_pub : denomKey,
		    ub_sig : ubSigEnc,
		    coin_sig : deppermSigEnc };

        // actually craft the JSON
        DepositPermission.coins.push(coin); 
        TWRgnunetFree(summed);
    
      } /* single coin iteration */
      depFeeStmt.reset();
      coinStmt.reset();
    } /* current denomination */

    tDebug(JSON.stringify(DepositPermission), true);
    // send JSON to merchant

    let payUrlRe = /^\//;
    let merchantPayUrl = "http://" + merchantSite;
    if (payUrlRe.test(contractObj.pay_url))
      merchantPayUrl += contractObj.pay_url;
    else
      merchantPayUrl += "/" + contractObj.pay_url;
    makeHttp(
    /* 'confirmation' is already the stringified body */
    function (confirmation){
      tDebug("Merchant says: " + confirmation, true);
      
      htmlParser = Components.classes["@mozilla.org/xmlextras/domparser;1"]
                       .createInstance(Components.interfaces.nsIDOMParser);
      let doc = htmlParser.parseFromString(confirmation, "text/html");

      //window.gBrowser.contentDocument.documentElement.innerHTML = doc.documentElement.innerHTML;
      window.gBrowser.contentDocument.replaceChild(doc.documentElement,
                                                   window.gBrowser.contentDocument.documentElement);
      let db = getDbHandle();
      let spentStmt;
      try{
        spentStmt = db.createStatement("UPDATE coins SET spent = :ok"
	                               + ", error_proof = :nullify"
	                               + " WHERE transaction_id = :transactionId");
        spentStmt.params.ok = true;
	spentStmt.params.transactionId = contractObj.contract.transaction_id;
	spentStmt.params.nullify = null;
        spentStmt.executeAsync();
      }
      catch(e){
        printError(e);
	return;
      }
	db.asyncClose();
     },
     function (paymentStatus){
       /* here all the various things to do in case of errors */
       showOutput(paymentStatus + " on " + merchantPayUrl, true);
     },
     "POST",
     merchantPayUrl,
     JSON.stringify(DepositPermission));
    coinStmt.finalize();
    depFeeStmt.finalize();
    pendingStmt.finalize();
    dbHandle.close();

} 

/**
 * Maps a denomination key to the number of coins available (in the wallet)
 * for that denomination
 *
 * @typedef denomNo
 *
 * @type {object}
 *
 * @property {string} denom - base32 encoding of denomination key in netwrok
 * format
 *
 * @property {number} no - how many coins of `denom` the wallet currently has
 */

/**
 * Count how many coins for each denomination at the given
 * mint the wallet owns
 *
 * @param {string} mint - base URL of the mint which minted
 * the coins to count
 *
 * @return {object} accounting - the accounting of coins minted by `mint`
 * @return {denomNo[]} accounting.arr
 * @return {Object.<string, number>} accounting.map - map from denomination keys base32
 * encodings to the quantity of the mapped denomination key. Provide same information
 * as `accounting.arr` just in a different format
 */
function countCoins(mint){
  showOutput("countCoins ", false);
  let ret = {'arr' : [],
             'map' : {}};

  let db = getDbHandle();
  let countStmt;
  try{
  countStmt = db.createStatement("SELECT mint_base_url, denom, count(*)"
                                 + " AS no FROM coins WHERE mint_base_url"
		                 + " = :mint AND pending = :no AND spent"
		                 + " = :no GROUP BY denom");

  countStmt.params.mint = mint;
  countStmt.params.no = false;
  }
  catch(e){
    printError(e);
    return;
  }

  while(countStmt.executeStep()){
    
    let no = countStmt.row.no;
    let denom = countStmt.row.denom;
    showOutput("There are "
               + no
	       + " coins of"
               + " denomination "
	       + denom.substring(denom.length - 16,
                                 denom.length),
               false);
    let entry = {'denom' : denom,
                 'no' : no};
    ret.arr.push(entry);
    ret.map[denom] = no;
  }

  countStmt.reset();
  countStmt.finalize();
  db.close();
  return ret;

  // Asynchronous version

  /*let handlerObj =
    {'handleResult' : function (resultSet){
       for (let row = resultSet.getNextRow();
            row;
            row = resultSet.getNextRow()) {
         
	 let denom = row.getResultByName("denom");
	 let no = row.getResultByName("no");
	 showOutput("You have "
	            + no
		    + " coins of"
	            + " denomination "
		    + denom.substring(denom.length - 16,
		                      denom.length),
		    true);
	 }
      },
     'handleError' : function (){},
     'handleCompletion' : function (){}
    };

  countStmt.executeAsync(handlerObj);*/


}

/**
 * Further level of abstraction around `countCoins`. This function's
 * output is typically passed to the KP solver
 *
 * @param {string} mint - base URL of the mint whose coins
 * are to be counted
 *
 * @param {talerAmount} price - the price of the purchase. This
 * value is needed since this function is typically called in the
 * context of a payment
 * 
 * @return {kpCompliantDenom[]} - this function adds 'no' and 'price'
 * fields to each cell of this array
 */
function getPaymentInput(mint, price){

  let q = countCoins(mint);
  let knInput = fetchDenomsAmntFeeHndl(q.arr);
  for (let i = 0; i < knInput.length; i++){
    knInput[i].no = q.map[knInput[i].denom];
    knInput[i].price = price;
    showOutput("entry " + JSON.stringify(knInput[i]),
               false);
  }
  return knInput;
}

function testCoinPick(){

  let q = getPaymentInput(getMintUrl());
  showOutput(JSON.stringify(q), false);
}

/**
 * Pseudo code for new coins selection
 *
 * let arr = getPaymentInput(mint);
 * let picked;
 * let choice = [];
 * while (acc < price) {
 *  acc += picked[0];
 *  picked = sortPaymentCoinsPriced(arr, price, mint);
 *  while(picked[0].no == 0)
 *   picked.shift();
 *  if (picked.length == 0)
 *   // no money. report error
 *  choice[0].no--;
 *  choice.push(picked[0]);
 *  price -= picked[0];
 *    }
 * must return a `knOutput` "type";
 */

/**
 * Sort coins according to the price to pay
 *
 * @param {kpCompliantDenom[]} arr - coins to be sorted
 * @param {number} pricePtr - pointer to the the price the
 * coins have to be sorted according to
 * @param {string} mint - mint base URL
 * @return {void} - `arr` gets sorted in-place
 */
function sortPaymentCoinsPriced(arr, pricePtr, mint){
  tDebug("sortPaymentCoinsPriced (" + JSON.stringify(arr) + ", "
         + pricePtr + ", " + mint + ")", false);
  if (arr.length < 2)
    return;
  /**
   * Compute all the withdrawing fees that are necessary, in
   * case `aPtr` gets included in the coins to spend
   *
   * @param {number} aPtr - the allocated `struct TALER_Amount`
   * corresponding to the coin being processed
   * @param {number} pricePtr - the allocated `struct TALER_Amount`
   * corresponding to the (residual) price to pay
   * @return {number} the allocated computed amount corresponding to
   * the fees to pay if `aPtr` gets spent
   */
  function getImplicitRefreshFee(aPtr, pricePtr){
    tDebug("getImplicitRefreshFee (" + dumpAmountPtr(aPtr)
           + ", " + dumpAmountPtr(pricePtr)
	   + ")", false);
    let difference = TWRALLgetAmount32(0, 0, TWRgetCurrency(pricePtr));
     
    if (TamountCmp(pricePtr, aPtr) >= 0) // price > coin
      return difference;
    else TamountSubtract(difference, aPtr, pricePtr);
    tDebug("getImplicitRefreshFee, difference " + dumpAmountPtr(difference), false);
    let db = getDbHandle(); 
    let denomsStmt = db.createStatement("SELECT denom_pub FROM denom_table"
                                         + " WHERE mint = :thisMint");
    denomsStmt.params.thisMint = mint;
    let availDenoms = []; 
    while(denomsStmt.executeStep())
      availDenoms.push({denom: denomsStmt.row.denom_pub});
    denomsStmt.finalize();
    db.close();
    let knapsackInput = fetchDenomsAmntFeeHndl(availDenoms);
    knapsackInput.sort(withdrawSortFunction);
    let knapsackOutput = knapsackSolver(knapsackInput,
                                        {value: TWRgetValue(difference),
					 fraction: TWRgetFraction(difference),
					 currency: TWRgetCurrency(difference)},
					false);
    /* sum up wd fees */
    let wdAcc = TWRALLgetAmount32(0, 0, TWRgetCurrency(pricePtr));
    for (let j=0; j<knapsackOutput.length; j++){

      tDebug("getImplicitRefreshFee, potential wd bundle "
             + "j = " + j
	     + " denom "
	     + JSON.stringify(knapsackOutput[j].denom)
	     + " # " + knapsackOutput[j].no,
	     false);
      let coinIndex = findCoinInArray(knapsackInput,
                                      knapsackOutput[j].denom,
				      sameAmount,
				      (arg) => arg.volume);

      let wdFee = knapsackInput[coinIndex].value.withdrawFee;      

      let wdFeePtr = TWRALLgetAmount32(wdFee.value,
                                       wdFee.fraction,
				       wdFee.currency);

      if (GNUNET_SYSERR == multiplyAmount(wdFeePtr, knapsackOutput[j].no))
        tDebug("Failed to multiply wd fee per number of coins to potentially withdraw", true);
      if (GNUNET_SYSERR == TamountAdd(wdAcc, wdAcc, wdFeePtr))
        tDebug("Failed to add wd fees regarding coins to potentially withdraw", true);
      TWRgnunetFree(wdFeePtr);
    }
    /* To be freed by the sorting function */
    tDebug("getImplicitRefreshFee: for " + dumpAmountPtr(aPtr)
           + ", and price " + dumpAmountPtr(pricePtr)
	   + ": wd fees are: " + dumpAmountPtr(wdAcc), false);
    return wdAcc;
  
  }

  /**
   * Rank according to dep_fee/coin_value + total_refresh_fees (making
   * the lowest win)
   *
   * @param {kpCompliantDenom} a 
   * @param {kpCompliantDenom} b 
   * @return {number} x<0 if a is better than b, x>=0 otherwise
   */
  function sortingCb(a, b){
    tDebug("sortingCb: priced sorting elems: " + dumpAmount(a.volume)
           + ", " + dumpAmount(b.volume), false);
    let depFeeAPtr =
      TWRALLgetAmount32(a.value.depositFee.value,
                        a.value.depositFee.fraction,
                        a.value.depositFee.currency); 
    let depFeeBPtr =
      TWRALLgetAmount32(b.value.depositFee.value,
                        b.value.depositFee.fraction,
                        b.value.depositFee.currency); 

    let aPtr = TWRALLgetAmount32(a.volume.value,
                                 a.volume.fraction,
				 a.volume.currency);

    let bPtr = TWRALLgetAmount32(b.volume.value,
                                 b.volume.fraction,
				 b.volume.currency);

    let refreshA = getImplicitRefreshFee(aPtr, pricePtr);
    let refreshB = getImplicitRefreshFee(bPtr, pricePtr);
    tDebug("sortingCb: refreshA, " + dumpAmountPtr(refreshA), false);
    tDebug("sortingCb: refreshB, " + dumpAmountPtr(refreshB), false);

    tDebug("sortingCb: nfa " + numberFromAmountPtr(depFeeAPtr), false);
    tDebug("sortingCb: nfa " + numberFromAmountPtr(depFeeBPtr), false);
    tDebug("sortingCb: nfa coin " + numberFromAmountPtr(aPtr), false);
    tDebug("sortingCb: nfa coin " + numberFromAmountPtr(bPtr), false);
    tDebug("sortingCb: 1st run " + Number(numberFromAmountPtr(depFeeAPtr) / numberFromAmountPtr(aPtr)), false);
    tDebug("sortingCb: 1st run " + Number(numberFromAmountPtr(depFeeBPtr) / numberFromAmountPtr(bPtr)), false);

    let totA = (numberFromAmountPtr(depFeeAPtr) / numberFromAmountPtr(aPtr))
      + numberFromAmountPtr(refreshA);
    let totB = (numberFromAmountPtr(depFeeBPtr) / numberFromAmountPtr(bPtr))
      + numberFromAmountPtr(refreshB);

    tDebug("sortingCb: totA, " + totA, false);
    tDebug("sortingCb: totB, " + totB, false);

    TWRgnunetFree(aPtr);
    TWRgnunetFree(bPtr);
    TWRgnunetFree(depFeeAPtr);
    TWRgnunetFree(depFeeBPtr);
    TWRgnunetFree(refreshA);
    TWRgnunetFree(refreshB);

    return totA - totB;
  }

  /* this case addresses both an empty wallet and a wallet
    having just one kind of coins. The according action to
    take must be taken by the caller.. */
  if (arr.length < 1)
    return arr;

  arr.sort(sortingCb);
}

/**
 * Manager of the new coins selection algorithm. For each chosen
 * coin, it re-sort the coins array according to the residual price
 * to pay. XXX doesn't take deposit fee into account, still
 *
 * @param {talerAmount} price - the price to pay
 * @param {talerAmount} maxFee - maximum fee the merchant can pay
 * for this purchase
 * @param {string} mint - base URL of the mint which minted the coins
 * to use in this payment
 * @return {knOutput[] | null} - the choice of coins to pass to the deposit
 * permission builder, or null if not enough coins are found
 */
function coinsSelection(price, maxFee, mint){
  let maxFeePtr = TWRALLgetAmount32(maxFee.value,
                                    maxFee.fraction,
	                            maxFee.currency);
  let crossed = false; /* has this payment crossed the max fee treshold? */
  let dumpCollection = function(collection){
    for (let i = 0; i < collection.length; i++)
      tDebug("Position " + i + ", coin " + dumpAmount(collection[i].volume)
             + ", #" + collection[i].no, true);  
  };
  let pricePtr = TWRALLgetAmount32(price.value,
                                   price.fraction,
				   price.currency);
  let zeroPtr = TWRALLgetAmount32(0, 0, price.currency);
  let ownedCoins = getPaymentInput(mint);
  tDebug("coinsSelection " + JSON.stringify(ownedCoins), false);
  if (typeof ownedCoins === "undefined"){
    showOutput("No coins minted by " + mint + "!", true);
    return;
  }
  let choice = [];
  let undone = true;
  while(TamountCmp(zeroPtr, pricePtr) < 0 && undone){
    dumpCollection(ownedCoins);
    /* out of money and still price not achieved */
    if (ownedCoins.length == 0){
      tDebug("empty wallet", true);
      return null; 
    }
    sortPaymentCoinsPriced(ownedCoins, pricePtr, mint);
    tDebug(JSON.stringify(ownedCoins[0]), true);
    while(typeof ownedCoins[0] !== "undefined" && ownedCoins[0].no == 0)
      ownedCoins.shift();
    if (typeof ownedCoins[0] === "undefined"){
      TWRgnunetFree(pricePtr);
      return choice; 
    }
    ownedCoins[0].no--;
    let chosenCoinPtr = TWRALLgetAmount32(ownedCoins[0].volume.value,
                                          ownedCoins[0].volume.fraction,
			                  ownedCoins[0].volume.currency);
    let thisFeePtr;
    if (TamountCmp(pricePtr, chosenCoinPtr) < 0)
      undone = false;
    else {
      thisFeePtr = TWRALLgetAmount32(ownedCoins[0].value.depositFee.value,
                                     ownedCoins[0].value.depositFee.fraction,
                                     ownedCoins[0].value.depositFee.currency);
      if (TamountCmp(maxFeePtr, thisFeePtr) < 1 && !crossed){
        /* now maxFeePtr holds the amount (of deposit fee) to be paid
	  entirely by the user. Note that this value is to be added to the
	  price to be still paid */  
        TamountSubtract(maxFeePtr, thisFeePtr, maxFeePtr); 
	TamountAdd(pricePtr, princePtr, maxFeePtr);
	crossed = true;
      }
      TamountSubtract(pricePtr, pricePtr, chosenCoinPtr);
      if (crossed)
        TamountAdd(pricePtr, pricePtr, thisFeePtr);
      TWRgnunetFree(thisFee);
    }
    TWRgnunetFree(chosenCoinPtr);

    let chosen = findCoinInArray(choice,
                                 ownedCoins[0].volume,
				 sameAmount,
				 (arg) => arg.denom);

    if (GNUNET_NO != chosen){
      choice[chosen].no++;  
    }
    else{
      choice.push({denom: ownedCoins[0].volume,
                   denom_pub: ownedCoins[0].denom,
                   no: 1}); 
    }
  }
  TWRgnunetFree(pricePtr);
  return choice;
}

/**
 * Implements the policy to sort coins in the context of payments.
 * This function, which is typically invoked by 'Array.sort(..)' is
 * mainly used to prepare the input coins for the KP solver, so that
 * the "best" coins are placed in the first positions of the `input`
 * parameter passed to the KP solver
 *
 * @param {kpCompliantDenom} entryOne
 * @param {kpCompliantDenom} entryTwo
 * @return {number} - some x < 1 if entryOne is better than entryTwo,
 * some x >= 0 otherwise
 */
function sortPaymentCoins(entryOne, entryTwo){

  /* Picked from the first one since is not possible to
    pass a third parameter to the sorting function */
  let pricePtr =
    TWRALLgetAmount32(entryOne.price.value,
                      entryOne.price.fraction,
                      entryOne.price.currency);
  let coinOnePtr =
    TWRALLgetAmount32(entryOne.volume.value,
                      entryOne.volume.fraction,
                      entryOne.volume.currency);
  let depFeeOnePtr =
    TWRALLgetAmount32(entryOne.value.depositFee.value,
                      entryOne.value.depositFee.fraction,
                      entryOne.value.depositFee.currency); 
  let coinTwoPtr =
    TWRALLgetAmount32(entryTwo.volume.value,
                      entryTwo.volume.fraction,
                      entryTwo.volume.currency);
  let depFeeTwoPtr =
    TWRALLgetAmount32(entryTwo.value.depositFee.value,
                      entryTwo.value.depositFee.fraction,
                      entryTwo.value.depositFee.currency); 
  let distanceOne = TWRALLgetAmount32(0, 0, entryOne.price.currency);
  if (TamountCmp(pricePtr, coinOnePtr) > 0)
    TamountSubtract(distanceOne, pricePtr, coinOnePtr);
  else if (TamountCmp(pricePtr, coinOnePtr) < 0)
    TamountSubtract(distanceOne, coinOnePtr, pricePtr);
  else distanceOne = TWRALLgetAmount32(0, 0, entryOne.price.currency);

  let distanceTwo = TWRALLgetAmount32(0, 0, entryOne.price.currency);
  if (TamountCmp(pricePtr, coinTwoPtr) > 0)
    TamountSubtract(distanceTwo, pricePtr, coinTwoPtr);
  else if (TamountCmp(pricePtr, coinTwoPtr) < 0)
    TamountSubtract(distanceTwo, coinTwoPtr, pricePtr);
  else distanceTwo = TWRALLgetAmount32(0, 0, entryOne.price.currency);

  TWRgnunetFree(pricePtr);
  let xOne = TWRmultiplyAmounts(distanceOne, depFeeOnePtr);
  let xTwo = TWRmultiplyAmounts(distanceTwo, depFeeTwoPtr);
  let facOne = (TWRgetValue(coinOnePtr) + (TWRgetFraction(coinOnePtr)/1000000)) * xTwo;
  let facTwo = (TWRgetValue(coinTwoPtr) + (TWRgetFraction(coinTwoPtr)/1000000)) * xOne;
  TWRgnunetFree(distanceOne);
  TWRgnunetFree(distanceTwo);
  TWRgnunetFree(depFeeOnePtr);
  TWRgnunetFree(depFeeTwoPtr);

  showOutput("Coin One: "
             + getPrettyAmount(coinOnePtr)
             + " distance: "
             + getPrettyAmount(distanceOne)
	     + " xOne: "
	     + xOne
	     + " facOne: "
	     + facOne
	     , false);

  showOutput("Coin Two: "
             + getPrettyAmount(coinTwoPtr)
             + " distance: "
             + getPrettyAmount(distanceTwo)
	     + " xTwo: "
	     + xTwo
	     + " facTwo: "
	     + facTwo
	     , false);

  let cmp = facTwo - facOne;
  showOutput("cmp: " + cmp, false);
  if (cmp != 0){
    TWRgnunetFree(coinOnePtr);
    TWRgnunetFree(coinTwoPtr);
    return cmp;
  }
  cmp = TamountCmp(coinOnePtr, coinTwoPtr);
  TWRgnunetFree(coinOnePtr);
  TWRgnunetFree(coinTwoPtr);
  if (cmp != 0)
    return cmp;
  return getTimestampNumber(entryOne.value.expirationDate)
           - getTimestampNumber(entryTwo.value.expirationDate);
}

/**
 * Just choose coins using the new algorithm
 *
 * @param {string} mint - which mint whose coins should be picked
 * @param {string} currency - which currency the picked coins should be of
 */
function testNewPayment(mint, currency){

  let price = {'value' : 2,
               'fraction' : 10000,
	       'currency' : currency};

  let maxDepFee = {'value' : 20,
                   'fraction' : 0,
	           'currency' : currency};

  let check = coinsSelection(price, maxDepFee, mint);
  showOutput(JSON.stringify(check), true);
}

/**
 * Just choose coins simulating a payment, used to debug the
 * KP solver. Prints the result on the current HTML page
 *
 * @param {string} mint - which mint whose coins should be picked
 * @param {string} currency - which currency the picked coins should be of
 */
function testPayment(mint, currency){

  let price = {'value' : 0,
               'fraction' : 10000,
	       'currency' : currency};

  let q = getPaymentInput(mint, price);
  q.sort(sortPaymentCoins);

  let maxdepfee = {'value' : 20,
                   'fraction' : 0,
	           'currency' : currency};

  let check = knapsackSolver(q,
                             price,
			     true,
			     maxdepfee);

  showOutput(JSON.stringify(check), true);
}

/** 
 * Testcase for `struct TALER_Amount` C-land allocators.
 * Prints outcome on the current HTML page
 */
function testAmountGetters(){
  
  let amone = TWRALLgetAmount32(1,20001,"PUDOS");
  let amtwo = TWRALLgetAmount32(4,9000,"PUDOS");
  let sum = null;

  TamountAdd(sum, amone, amtwo);

  showOutput("Sum : " + TWRgetValue(sum)
             + "."
	     + TWRgetFraction(sum)
	     + " "
	     + TWRgetCurrency(sum), true);

  TWRgnunetFree(amone);
  TWRgnunetFree(amtwo);
  TWRgnunetFree(sum);
}
