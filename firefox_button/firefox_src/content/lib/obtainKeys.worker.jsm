/*

  This file is part of TALER
  Copyright (C) 2015, 2016 INRIA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>

*/

/**
 * @file routines to be called from within a Web Worker, mostly
 * for verifying (mint's) keys signature
 * @author Marcello Stanisci
 */

importScripts("chrome://taler-button/content/lib/libWrapper.jsm");

var GNUNET_OK = 1;
var GNUNET_NO = 0;
var GNUNET_SYSERR = -1;
var NULL = 0;
var HASHCODE_SIZE = 64;
var PTR_SIZE = 4;
var EDDSAKEY_SIZE = 256 / 8;
var EDDSASIG_SIZE = 2 * (256 / 8);
var BLINDFACT1024_SIZE = 1024 / 8;

/**
 * NOTE. Throughout this worker, no prior conversion is done before passing
 * values to emscripted routines. That's because most of the converting routines
 * needs the 'window' object, which is both missing in a worker context. Although
 * it would be possible to pass 'window' along the worker gotten message, by
 * design that's not done. Whereas some conversions could have been done, it has
 * been chosen to not do it if the called (emscripted) function has still some
 * unconverted parameter.
 */

/**
 * NOTE: Re-defined here since util.jsm is not importable here due to its calls
 * to Components.*. The same applies to the definitions above
 *
 * @param {string} dateStr - date in the format "/Date(x)/" where x is the number
 * of seconds since the Epoch
 * @return {Number} - x taken from `dateStr`
 */
function getTimestampNumber(dateStr) {

  var mec = dateStr.match(/Date\(([0-9]+)\)/);
  return Number(mec[1]);

}

/**
 * Verify the given signkey against the given masterkey
 *
 * @param {object} signkeyObj - the signkey whose signature is to be verified.
 * This object obeys to the following [specification]{@link http://api.taler.net/api-mint.html#obtaining-mint-keys}
 * (see the `signkeys` entry)
 * @param {string} masterkey - the mint's master key
 * @return GNUNET_OK on success, GNUNET_NO if the signature is invalid,
 * GNUNET_SYSERR upon errors
 */
function verifySignkeys(signkeyObj, masterkey){

  var TWRverifySignkeyEnc = Module.cwrap('TALER_WR_verify_sign_key_enc',
                                         'number',
                                        ['string',
				         'number',
				         'number',
				         'number',
				         'string',
				         'string']);


  return TWRverifySignkeyEnc(masterkey,
                             getTimestampNumber(signkeyObj.stamp_start),
                             getTimestampNumber(signkeyObj.stamp_expire),
                             getTimestampNumber(signkeyObj.stamp_end),
                             signkeyObj.key,
	                     signkeyObj.master_sig);
}

/**
 * Verify the given denomination key against the master key.
 *
 * @param {object} denomAggregate - value set associated with a single denomination key
 * as returned by /keys
 *
 * @param {string} masterkey - string being the encoding of the masterkey which
 * signed this denomination key
 *
 * @return GNUNET_OK on success, GNUNET_NO if the signature is invalid,
 * GNUNET_SYSERR upon errors
 */
function verifySingleDenom(denomAggregate, masterkey)
{

  var TWRverifyDenomEnc = Module.cwrap('TALER_WR_verify_denom_enc',
                                       'number',
                                      ['string',
				       'string',
				       'string',
				       'number',
				       'number',
				       'number',
				       'number',
				       'number',
				       'number',
				       'string',
				       'number',
				       'number',
				       'string',
				       'number',
				       'number',
				       'string',
				       'number',
				       'number',
				       'string']);

  return TWRverifyDenomEnc(denomAggregate.denom_pub,
                           masterkey,
	                   denomAggregate.master_sig,
                           getTimestampNumber(denomAggregate.stamp_start),
                           getTimestampNumber(denomAggregate.stamp_expire_withdraw),
                           getTimestampNumber(denomAggregate.stamp_expire_deposit),
                           getTimestampNumber(denomAggregate.stamp_expire_legal),
                           denomAggregate.value.value,
                           denomAggregate.value.fraction,
                           denomAggregate.value.currency,
                           denomAggregate.fee_withdraw.value,
                           denomAggregate.fee_withdraw.fraction,
                           denomAggregate.fee_withdraw.currency,
                           denomAggregate.fee_deposit.value,
                           denomAggregate.fee_deposit.fraction,
	                   denomAggregate.fee_deposit.currency,
                           denomAggregate.fee_refresh.value,
                           denomAggregate.fee_refresh.fraction,
	                   denomAggregate.fee_refresh.currency);

}

/**
 * Verify the concatenation of each key's hash
 *
 * @param {string[]} denomsArray - strings being each denomination
 * key's encoding (in network format)
 *
 * @param {string} signkey - the signkey used to sign these keys
 * @param {number} issueDate - generation date of these keys
 * @param {string} cumulativeSig - encoding of the actual signature over
 * these keys
 * @return GNUNET_OK if the signature is successful, GNUNET_NO
 * if not, GNUNET_SYSERR upon errors
 */
function verifyDenoms(denomsArray,
                      signkey,
		      issueDate,
		      cumulativeSig){

  var TWRverifyDenomsEnc = Module.cwrap('TALER_WR_verify_denoms_enc',
                                        'number',
		                       ['string',
			                'number',
			                'number',
			                'number',
			                'string']);

  /* array of pointers to strings */
  var strPtrs = new Uint32Array(denomsArray.length);

  /* allocating each string and saving its pointer to `strPtrs` */
  for (var i = 0; i < denomsArray.length; i++){
    
    strPtrs[i] = Module._malloc(denomsArray[i].length);
    Module.writeStringToMemory(denomsArray[i],
                               strPtrs[i],
                               false); // puts 0-terminator
  }

  /* storing into heap the strings' pointers array */
  var arraySize = strPtrs.length * strPtrs.BYTES_PER_ELEMENT;
  var arrayOffset = Module._malloc(arraySize);
  /* creating a "view" over the allocated space */
  var arrayPtr = new Uint8Array(Module.HEAPU8.buffer,
                                arrayOffset,
				arraySize);
  arrayPtr.set(new Uint8Array(strPtrs.buffer)); /* actual copy */
  var ret = TWRverifyDenomsEnc(signkey,
                               arrayPtr.byteOffset,
                               denomsArray.length,
			       issueDate,
			       cumulativeSig);  
  for (var i = 0; i < strPtrs.length; i++)
    Module._free(strPtrs[i]);
  Module._free(arrayOffset);
  return ret;
}

self.onmessage = function(msg){

  switch(msg.data.reason){
    case "verifyKeys":
    var keys = msg.data.param.keys;
    var mint = msg.data.param.mint;
    var masterkey = keys.master_public_key;
    var gotSigner = -1;

    for (var sk in keys.signkeys){
    
      if (keys.signkeys[sk].key == keys.eddsa_pub)
        gotSigner = sk;

      if (GNUNET_OK != verifySignkeys(keys.signkeys[sk], masterkey)) {
	/* TODO error mgmt */
        console.log("Invalid signature on signkey "
	           + keys.signkeys[sk].key);
      }
    } /* end of signkeys loop */

    /* check if denoms' signer key is among signkeys*/
    if (gotSigner < 0){
      console.log("signkey signing denoms not in list! Aborting");
      /* TODO error mgmt */
    }
    var denoms = []; // used later for cummulative verification
    var denomMaxLength = 0;
    for (var denom = 0; denom < keys.denoms.length; denom++) {
      denoms.push(keys.denoms[denom].denom_pub);
      if (GNUNET_OK != verifySingleDenom(keys.denoms[denom],
                                         masterkey)) {
        console.log("Invalid signature on denom " + denom);
        /* TODO error mgmt */
      }
    } /* end of denoms loop */

    /* cumulative verification */
    if (GNUNET_OK != verifyDenoms(denoms,
                                  keys.signkeys[gotSigner].key,
		                  getTimestampNumber(keys.list_issue_date),
		                  keys.eddsa_sig))
      console.log("Invalid cumulative (on denoms) signature");
    else {
      self.postMessage(GNUNET_OK);
    }

    /* Let's head towards auditor's signature verification! */
    /* TODO once one "demo" auditor will be in place */

    break;

    default:
     //
    
    
    }


  }
