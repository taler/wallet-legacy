/*

  This file is part of TALER
  Copyright (C) 2015, 2016 INRIA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>

*/

/**
 * @file Utilities aimed to create reserves
 * @author Marcello Stanisci
 */

var EXPORTED_SYMBOLS = [
  'TWReddsaVerify',
  'withdrawSortFunction',
  'fetchDenomsAmntFeeHndl',
  'trnsHistory',
  'createReserve',
  'dateAndTime',
  'withdraw',
  'wipeReserves',
  'wipeKeys',
  'wipeCoins',
  'knapsackSolver',
  'emptyReserve'
];

Components.utils.import("resource://gre/modules/Services.jsm");
Components.utils.import("chrome://taler-button/content/lib/windowMgmt.jsm");
Components.utils.import("chrome://taler-button/content/lib/chromeTransform.jsm");
Components.utils.import("chrome://taler-button/content/lib/prettyPrint.jsm");
Components.utils.import("chrome://taler-button/content/lib/io.jsm");
Components.utils.import("chrome://taler-button/content/lib/util.jsm");
Components.utils.import("chrome://taler-button/content/lib/keys.jsm");
Components.utils.import("chrome://taler-button/content/lib/emscripIface.jsm");
Components.utils.import("resource://gre/modules/FileUtils.jsm");
Components.utils.import("resource://gre/modules/ctypes.jsm");

/**
 * Global clock; useful in the following invocation: d.getTime(),
 * when the time given in seconds (since the Epoch) is needed
 */
var d = new Date();

/**
 * Debugging function used to test emscripten. It
 * just prints the current date and time on the displayed
 * HTML page
 */
function dateAndTime() {
  var timePtr = TWRALLgetCurrentTime();
  showOutput(TWRgetFancyTime(timePtr));
  TWRgnunetFree(timePtr);
}

/**
 * Skeleton for the displaying of the transactions made by the user
 */
function trnsHistory() {
  getLastWindow().alert("Trans History");
}

/**
 * Get a previously generated blindkey from the DB
 * 
 * @param mint - the mint which signed the coin blinded with the
 * blindkey which is to be returned
 * @param value - value (as in the 'value' field of an 'amount' object)
 * of the coin signed with this blindkey
 * @param fraction - fraction field of the coin signed with this blindkey
 * @param currency - coin's currency
 * @return the blindkey's base32 string
 */
function getBlindKey(mint, value, fraction, currency) {
  showOutput("getBlindKey", false);
  let depubRet = GNUNET_SYSERR;
  // open 'storage' file
  let file = FileUtils.getFile("ProfD", ["wallet.sqlite"]);
  let dbConn = Services.storage.openDatabase(file);
  let blindStmt;
  try{
  blindStmt = dbConn.createStatement("SELECT denom_table.denom_pub AS"
                                     + " depub FROM denom_table INNER JOIN"
                                     + " denom_amount_table ON"
			             + " denom_amount_table.denom"
			             + " = denom_table.denom_pub WHERE"
			             + " denom_table.mint = :mint AND"
			             + " denom_amount_table.currency = :currency"
			             + " AND denom_amount_table.value = :value"
			             + " AND denom_amount_table.fraction = :fraction");
  }
  catch(e){
    printError(e);
    return;
  }
  blindStmt.params.mint = mint;
  blindStmt.params.currency = currency;
  blindStmt.params.value = value;
  blindStmt.params.fraction = fraction;
  if (!blindStmt.executeStep())
    showOutput("no blinding key found, " + mint + amount + currency,
               true);
  else depubRet = blindStmt.row.depub;
  blindStmt.reset();
  blindStmt.finalize();
  dbConn.close();
  return depubRet;
}

/**
 * Withdraw a single coin. Mainly used as a debug function to
 * be called from a dedicated button
 *
 * @param {number} value - integer part of the coin to withdraw
 * object
 * @param {number} fraction - fractional part of the coin to withdraw
 * @param {string} cur - currency of the coin to withdraw
 * @param {boolean} signing - if true, the coin will be withdrawn,
 * otherwise only the reserve's status will be queried
 * @param {string} mint - desired mint's base URL
 */
function withdraw(value, fraction, cur, signin, mint) {

  // From which reserve (just pick the first reserve, for now)
  let dbConn = getDbHandle();
  // we withdraw from the first returned reserve
  let resStmt;
  try {
    resStmt = dbConn.createStatement("SELECT reserve_priv FROM"
                                     + " reserve_table WHERE"
		                     + "mint_base_url = :mint");
  }
  catch(e){
    printError(e); 
    return;
  }
  resStmt.params.mint = mint;
  resStmt.executeStep();
  let privEnc = resStmt.row.reserve_priv;
  if (signing)
    /* it'll free 'resPbk' and 'resPriv.dec' */
    withdrawSign(mint, privEnc, value, fraction, cur);
  else // status
    withdrawStatus(showOutput, mint, privEnc);
  resStmt.reset();
  resStmt.finalize();
  dbConn.close();
}

/**
 * Withdraw a coin
 * 
 * @param {string} mint - mint's base URL from which withdraw the coin
 * @param {string} reservePrivEnc - base32 encoding of the
 * reserve's private key from which withdraw the coin
 * @param {number} value - integer part of the coin's value
 * @param {number} fraction - fractional part of the coin's
 * value see {@link http://api.taler.net/api-mint.html#amount}
 * @param {string} currency - coin's currency
 */
function withdrawSign(mint,
                      reservePrivEnc,
		      value,
		      fraction,
		      currency) {

  let reservePriv = TWRALLeddsaPrivateKeyFromString(reservePrivEnc);
  let reservePub = TWRALLeddsaPublicKeyFromPrivString(reservePrivEnc);
  let reservePubEnc = getCrockedValue(reservePub, EDDSAKEY_SIZE);
  // 1. Get denomination key
  let denomTxt = getBlindKey(mint, value, fraction, currency);
  if (GNUNET_SYSERR == denomTxt){
    showOutput("blind key not found (wrong currency?)", true);
    return;
  }
  let denomNetEnc = ALLgetDeCrock(denomTxt);
  var denomKey = GCALLrsaPublicKeyDecode(denomNetEnc.dec,
                                         denomNetEnc.length);
  // denomkey decoded (from network format)
  // 2. generate a coin
  let coinPriv = GCALLeddsaKeyCreate();
  let coinPub = TWRALLeddsaPublicKeyFromPrivate(coinPriv);
  // 3. blind the coin
  // 3.a generate blinding factor
  let blindFac = GCALLrsaBlindingKeyCreate(1024);
  // 3.b hash coin's public key
  let buf = TWRALLhash(coinPub, EDDSAKEY_SIZE);
  // 3.c actual blinding
  let ptr = emscMalloc(PTR_SIZE);
  let coinEnvSize = GCALLrsaBlind(buf, blindFac, denomKey, ptr);
  if (!(coinEnvSize > 0))
    showOutput("/reserve/withdraw, Blinding NOT accomplished");
  // 3.d handle to the blinded coin + its base32 encoding
  let coinEnv = getLastWindow().Module.getValue(ptr, '*');
  let blindedCoinEnc = getCrockedValue(coinEnv, coinEnvSize);
  showOutput("blind to send " + blindedCoinEnc, false);
  // 4 creating the 'reserve_sig' field
  // 4.a getting all missing values to sign
  // 4.a.1 fee
  let dbHandle = getDbHandle();
  let feeStmt;
  try{
  feeStmt = dbHandle.createStatement("SELECT * FROM fee_withdraw_table"
                                     + " WHERE denom = :dnmTxt");
  }
  catch(e){
    printError(e);
    return;
  }
  feeStmt.params.dnmTxt = denomTxt;
  feeStmt.executeStep();
  let feeAmnt = feeStmt.row.value;
  let feeFrac = feeStmt.row.fraction;
  feeStmt.reset();
  feeStmt.finalize();
  /* TODO translate into a call to the function which
  instantiates the 64-bit variables */
  // 4.a.2 Sum amount + fee, as for the specs
  // 4.a.2.a translates to 64-bit the involved quantities
  let value64 = ctypes.UInt64(value);
  let fee64sum = ctypes.UInt64(feeAmnt);
  let amountPlusFee = TWRALLamountAdd(ctypes.UInt64.lo(value64),
    ctypes.UInt64.hi(value64),
    ctypes.UInt64.lo(fee64sum),
    ctypes.UInt64.hi(fee64sum),
    fraction,
    feeFrac,
    currency);
  // 4.a.3 get a 'fee amount struct'
  let fee64 = ctypes.UInt64(feeAmnt);
  let feeAmntStruct = TWRALLgetAmount(ctypes.UInt64.lo(fee64),
                                      ctypes.UInt64.hi(fee64),
                                      feeFrac, currency);
  showOutput("amount+fee summed " + amountPlusFee
             + ", feeAmntStruct = " + feeAmntStruct,
	     false);
  // 4.b creating the signature's payolad
  let wdrwBun = TWRALLmakeWithdrawBundle(reservePub,
                                         amountPlusFee,
                                         feeAmntStruct,
                                         denomKey,
                                         coinEnv,
                                         coinEnvSize);
  showOutput("bundle at : " + wdrwBun, false);
  // see if purpose is at its place.
  let gotPurpSize = DWRgetPurpose(wdrwBun);
  showOutput("with size : " + gotPurpSize, false);
  // 5 sign
  /* get a handle to the signature's purpose (any 'normal'
    pointer to the structure does the job, since the purpose
    is at the beginning of the memory occupied by the
    structure) */
  let sigPtr = TWRALLmakeEddsaSignature(reservePriv, wdrwBun);
  showOutput("signed at " + sigPtr, false);
  try {
    if (sigPtr == null)
      throw "Invalid signature on coin";
  }
  catch (err) {
    showOutput("withdraw: " + err.message, true);
    return;
  }
  let sigEnc = getCrockedValue(sigPtr, EDDSASIG_SIZE);
  showOutput("big coin's signature : " + sigEnc, false);
  // 6 pack the JSON
  let withdrJson = JSON.stringify(
    {
      'denom_pub': denomTxt,
      'coin_ev': blindedCoinEnc,
      'reserve_pub': reservePubEnc,
      'reserve_sig': sigEnc
    }
  );
  showOutput("Sending " + withdrJson, false);
  showOutput("Sending to mint " + mint, false);
  // 7 commit to disk the current operation
  /* check if 'withdraw_attempts' is defined and, if not, define it.
    'pending' = true indicates that the coins has still to be minted */

  /* TODO add columns for storing deposit permissions signatures,
    see (#3763/msg#9729)*/
  try{
  dbHandle.executeSimpleSQL("CREATE TABLE IF NOT EXISTS coins"
                            + " (mint_base_url TEXT"
			    + ", denom TEXT, coin_priv TEXT PRIMARY"
			    + " KEY, mint_sig TEXT, blind_fact TEXT"
			    + ", value NUMBER , fraction NUMBER"
			    + ", currency TEXT, pending NUMBER"
			    + ", spent NUMBER, error_proof TEXT"
			    + ", transaction_id NUMBER"
			    + ", FOREIGN KEY(denom) REFERENCES"
			    + " denom_table(denom_key))");
  }
  catch(e){
    printError(e);
    return;
  }
  let coinPrivEnc = getCrockedValue(coinPriv, EDDSAKEY_SIZE);
  let wdrwAttemptStmt;
  try{
  wdrwAttemptStmt = dbHandle.createStatement("INSERT INTO coins VALUES"
                                             + " (:mintBase, :denomPub, :coinPriv"
                                             + ", :mintDummy, :blindFact"
			                     + ", :value, :fraction, :currency"
			                     + ", :isPending, :spent, :errorProof"
			                     + ", :transactionId)");
  wdrwAttemptStmt.params.mintBase = mint;
  wdrwAttemptStmt.params.denomPub = denomTxt;
  wdrwAttemptStmt.params.mintDummy = "init";
  wdrwAttemptStmt.params.coinPriv = coinPrivEnc;
  wdrwAttemptStmt.params.blindFact = 
    getCrockedValue(blindFac, BLINDFACT1024_SIZE);
  wdrwAttemptStmt.params.value = value;
  wdrwAttemptStmt.params.fraction = fraction;
  wdrwAttemptStmt.params.currency = currency;
  wdrwAttemptStmt.params.isPending = true;
  wdrwAttemptStmt.params.spent = false;
  wdrwAttemptStmt.params.errorProof = "init";
  wdrwAttemptStmt.params.transactionId = -1;
  }
  catch(e){
    printError(e);
    return;
  }
  wdrwAttemptStmt.executeAsync();
  // 8 send!
  makeHttp(function(blindJson) {
    let blindSigEnc = ALLgetDeCrock(JSON.parse(blindJson).ev_sig);
    let blindSig =
      GCALLrsaSignatureDecode(blindSigEnc.dec,
                              blindSigEnc.length);
    // 8.a unblind
    let unblindedSig = GCALLrsaUnblind(blindSig, blindFac, denomKey);
    if (!unblindedSig)
      showOutput("/reserve/withdraw, failed to unblind, unblindedSig "
                 + unblindedSig + " blindSig " +
        blindSig + ", blindFac " + blindFac + ", denomKey "
	         + denomKey, true);
    else
      showOutput("unblinding ok", false);
    let encUnblindSig = TWRALLgetEncodingFromRsaSignature(unblindedSig);
    let ubSig = getLastWindow().Module.UTF8ToString(encUnblindSig);
    //showOutput("unblinded enc is : " + getLastWindow().Module
    //.UTF8ToString(encUnblindSig), false);
    // 8.a.1 verify
    if (GNUNET_OK == GCrsaVerify(buf, unblindedSig, denomKey))
      showOutput("coin minted!", false);
    else showOutput("invalid mint", false);
    /* 8.b change the 'pending' status to false for the minted
      coin and store the unblinded coin! */
    let coinStmt;
    try{
    coinStmt = dbHandle.createStatement("UPDATE coins SET pending = :newState"
	                                + ", mint_sig = :gotSig WHERE coin_priv"
	                                + " = :coinPriv");
    coinStmt.params.newState = false;
    coinStmt.params.gotSig = ubSig;
    coinStmt.params.coinPriv = getCrockedValue(coinPriv, EDDSAKEY_SIZE);
    }
    catch(e){
      printError(e);
      return;
    }
    coinStmt.executeAsync();
    dbHandle.asyncClose();
    emscFree(blindSigEnc.dec);
    emscFree(buf);
    TWRgnunetFree(blindSig);
    TWRgnunetFree(unblindedSig);
    TWRgnunetFree(encUnblindSig);
    GCrsaBlindingKeyFree(blindFac);
    GCrsaPublicKeyFree(denomKey);
    TWRgnunetFree(coinPriv);
  }, showOutput, "POST", mint + "reserve/withdraw",
     withdrJson);
  emscFree(sigPtr);
  emscFree(denomNetEnc.dec);
  emscFree(reservePriv);
  TWRgnunetFree(reservePub);
  TWRgnunetFree(coinPub);
  TWRgnunetFree(coinEnv);
  TWRgnunetFree(wdrwBun);
}

/**
 * Query the status of a reserve.
 *
 * @param statusFun - callback to trigger on the (successful) result
 * of /reserve/status
 * @param {string} mint - base URL of this reserve's hosting mint
 * @param {string} reservePrivEnc - base32 encoding of the reserve to query
 */
function withdrawStatus(statusFun, mint, reservePrivEnc) {
  let reservePub = TWRALLeddsaPublicKeyFromPrivString(reservePrivEnc);
  let reservePubEnc = getCrockedValue(reservePub, EDDSAKEY_SIZE);
  TWRgnunetFree(reservePub);
  showOutput("req status for " + reservePubEnc + " @ " + mint, false);
  makeHttp(statusFun,
           showOutput,
	   "GET",
           mint + "reserve/status?reserve_pub="
	   + reservePubEnc);
}

/**
 * Reserve's DB layout:
 * |priv_key | amount | deposit time (when the user clicked "ok" on
 * the wallet's dialog) | last_fetch (last status for this reserve)|
 * NOTE: last_fetch will always hold the time of the last attempt,
 * since once all the amount is loaded in the reserve, the whole amount
 * will be withdrawn and the corresponding row deleted.
 * Thus the only field asynchronously updated will be 'last_fetch'. If
 * some problem will occurr while creating the reserve, the user will
 * be informed by some dialog so he could decide what to do with the
 * 'pending' reserve.
 * Once the reserve is successfully created, the wallet will automatically
 * withdraw all the corresponding coins and destroy the reserve's row.
 */

/**
 * This "bundle" is gotten from a bank website which has tight integration
 * with Taler. Intuitivaly, this bundle indicates to the wallet where in the
 * page it can put the locally generated data, such as a new reserve key, and
 * a mint base URL. NOTE that this documentation could be somewhere obsoleted
 * by the new WebExtensions-friendly wallet
 *
 * @typedef BankPageDetails
 * @type {object}
 *
 * @property {string} input_pub - 'id' attribute of HTML 'input' element
 * belonging to the bank's 'form' which performs the SEPA transfer to the
 * mint. The wallet will use this 'id' to inject the new reserve public
 * into its 'input' field (which represents the "subject" field of the SEPA
 * operation)
 *
 * @property {string} input_amount - 'id' attribute of the HTML 'input'
 * element which contains the amount the user wants to withdraw (= transfer to)
 * from the mint. The wallet needs it to both check if the chosen mint works on
 * the currency the user is willing to withdraw this amount and to inspect
 * whether the new reserve has gotten the entire amount
 *
 * @property {string} mint_rcv - 'id' attribute of the HTML 'input' element
 * which contains the base URL of the mint the user wants to withdraw from.
 * The wallet will inject the chosen mint into that 'input' element, in order
 * to let the bank create the new reserve at the desired mint
 */

/**
 * Manager of a reserve creation. It also interacts with a bank's SEPA
 * form in order to inject into it a reserve public key. Let the user
 * choose which mint will host this new reserve, and check if the chosen
 * mint operates with the amount's currency.
 *
 * @param {BankPageDetails} reserveDetails
 */
function createReserve(reserveDetails) {
  let window = getLastWindow();
  /* see if there is already an open dialog */
  let isPanel = window.document.getElementById("pick-mint");
  if (isPanel){
    /* ignore the click and re-focus the existing window */
    showOutput("Reclick", true);
    isPanel.focus();
    return;
  }
  let pk = GCALLeddsaKeyCreate();
  let pbk = TWRALLeddsaPublicKeyFromPrivate(pk);
  let pbkEnc = getCrockedValue(pbk, EDDSAKEY_SIZE);
  /* creating this here since we loose the reference to 'pk' once in
    asynchronous mode (= from inside the 'close' event mgmt) */
  let pkEnc = getCrockedValue(pk, EDDSAKEY_SIZE);
  showOutput("reserve pub/priv : " + pbkEnc + "/" + pkEnc, false);
  if (pk && pbk) {
    /* paste key into hidden 'input' */
    let keyInput = window.gBrowser.contentDocument
        .getElementById(reserveDetails.input_pub);
    keyInput.value = pbkEnc;
    tDebug("Creating reserve of: " + reserveDetails.input_amount,
           false);
    let amountObj = parseGivenAmount(reserveDetails.input_amount);
    if (amountObj == null){
      window.alert("Please withdraw something!");
      return;
    }

    /**
     * TODO - add the chosen mint into some table IF it wasn't
     *        already among the known ones
     *      - put in mintsList only those mints which accept the
     *        withdrawal's currency
     */
    let dialogParam = {mints : ["mint.demo.taler.net"],
                       chosenMint : ""};
    /* let user choose the mint */
    window.openDialog("chrome://taler-button/content/withdraw.xul",
                      "",
                      "chrome, dialog, modal, resizable=yes",
		      dialogParam).focus();
    showOutput(JSON.stringify(dialogParam), false);
    if (dialogParam.chosenMint == "canceled")
      return;
    if (dialogParam.chosenMint == ""){
      window.alert("Please choose a mint");
      return;
    }
    attachProgressMeter();
    let schemaRe = /^http:\/\//;
    let endSlashRe = /\/$/;
    let mintUrlSchema = dialogParam.chosenMint;
    if (!schemaRe.test(dialogParam.chosenMint))
      mintUrlSchema = "http://" + dialogParam.chosenMint;
    if (!endSlashRe.test(mintUrlSchema))
      mintUrlSchema = mintUrlSchema + "/";
    /* injecting chosen mint's URL into bank's DOM */
    window.gBrowser.contentDocument
      .getElementById(reserveDetails.mint_rcv)
      .value = mintUrlSchema;
    showOutput("mint rcv " +
               window.gBrowser.contentDocument
               .getElementById(reserveDetails.mint_rcv)
               .value + " @field " + reserveDetails.mint_rcv, false);
    let cls = {limit: 10,
               //timeSlice: 10000,
               timeSlice: 5000,
               desiredAmt: amountObj,
	       mint: mintUrlSchema,
	       reservePrivEnc: pkEnc};
    let preSubmitChecks = function(x) {
      let acceptedCurrency = getMintCurrency(x.mint);
      if (acceptedCurrency != amountObj.currency){
	/* fixme: show error dialog */
	tDebug("no mint in DB", true);
        return;
      }
      putReserveInDB(mintUrlSchema,
                     pkEnc,
                     amountObj.value,
                     amountObj.fraction,
                     amountObj.currency,
                     d.getTime());
      /* submit SEPA form */
      let sepaForm = window.gBrowser.contentDocument
          .getElementById(reserveDetails.form_id);
      sepaForm.submit();
      checkReserve(x);
    };
    obtainKeys(mintUrlSchema, preSubmitChecks, cls);
  }
  else {
    /* keys creation failed */
    window.openDialog("chrome://taler-button/content/withdraw.xul",
                      "",
                      "chrome, dialog, modal, resizable=yes",
		      null).focus();
  }
  TWRgnunetFree(pk);
  TWRgnunetFree(pbk);
}

/**
 * Set of data holding a Taler-compliant `amount` object.
 * See {@link http://api.taler.net/api-mint.html#amount}
 *
 * @typedef amountAlloc
 *
 * @type {object}
 *
 * @property {number} ptr - pointer to allocated memory which contains a `struct
 * TALER_Amount` representing the amount the user expects to be at the new reserve.
 *
 * @property {number} value - integer part of the amount the user is willing to
 * withdraw
 *
 * @property {number} fraction - fractional part of the amount the user is willing
 * to withdraw
 *
 * @property {string} currency - currency of the amount the user is willing
 * to withdraw
 */

/**
 * Parse an amount gotten from a bank's SEPA form
 *
 * @param {string} amountInputId - 'id' attribute of the 'input' field belonging
 * to the bank's SEPA form from where the amount (as a string) is to be fetched
 *
 * @return {amountAlloc} desiredAmountObject
 */
function parseGivenAmount(amountInputId){
  let window = getLastWindow();
  let amountTxt = window.gBrowser.contentDocument
  .getElementById(amountInputId).value;
  if (amountTxt == "")
    return; /* dialog just closed */
  /* Since this information comes from the bank's DOM, it is expectable
    that it's already in a right form */
  let re = /([0-9]+)\.?([0-9][0-9]?)? ([A-Z]+)/;
  let amount = re.exec(amountTxt);
  if (amount == null || amount[0] != amountTxt){
    window.alert("Incorrect amount entered");
    return null;
  }
  showOutput(JSON.stringify(amount), false);
  let amount_fraction = 0;
  if (amount[2] != null) // fractional part given
    amount_fraction = Number("0." + amount[2]) * 1000000;
  if (amount[1] + amount_fraction == 0)
    return null;
  let desiredAmnt64 = ctypes.UInt64(amount[1]);
  let desiredAmnt =
  TWRALLgetAmount(ctypes.UInt64.lo(desiredAmnt64),
                  ctypes.UInt64.hi(desiredAmnt64),
                  amount_fraction,
                  amount[amount.length - 1]);
  // 'ptr' will be freed by 'checkReserve()'
  let desiredAmntObj = {'ptr' : desiredAmnt,
                        'value' : amount[1],
                        'fraction' : amount_fraction,
                        'currency' : amount[amount.length - 1]};
  return desiredAmntObj;   
}

/**
 * Update the 'last_fetch' column in the reserve's row. Called whenever the
 * desired amount is still missing on the reserve
 *
 * @param {string} reservePriv - base32 encoding of the reserve to update
 */
function updateReserve(reservePrivEnc){

  let dbConn = getDbHandle();
  let stmt;
  try{
    stmt = dbConn.createStatement("UPDATE reserve_table SET"
                                  + " last_fetch = :now WHERE"
	                          + " reserve_priv = :reservePriv");
  }
  catch(e){
    printError(e);
    return;
  }
  stmt.params.now = d.getTime();
  stmt.params.reservePriv = reservePriv;
  stmt.executeStep();
  stmt.finalize();
  dbConn.close();
}

/**
 * Store a reserve's details into DB
 *
 * @param {string} mintUrl - base URL of the reserve's hosting mint
 * @param {string} reservePrivEnc - base32 encoding of the reserve's private
 * key being put into DB
 * @param {number} reserveValue - integer part of this reserve's associated
 * amount
 * @param {number} reserveFraction - fraction part of this reserve's associated
 * amount
 * @param {string} reserveCurrency - currency of this reserve's associated amount
 * @param {number} lastFetch - date (given in seconds since the Epoch) when this
 * reserve has been polled last time
 */
function putReserveInDB(mintUrl,
                        reservePrivEnc,
			reserveValue,
			reserveFraction,
                        reserveCurrency,
			lastFetch) {
    // DB mgmt, TABLE: eddsa priv. key | mint url | last_fetch
    // open 'storage' file
    let file = FileUtils.getFile("ProfD", ["wallet.sqlite"]);
    let dbConn = Services.storage.openDatabase(file);
    // check if 'reserve_table' is defined and, if not, define it.
    dbConn.executeSimpleSQL("CREATE TABLE IF NOT EXISTS reserve_table" 
                            + " (reserve_priv TEXT" 
			    + ", reserve_value INTEGER" 
			    + ", reserve_fraction INTEGER" 
			    + ", reserve_currency TEXT" 
			    + ", mint_base_url TEXT" 
			    + ", last_fetch INTEGER)");
    // feed data to table
    let stmt =
      dbConn.createStatement("INSERT INTO reserve_table VALUES"
                             + " (:keyPriv, :reserveValue, :reserveFraction"
                             + ", :reserveCurrency, :mintBase, :fetchWhen)");
    stmt.params.keyPriv = reservePrivEnc;
    stmt.params.reserveValue = reserveValue;
    stmt.params.reserveFraction = reserveFraction;
    stmt.params.reserveCurrency = reserveCurrency;
    stmt.params.mintBase = mintUrl;
    stmt.params.fetchWhen = lastFetch;
    stmt.executeAsync(
      {
        handleResult: function(resultSet) {
          // no results to handle, we just insert values.
          // Get rid of this object?
        },
        handleError: function() {
          showOutput("DB error");
        },
        handleCompletion: function() {
          // called when the query "returns"
        }
      }
    );
    dbConn.asyncClose();
}

/**
 * Poll a reserve until the desired amount is transferred (or a persistent
 * state is reached) and withdraw any coins from it
 *
 * @param {amountAlloc} bundle - the amount the user expects to be transferred
 * to the new reserve
 */
function checkReserve(bundle){
  let resPub = TWRALLeddsaPublicKeyFromPrivString(bundle.reservePrivEnc);
  let resPubEnc = getCrockedValue(resPub, EDDSAKEY_SIZE);
  TWRgnunetFree(resPub);
  showOutput("checkReserve(): checking reserve pub/priv : "
             + resPubEnc
	     + "/"
	     + bundle.reservePrivEnc,
	     false);
  showOutput("checking loaded amount",
             false);
  let count = 0;
  // this function runs 'isThere' periodically
  let period = function() {
    /**
     * the run started just because it was already in the "pipeline",
     * but it's useless, so return. Nonetheless, if the timeslice is
     * too short, run i+1 may find 'ptr' still non-null even though
     * run i actually set it to null. So further reference on 'ptr'
     * may generate warnings
     */
    if (bundle.desiredAmt.ptr == null)
      return;
    // this function elaborates the response to a /reserve/status
    let isThere = function(jsonStatus) {
      let balance = JSON.parse(jsonStatus).balance;
      // define proper amount structure
      let availCur = balance.currency;
      let availVal = ctypes.UInt64(balance.value);
      let availFrac = balance.fraction;
      let avail = TWRALLgetAmount(ctypes.UInt64.lo(availVal),
                                  ctypes.UInt64.hi(availVal),
				  availFrac,
				  availCur);
      showOutput("In reserve : "
                 + balance.value
		 + "."
		 + balance.fraction
                 + " "
		 + balance.currency,
		 false);
      showOutput("desired amount "
                 + JSON.stringify(bundle.desiredAmt),
		 false);
      showOutput("Cmp is : "
                 + TamountCmp(avail, bundle.desiredAmt.ptr),
		 false);
      switch (TamountCmp(avail, bundle.desiredAmt.ptr)) {
        case 1: // we got more. What to do (?)
          break;
        case 0: /* we got the exact amount, it's now possible to delete
	         the reserve's row in DB, and withdraw everything */
          getLastWindow().clearInterval(interval);
	  tDebug("freeing reserve amt", true);
	  removeProgressMeter();
          TWRgnunetFree(bundle.desiredAmt.ptr);
          bundle.desiredAmt.ptr = null;
	  /* extra check needed since the callback runs one time
	    more after disabling it in case the limit is reached,
	    so deallocating the desired amount when disabling the
	    callback makes the comparison between the desired amount
	    and the money in reserve be true and the withdrawal be
	    attempted (therefore failing with "Insufficient funds") */
	  if (count <= bundle.limit){
            tDebug("all money in reserve; withdrawing", true);
	    emptyReserve(bundle.mint, bundle.reservePrivEnc,
	                 bundle.desiredAmt.value,
	                 bundle.desiredAmt.fraction,
		         bundle.desiredAmt.currency);
	  }
          break;
        case -1: // still wait, update 'last_fetch' column
          showOutput("not all money in reserve", true);
	  removeProgressMeter();
	  updateReserve(bundle.reservePrivEnc);
      }
      TWRgnunetFree(avail);
    };
    count++;
    showOutput("count down is : " + count, false);
    if (count > bundle.limit) {
      tDebug("freeing after limit reached", true);
      TWRgnunetFree(bundle.desiredAmt.ptr);
      bundle.desiredAmt.ptr = null; //extra care
      getLastWindow().clearInterval(interval);
      showOutput("limit reached count/limit : "
                 + (count - 1) + "/" + bundle.limit);
    }
    showOutput("checking priv " + bundle.reservePrivEnc, false);
    withdrawStatus(isThere, bundle.mint, bundle.reservePrivEnc);
  };
  let interval = getLastWindow().setInterval(period, bundle.timeSlice);
}

/**
 * Removes any reserve entry (by deleting the table) from local DB.
 * Used for debugging
 */
function wipeReserves(){
  let dbHandle = getDbHandle();
  let wipe =
    dbHandle.createStatement("DROP TABLE IF EXISTS reserve_table");
  wipe.executeAsync();
  dbHandle.asyncClose();
}

/**
 * Removes any coin entry (by deleting the table) from local DB.
 * Used for debugging
 */
function wipeCoins(){
  let dbHandle = getDbHandle();
  let wipe = dbHandle.createStatement("DROP TABLE IF EXISTS coins");
  wipe.executeAsync();
  dbHandle.asyncClose();
}

/**
 * Delete keys DB tables
 */
function wipeKeys(){

  let dbHandle = getDbHandle();

  let wipeMaster =
    dbHandle.createStatement("DROP TABLE IF EXISTS mint_table");
  let wipeDenom =
    dbHandle.createStatement("DROP TABLE IF EXISTS denom_table");
  let wipeDenomAm =
    dbHandle.createStatement("DROP TABLE IF EXISTS denom_amount_table");
  let wipeFeeW =
    dbHandle.createStatement("DROP TABLE IF EXISTS fee_withdraw_table");
  let wipeFeeD =
    dbHandle.createStatement("DROP TABLE IF EXISTS fee_deposit_table");
  let wipeFeeR =
    dbHandle.createStatement("DROP TABLE IF EXISTS fee_refresh_table");
  let wipeSignK =
    dbHandle.createStatement("DROP TABLE IF EXISTS signkeys_table");
  wipeMaster.executeAsync();
  wipeDenom.executeAsync();
  wipeDenomAm.executeAsync();
  wipeFeeW.executeAsync();
  wipeFeeD.executeAsync();
  wipeFeeR.executeAsync();
  wipeSignK.executeAsync();
  dbHandle.asyncClose();
}

/**
 * Withdraw all the coins from a reserve
 *
 * @param {string} mint - base URL of the reserve's hosting mint
 * @param {string} reservePrivEnc - base32 of the reserve's private key
 * @param {number} maxValue - integer part of the amount to withdraw
 * @param {number} maxFraction - fractional part of the amount to withdraw
 * @param {string} maxCurrency - currency of the amount to withdraw
 */
function emptyReserve(mint,
                      reservePrivEnc,
                      maxValue,
                      maxFraction,
		      maxCurrency){
  let dbConn = getDbHandle();
  let sameMintStmt;
  try{
    sameMintStmt = dbConn.createStatement("SELECT denom_pub FROM denom_table"
                                          + " WHERE mint = :thisMint");
  sameMintStmt.params.thisMint = mint;
  }
  catch(e){
    printError(e);
    return;
  }
  let sameMintHndl = function (resultSet) {
    /* Return an array of denoms encodings, each belonging to
      the same mint */
    for (let row = resultSet.getNextRow();
	 row;
         row = resultSet.getNextRow()) {
      let denom = row.getResultByName("denom_pub");
      showOutput("got dnm " + denom, false);
      this.denoms.push({denom: denom});
    }
  }; 
  let handlerObj =
    {'denoms' : [],
     'mint' : mint,
     'reserve' : reservePrivEnc,
     'knapsackLimit' : {'value' : maxValue,
	                'fraction' : maxFraction,
		        'currency' : maxCurrency},
     'handleResult' : sameMintHndl,
     'handleError' :
       function (){
         showOutput("error in getting denoms for this mint",
                    true);
       },
     'handleCompletion' :
       function (){
         showOutput(JSON.stringify(this.denoms), false)
         let knapsackInput =
           fetchDenomsAmntFeeHndl(this.denoms);
	 knapsackInput.sort(withdrawSortFunction);
	 tDebug("Sorted: " + JSON.stringify(knapsackInput), false);
	 let knapsackOutput = knapsackSolver(knapsackInput,
	                                     this.knapsackLimit,
	                                     false);
	 showOutput(JSON.stringify(knapsackOutput),
	            false);
	 massiveWithdraw(this.mint, this.reserve, knapsackOutput);
       }
    }
  sameMintStmt.executeAsync(handlerObj);
  dbConn.asyncClose();
}

/**
 * Taler-compliant amount. See {@link http://api.taler.net/api-mint.html#amount}
 * 
 * @typedef talerAmount
 *
 * @type {object}
 *
 * @property {number} value - integer part of an amount
 * @property {number} fraction - fractional part of an amount
 * @property {string} currency - currency of an amount
 */

/**
 * Extracts 'amount' from a returned DB query object
 *
 * @param {object} - superset object of some Taler-compliant amount
 *
 * @return {talerAmount}
 */
function rowToAmount(row){
  
  let amountObj = {'value' : row.value,
                   'fraction' : row.fraction,
                   'currency' : row.currency
                  };
  return amountObj;
};

/**
 * Container for metadata associated with a single denomination keys. It uses
 * terminology borrowed from the knapsack problem's
 *
 * @typedef kpCompliantDenom
 *
 * @type {object}
 *
 * @property {object} value - the "value" of this object. Typically, that is
 * what any KP solver would maximize, although frequently that is what the
 * wallet sholud *minimize*
 *
 * @property {talerAmount} value.withdrawFee - withdraw fee associated with
 * this denomination key
 *
 * @property {talerAmount} value.depositFee - deposit fee associated with
 * this denomination key
 *
 * @property {talerAmount} value.refreshFee - refresh fee associated with
 * this denomination key
 *
 * @property {number} value.expirationDate - date when this denomination key will
 * loose its validity
 *
 * @property {string} denom - denomination key's base32 encoding (still in
 * network format)
 *
 * @property {talerAmount} volume - the amount corresponding to the coins supposed
 * to be signed with this denomination key. Note that in KP terminology, this value
 * is what consumes knapsack's free space, so the name "volume".
 *
 * @property {?number} no - how many coins of this denomination there are currently
 * in the wallet. This field is optional and only present when the KP solver deals
 * with payments
 *
 * @property {?talerAmount} price - the price that this coin can be used to pay
 */

/**
 * Get various metadata associated with a set of denomination keys. That
 * metadata will typically be used to rank the coins in order to solve the
 * knapsack problem
 *
 * @param {string[]} denomsArray - base32 encoding of denomination keys
 * this function will return data about
 * 
 * @return {kpCompliantDenom[]}
 */
function fetchDenomsAmntFeeHndl(denomsArray) {

  let dbhandle = getDbHandle();
  tDebug(JSON.stringify(denomsArray), false);
  let denomAmntStmt;
  let denomWitFeeStmt;
  let denomDepFeeStmt;
  let denomRefFeeStmt;
  let denomMetaStmt;

  try{
    denomMetaStmt = 
      dbhandle.createStatement("SELECT stamp_expire_deposit FROM"
                             + " denom_table WHERE"
			     + " denom_pub = :thisDenom");

    denomAmntStmt =
      dbhandle.createStatement("SELECT value, fraction, currency"
                             + " FROM denom_amount_table WHERE"
			     + " denom = :thisDenom");
    denomWitFeeStmt =
      dbhandle.createStatement("SELECT value, fraction, currency FROM"
                             + " fee_withdraw_table WHERE denom"
                             + " = :thisDenom");

    denomDepFeeStmt =
      dbhandle.createStatement("SELECT value, fraction, currency FROM"
                             + " fee_deposit_table WHERE denom"
                             + " = :thisDenom");


    denomRefFeeStmt =
      dbhandle.createStatement("SELECT value, fraction, currency FROM"
                             + " fee_refresh_table WHERE denom"
                             + " = :thisDenom");
  }
  catch(e){
    printError(e);
    return; 
  }

  let denomsAndFees = [];
  for (let i = 0; i < denomsArray.length; i++) {
      
    showOutput("key no : " + i, false);
    denomsAndFees[i] = {value : {}};
    tDebug(denomsArray[i], true);
    denomMetaStmt.params.thisDenom = denomsArray[i].denom;
    denomAmntStmt.params.thisDenom = denomsArray[i].denom;
    denomWitFeeStmt.params.thisDenom = denomsArray[i].denom;
    denomDepFeeStmt.params.thisDenom = denomsArray[i].denom;
    denomRefFeeStmt.params.thisDenom = denomsArray[i].denom;

    /* Executed synchronously since this function is already running
      inside an asynchronous context; that is the handler of a reserve
      creation */

    /* only one result: the denom is a primary key */
    if (!denomMetaStmt.executeStep())
      showOutput("incomplete denom (no expire deposit date) in DB", true);
    else
      tDebug("denomMeta " + denomMetaStmt.row.stamp_expire_deposit, true);
    if (!denomAmntStmt.executeStep())
      showOutput("incomplete denom (no amount) in DB", true);
    if (!denomWitFeeStmt.executeStep())
      showOutput("incomplete denom (no withdraw fee) in DB", true);
    if (!denomDepFeeStmt.executeStep())
      showOutput("incomplete denom (no deposit fee) in DB", true);
    if (!denomRefFeeStmt.executeStep())
      showOutput("incomplete denom (no refresh fee) in DB", true);

    showOutput("denom[" + i + "]  = "
               + denomAmntStmt.row.currency
               + " " + denomAmntStmt.row.value
               + "."
	       + denomAmntStmt.row.fraction,
	       false);

    showOutput("fee[" + i + "] = "
               + denomWitFeeStmt.row.currency
               + " "
	       + denomWitFeeStmt.row.value
	       + "."
	       + denomWitFeeStmt.row.fraction,
	      false);
    /* 'value' as meant in the knapsack problem, not as meant
      in Taler terminology. In some cases, like when withdrawing
      a reserve, it might get minimized instead of maximized */
    denomsAndFees[i].value.withdrawFee = rowToAmount(denomWitFeeStmt.row);
    denomsAndFees[i].value.depositFee = rowToAmount(denomDepFeeStmt.row);
    denomsAndFees[i].value.refreshFee = rowToAmount(denomRefFeeStmt.row);
    denomsAndFees[i].value.expirationDate = denomMetaStmt.row.stamp_expire_deposit;
    denomsAndFees[i].denom = denomsArray[i].denom;
    denomsAndFees[i].volume = rowToAmount(denomAmntStmt.row);

    denomMetaStmt.reset();
    denomWitFeeStmt.reset();
    denomDepFeeStmt.reset();
    denomRefFeeStmt.reset();
    denomAmntStmt.reset();

  }
  showOutput(JSON.stringify(denomsAndFees),
             false);
  denomAmntStmt.finalize();
  denomWitFeeStmt.finalize();
  denomDepFeeStmt.finalize();
  denomRefFeeStmt.finalize();
  dbhandle.asyncClose();
  return denomsAndFees;
}

/**
 * KP's solver output. In particular, this object associates a coin with
 * the quantity of this coin to withdraw/spend
 * 
 * @typedef knOutput
 *
 * @type {object}
 *
 * @property {talerAmount} denom - the amount corresponding to this coin
 *
 * @property {number} no - how many coins need to be spent/withdrawn for a
 * certain instance of the KP
 *
 * @property {?string} denom_pub - used in case of payment, it holds the
 * denomination key's base32 encoding (still in network format)
 */

/**
 * "Fill" the knapsack. In case of withdrawing the KP is Unbounded (any
 * number of coin per denomination can be withdrawn), while in case of
 * paying it is Bounded (the wallet has a fixed number of coins per each
 * denomination). According to the given parameter, this function is usable
 * in both the UKP and the BKP case
 *
 * @param {kpCompliantDenom[]} input - what can be put in the knapsack
 * @param {talerAmount} limit - the capacity of the knapsack
 * @param {?boolean} pay - if true, the BKP is addressed
 * @param {?talerAmount} maxFee - in case of payment, it indicates the
 * maximum deposit fee the merchant wants to pay
 * @return {knOutput[]}
 */
function knapsackSolver(input, limit, pay, maxFee){
  let ret = [];
  let allocLimit = TWRALLgetAmount32(limit.value,
                                     limit.fraction,
		                     limit.currency);
  let maxFeePtr = null;
  let accFee = null;

  if(pay){
    showOutput("KP, limit: " + JSON.stringify(limit), false);
    showOutput("KP, input: " + JSON.stringify(input), false);
  }

  for (let i = 0; i < input.length; i++) {
    showOutput("Processing : volume = " + input[i].volume.value
               + "." + input[i].volume.fraction
               + " " + input[i].volume.currency + "; "
               + "value  = (W) " + input[i].value.withdrawFee.value
               + "." + input[i].value.withdrawFee.fraction
	       + " " + input[i].value.withdrawFee.currency
               + " (D) " + input[i].value.depositFee.value
               + "." + input[i].value.depositFee.fraction
	       + " " + input[i].value.depositFee.currency
               + " (R) " + input[i].value.refreshFee.value
               + "." + input[i].value.refreshFee.fraction
	       + " " + input[i].value.refreshFee.currency
	       + " (Expir) " + input[i].value.expirationDate
	       + " #" + input[i].no,
	       false);
  }
  let acc = TWRALLgetAmount32(0,
                              0,
			      limit.currency);
  if (pay){
    accFee = TWRALLgetAmount32(0,
                               0,
                               maxFee.currency);
    maxFeePtr = TWRALLgetAmount32(maxFee.value,
                                  maxFee.fraction,
                                  maxFee.currency);
  }
  for (let i = 0; i < input.length; i++){
    let coinPtr = TWRALLgetAmount32(input[i].volume.value,
                                    input[i].volume.fraction,
                                    input[i].volume.currency);
    let feePtr = null;
    if (pay)
      feePtr = TWRALLgetAmount32(input[i].value.depositFee.value,
                                 input[i].value.depositFee.fraction,
                                 input[i].value.depositFee.currency);
    else
      feePtr = TWRALLgetAmount32(input[i].value.withdrawFee.value,
                                 input[i].value.withdrawFee.fraction,
                                 input[i].value.withdrawFee.currency);
    let no = 0; 
     showOutput("limit = "
                + TWRgetValue(allocLimit)
		+ "."
		+ TWRgetFraction(allocLimit)
		+ " on coin "
		+ i,
                false);
    /* check weird cases when 'allocLimit' is given negative? */
    while (TamountCmp(acc, allocLimit) < 0){
     showOutput("acc = "
                + TWRgetValue(acc)
		+ "."
		+ TWRgetFraction(acc)
		+ " on coin "
		+ i,
                false);
     /* in case of paying, the problem is BKP, so it's
       necessary to account for the picked coins */
     if (pay){
       if(input[i].no <= 0)
         break;
       else
         input[i].no--;
     }
     no++;
     TamountAdd(acc, acc, coinPtr);
     /* in case of withdrawal, the fee weights on the reserve's
       amount */
     if (pay){
       TamountAdd(accFee, accFee, feePtr);
     }
     else
       TamountAdd(acc, acc, feePtr);
    } /* end of single coin loop */
    /* remove last coin in case of withdrawing AND limit has
      been passed */
    if (!pay && no != 0 && (TamountCmp(acc, allocLimit) > 0)){
      showOutput("withdrawing, exceeded by one",
                 false);
      no--;
      TamountSubtract(acc, acc, coinPtr);
      TamountSubtract(acc, acc, feePtr);
    }
    TWRgnunetFree(coinPtr);
    TWRgnunetFree(feePtr);
    if (pay){
      if(no != 0)
        ret.push({'denom': input[i].volume,
	          'denom_pub': input[i].denom,
		  'no': no});
      if (TamountCmp(accFee, maxFeePtr) > 0){
        TWRgnunetFree(acc);
        TWRgnunetFree(allocLimit);
        TWRgnunetFree(maxFeePtr);
        TWRgnunetFree(accFee);
	return GNUNET_SYSERR;
      }
      /* In case of paying, the algorithm does want to exceede the 'limit'
        (AKA the price to pay); that happens when the wallet can't match
        exactly the amount to pay */
      if (TamountCmp(acc, allocLimit) >= 0){
        TWRgnunetFree(acc);
        TWRgnunetFree(allocLimit);
        TWRgnunetFree(maxFeePtr);
        TWRgnunetFree(accFee);
	return ret;
      }
    }
    else
      /* withdrawing case: exceeded by one */
      if (no != 0){
        ret.push({'fees': input[i].value, //XXX to test
	          'denom': input[i].volume,
	          'no': no});
        showOutput("adding "
	           + no
		   + " of coin "
		   + i,
		   false);
	}
  }
  if (pay){
    TWRgnunetFree(maxFeePtr);
    TWRgnunetFree(accFee);
    if (TamountCmp(acc, allocLimit) < 0){
      showOutput("Not enough money",
                 true);
      ret = GNUNET_NO;
    }
  }
  TWRgnunetFree(acc);
  TWRgnunetFree(allocLimit);
  return ret;
}

/**
 * Implements the policy to sort coins in the context of withdrawal.
 * This function, which is typically invoked by 'Array.sort(..)' is
 * mainly used to prepare the input coins for the KP solver, so that
 * the "best" coins are placed in the first positions of the `input`
 * parameter passed to the KP solver
 *
 * @param {kpCompliantDenom} oOne
 * @param {kpCompliantDenom} oTwo
 * @return {number} - some x < 1 if oOne is better than oTwo, some
 * x >= 0 otherwise
 */
function withdrawSortFunction(oOne, oTwo){
  let valueOne = new Array(3);
  let valueTwo = new Array(3);
  valueOne["fees"] = 0;
  valueOne["amount"] = oOne.volume;
  valueOne["eDate"] = getTimestampNumber(oOne.value.expirationDate);
  valueTwo["fees"] = 0;
  valueTwo["amount"] = oTwo.volume;
  valueTwo["eDate"] = getTimestampNumber(oTwo.value.expirationDate);
  let oneWFeePtr = TWRALLgetAmount32(oOne.value.withdrawFee.value,
                                     oOne.value.withdrawFee.fraction,
				     oOne.value.withdrawFee.currency);
  let twoWFeePtr = TWRALLgetAmount32(oTwo.value.withdrawFee.value,
                                     oTwo.value.withdrawFee.fraction,
				     oTwo.value.withdrawFee.currency);
  let oneDFeePtr = TWRALLgetAmount32(oOne.value.depositFee.value,
                                     oOne.value.depositFee.fraction, 
                                     oOne.value.depositFee.currency);
  let twoDFeePtr = TWRALLgetAmount32(oTwo.value.depositFee.value,
                                     oTwo.value.depositFee.fraction, 
                                     oTwo.value.depositFee.currency);
  let oneRFeePtr = TWRALLgetAmount32(oOne.value.refreshFee.value,
                                     oOne.value.refreshFee.fraction,
				     oOne.value.refreshFee.currency);
  let twoRFeePtr = TWRALLgetAmount32(oTwo.value.refreshFee.value,
                                     oTwo.value.refreshFee.fraction,
				     oTwo.value.refreshFee.currency);
  let oneAmntPtr = TWRALLgetAmount32(oOne.volume.value,
                                     oOne.volume.fraction,
				     oOne.volume.currency);
  let twoAmntPtr = TWRALLgetAmount32(oTwo.volume.value,
                                     oTwo.volume.fraction,
				     oTwo.volume.currency);
  /* New interface, to be tested
  let oneWFeePtr = TWRALLgetAmount(oOne.value.withdrawFee);
  let twoWFeePtr = TWRALLgetAmount(oTwo.value.withdrawFee);
  let oneDFeePtr = TWRALLgetAmount(oOne.value.depositFee);
  let twoDFeePtr = TWRALLgetAmount(oTwo.value.depositFee);
  let oneRFeePtr = TWRALLgetAmount(oOne.value.refreshFee);
  let twoRFeePtr = TWRALLgetAmount(oTwo.value.refreshFee);
  let oneAmntPtr = TWRALLgetAmount(oOne.volume);
  let twoAmntPtr = TWRALLgetAmount(oTwo.volume);
  */
  if (GNUNET_SYSERR == TWRmultiplyAmount(oneWFeePtr, 2))
    showOutput("Integer overflow (oneWFeePtr)", true); 
  if (GNUNET_SYSERR == TWRmultiplyAmount(twoWFeePtr, 2))
    showOutput("Integer overflow (twoWFeePtr)", true); 
  if (GNUNET_SYSERR == TWRmultiplyAmount(oneDFeePtr, 2))
    showOutput("Integer overflow (oneDFeePtr)", true); 
  if (GNUNET_SYSERR == TWRmultiplyAmount(twoDFeePtr, 2))
    showOutput("Integer overflow (twoDFeePtr)", true); 
  let oneFeesPtr = oneWFeePtr;
  let twoFeesPtr = twoWFeePtr;
  TamountAdd(oneFeesPtr, oneWFeePtr, oneDFeePtr);
  TamountAdd(oneFeesPtr, oneFeesPtr, oneRFeePtr);
  TamountAdd(twoFeesPtr, twoWFeePtr, twoDFeePtr);
  TamountAdd(twoFeesPtr, twoFeesPtr, twoRFeePtr);
  let facOne = TWRmultiplyAmounts(oneFeesPtr, twoAmntPtr);
  let facTwo = TWRmultiplyAmounts(twoFeesPtr, oneAmntPtr);
  TWRgnunetFree(oneWFeePtr);
  TWRgnunetFree(twoWFeePtr);
  TWRgnunetFree(oneDFeePtr);
  TWRgnunetFree(twoDFeePtr);
  TWRgnunetFree(oneRFeePtr);
  TWRgnunetFree(twoRFeePtr);
  showOutput("F one : " + facOne, false);
  showOutput("F two : " +  facTwo, false);
  if (GNUNET_SYSERR != facOne)
    valueOne["fees"] = facOne; 
  if (GNUNET_SYSERR != facTwo)
    valueTwo["fees"] = facTwo; 
  showOutput("One " + valueOne['fees']
             + ", " + dumpAmount(valueOne['amount'])
	     + ", " + valueOne['eDate'],
	     false);
  showOutput("Two " + valueTwo['fees']
             + ", " + dumpAmount(valueTwo['amount'])
	     + ", " + valueTwo['eDate'],
	     false);
  /* actual comparison starts here! */
  let cmp = valueOne['fees'] - valueTwo['fees'];
  if (cmp != 0){
    TWRgnunetFree(oneAmntPtr);
    TWRgnunetFree(twoAmntPtr);
    return cmp;
  }
  cmp = TamountCmp(oneAmntPtr, twoAmntPtr);
  TWRgnunetFree(oneAmntPtr);
  TWRgnunetFree(twoAmntPtr);
  if (cmp != 0)
    return (-1) * cmp; // choose the bigger, save computation
  /* Prefer coins which expire later */
  return (-1) * (valueOne['eDate'] - valueTwo['eDate']);
}

/**
 * Perform multiple withdraws
 *
 * @param {string} mint - base URL of the mint to withdraw from
 * @param {string} reservePrivEnc - base32 encoding of the private key
 * of the reserve from which withdraw such coins
 * @param {knOutput[]} sortedCoins - how many coins per denomination needs
 * to be withdrawn
 */
function massiveWithdraw(mint, reservePrivEnc, sortedCoins){

  showOutput("massive withdraw : "
             + JSON.stringify(sortedCoins),
	     false);

  let reservePriv = TWRALLeddsaPrivateKeyFromString(reservePrivEnc);
  let reservePub = TWRALLeddsaPublicKeyFromPrivString(reservePrivEnc);
  let reservePubEnc = getCrockedValue(reservePub, EDDSAKEY_SIZE);


  for (let i = 0; i < sortedCoins.length; i++){
    for (let j = 0; j < sortedCoins[i].no; j++){

      showOutput("Wdr " + sortedCoins[i].denom.value
	         + "." + sortedCoins[i].denom.fraction,
                 false); 

      withdrawSign(mint,
                   reservePrivEnc,
                   sortedCoins[i].denom.value,
	           sortedCoins[i].denom.fraction,
                   sortedCoins[i].denom.currency);
    }
    
  }
  /* TODO each withdrawn coin should trigger the subtraction of its value
    to the corresponding reserve and, when the reserve reaches zero, some
    function should delete that reserve from the DB. OBSOLETED by the new
    wallet */
}
