/*
  This file is part of TALER
  Copyright (C) 2015, 2016 INRIA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>

  @author Marcello Stanisci
*/

/*
  Some fragments of the following code are taken from:

  https://developer.mozilla.org/en-US/Add-ons/How_to_convert_an_overlay_extension_to_restartless
  https://developer.mozilla.org/en-US/docs/Mozilla/Tech/XPCOM/Reference/Interface/nsIObserver

*/

var EXPORTED_SYMBOLS = ['Taler'];

Components.utils.import("resource://gre/modules/Services.jsm");
Components.utils.import("chrome://taler-button/content/lib/windowMgmt.jsm");
Components.utils.import("chrome://taler-button/content/lib/prettyPrint.jsm");



function windowDependantLoad(win){

  Components.utils.import("chrome://taler-button/content/lib/chromeTransform.jsm");
  Components.utils.import("chrome://taler-button/content/lib/pay.jsm");
  Components.utils.import("chrome://taler-button/content/lib/reserves.jsm");
  /* load our stylesheet */
  manageTalerStyles("load");
  /* syntactical definition, no DOM creation */
  let button = defineTalerButton();
  applyButton(win, button);

  /*
  win.gBrowser.tabContainer.addEventListener("TabOpen",
                                             Taler.Wallet.onTabShow,
                                             true);*/

  // TODO get tabbrowser from some "central" place
  win.gBrowser.tabContainer.addEventListener("TabSelect",
                                             Taler.Wallet.onTabSelect,
                                             true);
  win.gBrowser.addEventListener("load",
                                Taler.Wallet.onPageLoad,
                                true);
  win.gBrowser.addEventListener("beforeunload",
                                Taler.Wallet.onPageUnLoad,
                                true);
};

var Taler = {

    Wallet: {

      initWallet: function() {

        Taler.Wallet.stringBundle
	= Services.strings.createBundle("chrome://taler-button/locale/taler.properties?"
	                                + Math.random());
        /* loading Taler in each opened window */
       forEachOpenWindow(windowDependantLoad);
       /* attaching the main event listener to 'window mediator' (i.e.
         loading Taler for each future window) */
       Services.wm.addListener(Taler.Wallet.chromeWinListener);
      },

      notifyAbsence: function() {
        // notify the web page of our presence

        tDebug("notifying absence", false);

        forEachOpenWindow(

          function(xulWin, button) {

            for (var i = 0; i < xulWin.gBrowser.browsers.length; i++) {

              if (typeof xulWin.gBrowser.browsers[i].contentDocument.body !== "undefined") {
                var absence = xulWin.gBrowser.browsers[i].contentWindow.Event("taler-unload");
                xulWin.gBrowser.browsers[i].contentDocument.body.dispatchEvent(absence);
	      }
	    }
          }
        );
      },

      notifyPresence: function() {
        // notify the web page of our presence

        tDebug("notifying presence", false);

        forEachOpenWindow(

          function(xulWin, button) {

            for (var i = 0; i < xulWin.gBrowser.browsers.length; i++) {

              if (typeof xulWin.gBrowser.browsers[i].contentDocument.body !== "undefined") {

                tDebug("notification done", false);

                Taler.Wallet.registerCallbacks(xulWin.gBrowser.browsers[i].contentDocument); 
                var presence =
		  xulWin.gBrowser.browsers[i].contentWindow.Event("taler-wallet-installed");
                var enabled = xulWin.gBrowser.browsers[i].contentWindow.Event("taler-load");
                xulWin.gBrowser.browsers[i].contentDocument.dispatchEvent(presence);
                xulWin.gBrowser.browsers[i].contentDocument.dispatchEvent(enabled);

              }
            }
	  }
        );
      },

          haltWallet: function() {

            manageTalerStyles("unload");


            // get a (chrome) 'document' reference for each opened window, then
            // delete our extension (i.e. 'removeChild(..)') for each of them
            forEachOpenWindow(

              function(xulWin) {

                var xulDoc = xulWin.document;

                if (xulDoc.documentElement.getAttribute("windowtype") == "navigator:browser") {

                  var toRemove = xulWin.document.getElementById("taler-button");
                  toRemove.parentNode.removeChild(toRemove);
                }
              }

            );

            Services.wm.removeListener(Taler.Wallet.chromeWinListener);
          },

          /* Handelers triggered whenever the 'window mediator' object detects a new
	   opened window (it should not be called in case of a new tab). Note that a
	   further listener on the catched opened window is needed in order to wait for
           its DOM to be fully loaded, and then doing our activity (like making the
	   button appear). */

          chromeWinListener: {

            /* gets triggered by window mediator */
            onOpenWindow: function(xulWindow) {

              // getting the 'window' object for this event
              var window = xulWindow.QueryInterface(Components.interfaces.nsIInterfaceRequestor)
	                   .getInterface(Components.interfaces.nsIDOMWindow);
              // defining the second handler, triggered on fully loaded new window
              function onWindowLoad() {

                window.removeEventListener("load", onWindowLoad);
                if (window.document.documentElement.getAttribute("windowtype")
		    == "navigator:browser") {

                  windowDependantLoad(window);
                }
              };

              window.addEventListener("load", onWindowLoad);
            },


            onCloseWindow: function(xulWindow) {},
            onWindowTitleChange: function(xulWindow, newTitle) {}

          },


          /* It counts how many taler-style page are currently opened (mainly
	    needed for turning on/off the taler color on the button) */
          active: false,


          /* Triggered whenever some page is unloaded. This function
            will: send a message/event to the current webpage to check whether it
	    is a taler friendly page; if the page answer positively, then we update
	    our counter by subtracting one. Finally, if the counter reaches zero,
	    we uncolor the button. */
          onPageUnLoad: function(aEvent) {

            let doc = aEvent.originalTarget;
            if (doc instanceof doc.defaultView.HTMLDocument) {

              if (doc && doc.addEventListener) {

		for (let i = 0; i < Taler.Wallet.Util.talerSites.length; i++){
		  if (doc.defaultView.location == Taler.Wallet.Util.talerSites[i]){
		    tDebug("Taler-friendly navigating away: "
		           + doc.defaultView.location, false);
                    Taler.Wallet.Util.talerSites.splice(i, 1);
		      changeButtonColor.call(getLastWindow(),
		                             false);	  
		  }
                }
              }
            }
	  },

          onTabSelect: function(aEvent) {

	    let currentWindow = getLastWindow();
            let tab = aEvent.originalTarget;
	    let tabUrl = currentWindow.gBrowser.getBrowserForTab(tab)
	    .contentDocument.documentURI;
            let found = false;

	    for (let i = 0; i < Taler.Wallet.Util.talerSites.length; i++){
	      if (tabUrl == Taler.Wallet.Util.talerSites[i])
	        found = true;
	    }
            if (found)
	      changeButtonColor.call(currentWindow, true);	  
            else
	      changeButtonColor.call(currentWindow, false);	  
	  },

          stringBundle: {}, // locales management 


          /* Called once every page load, it registers the listener for the "taler-currency" event.
          The chain of events' monitoring is as follows:
          1) onPageLoad gets called on ANY page load (triggered by "load" event on window.gBrowser)
          (Apparently, "load" event on just the 'window' object does NOT give any sign.) 
          2) onPageLoad sends then a "taler-wallet" event to the merchant and, if the merchant is Taler
          compliant, then he will reply with a "taler-currency" event
          3) on any loaded page we check whether some merchant has dispatched an event to tell us
          that he is Taler friendly (he does so by issuing a "taler-currency" event on his (HTML)
	  window.contentDocument). */

	
          onPageLoad: function(aEvent) {

            let doc = aEvent.originalTarget;
            if (doc instanceof doc.defaultView.HTMLDocument) {

              if (doc && doc.addEventListener) {

		tDebug("Im registering on page", false);
                Taler.Wallet.registerCallbacks(doc);
              }
            }
          },

	  /* Register callbacks to manage events of interest, on the 'body' element */
	  registerCallbacks: function(htmlDoc){
		tDebug(htmlDoc, false);

            // FIXME: Warning: When called with a truthy forth argument,
            // listeners can be triggered potentially unsafely by untrusted code.
            // This requires careful review.

	        htmlDoc.addEventListener("taler-wire-probe",
		                         Taler.Wallet.Util.gettingWalletProbe,
			                 false,
					 true);

	        htmlDoc.addEventListener("taler-checkout-probe",
		                         Taler.Wallet.Util.gettingWalletProbe,
			                 false,
					 true);
                htmlDoc.addEventListener("taler-contract",
		                         handleContract,
			                 false,
					 true);
                htmlDoc.addEventListener("taler-create-reserve",
		                         function(evt){
			                 tDebug("parameters gotten from mint: " + Object.keys(evt.detail),
					        false);
					 createReserve(evt.detail);},
					 false,
					 true); 
	  },

          Util: {

	    talerSites: new Array(),

	    gettingWalletProbe: function(aEvent){
              let currentWindow = getLastWindow();
	      let doc = aEvent.originalTarget;
	      tDebug("target URL: " + doc.documentURI, false);
	      tDebug("focused URL: " + currentWindow.gBrowser.contentDocument.documentURI, false);

	      /* put this URL among the Taler-friendly ones */
	      try {
	        Taler.Wallet.Util.talerSites
		.push(doc.defaultView.location);
	      }
	      catch (err){
	        /* FAKE error: the color did change, although Firefox claims
		  object 'Taler' as undefined */
	      }

	      /* the event has got to be generated on the document of the
	        supposed recipient; otherwise it won't be able to access
	        the event's type field which is needed to determine whether
	        or not to reply to this signal */
              let wevent = doc.defaultView.Event("taler-wallet-present");
              doc.dispatchEvent(wevent);
	    
            /* check if the page which sent a probe is within the focused tab */
	    let selectedTabUrl = currentWindow.gBrowser
	    .getBrowserForTab(currentWindow.gBrowser.selectedTab)
	    .contentDocument.documentURI;
	    showOutput(selectedTabUrl, false);
	    showOutput(doc.documentURI, false);
            if (selectedTabUrl == doc.documentURI)
	      changeButtonColor(currentWindow, false);
	    }
          },
          /* Taler.Wallet.Util closed */



      } /* Taler.Wallet closed */

    }; /* Taler closed */
