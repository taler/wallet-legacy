---------
Licensing
---------

Each JavaScript/CSS/XUL file located under this directory
is licensed with GPL, or one of its variants; refer to each
file's header in order to get detailed information about its
license. The copyright of the aforementioned files is held by
INRIA. However, there are some PNG files which are licensed
with GPL or one of its variants whose copyright is held by Christian
Grothoff.

--------------------
Runtime dependencies
--------------------

The sole runtime dependency needed to run this project is the Firefox
Web browser, which is licensed under Mozilla Public License, and
whose copyright is held by the Mozilla Foundation
